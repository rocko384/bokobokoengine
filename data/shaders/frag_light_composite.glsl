#version 430

in vec2 TexCoords;

out vec4 outColor;

layout(binding = 0) uniform sampler2D ColorA;
layout(binding = 1) uniform sampler2D DepthA;
layout(binding = 2) uniform sampler2D ColorB;
layout(binding = 3) uniform sampler2D DepthB;

void main() {
	outColor = texture(ColorA, TexCoords) + texture(ColorB, TexCoords);
	gl_FragDepth = texture(DepthA, TexCoords).r;
}