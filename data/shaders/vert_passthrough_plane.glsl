#version 430

vec3 vertices[6] = vec3[6](
        vec3(-1.0, -1.0, 0.0),
        vec3(1.0, -1.0, 0.0),
        vec3(-1.0, 1.0, 0.0),
        vec3(-1.0, 1.0, 0.0),
        vec3(1.0, -1.0, 0.0),
        vec3(1.0, 1.0, 0.0)
    );

vec2 uv[6] = vec2[6](
        vec2(0.0, 0.0),
        vec2(1.0, 0.0),
        vec2(0.0, 1.0),
        vec2(0.0, 1.0),
        vec2(1.0, 0.0),
        vec2(1.0, 1.0)
    );

out vec2 TexCoords;

void main() {
	gl_Position = vec4(vertices[gl_VertexID], 1.0);
    TexCoords = uv[gl_VertexID];
}