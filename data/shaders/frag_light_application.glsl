#version 430

in vec2 TexCoords;

out vec4 outColor;

uniform vec3 ambient;

layout(binding = 0) uniform sampler2D BaseColor;
layout(binding = 1) uniform sampler2D BaseDepth;
layout(binding = 2) uniform sampler2D LightColor;
layout(binding = 3) uniform sampler2D LightDepth;

void main() {
	outColor = texture(BaseColor, TexCoords) * (vec4(ambient, 1.0) + texture(LightColor, TexCoords));
	gl_FragDepth = texture(BaseDepth, TexCoords).r;
}