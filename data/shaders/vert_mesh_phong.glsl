#version 430

layout(location = 0) in vec3 vposition;
layout(location = 1) in vec3 vnormal;
layout(location = 2) in vec3 vcolor;
layout(location = 3) in vec2 vtexcoord;

out vec3 FragCoord;
out vec3 fnormal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat3 normal;

void main() {
	FragCoord = (model * vec4(vposition, 1.0)).xyz;
	fnormal = normal * normalize(vnormal);
	gl_Position = projection * view * model * vec4(vposition, 1.0);
}
