#version 430

layout(location = 0) in vec2 vposition;
layout(location = 1) in vec3 vnormal;
layout(location = 2) in vec3 vcolor;
layout(location = 3) in vec2 vtexcoord;

out vec3 Color;
out vec2 Texcoord;

uniform vec2 tileSize;
uniform vec2 sheetSize;
uniform ivec2 tile;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
	Color = vcolor;
	Texcoord = (vtexcoord * tileSize / sheetSize) + (tile * tileSize / sheetSize);
	gl_Position = projection * view * model * vec4(vposition, 0.0, 1.0);
}
