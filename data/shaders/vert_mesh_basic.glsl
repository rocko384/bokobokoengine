#version 430

layout(location = 0) in vec3 vposition;
layout(location = 1) in vec3 vnormal;
layout(location = 2) in vec3 vcolor;
layout(location = 3) in vec2 vtexcoord;

out vec3 Color;
out vec2 Texcoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
	Color = vcolor;
	Texcoord = vtexcoord;
	gl_Position = projection * view * model * vec4(vposition, 1.0);
}
