#version 430

in vec2 TexCoords;

out vec4 outColor;

layout(binding = 0) uniform sampler2D Color;
layout(binding = 1) uniform sampler2D Depth;

void main() {
	outColor = texture(Color, TexCoords);
	gl_FragDepth = texture(Depth, TexCoords).r;
}