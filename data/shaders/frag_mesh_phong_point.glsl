#version 430

in vec3 FragCoord;
in vec3 fnormal;

layout(location = 0) out vec4 outColor;

struct PointLight {
    vec3 diffuse;
    vec3 specular;
    vec3 position;
    float intensity;
};

uniform PointLight light;
uniform vec3 viewCoord;

void main() {
    vec3 lightDirection = normalize(light.position - FragCoord);

    float diffuseBrightness = light.intensity * max(dot(normalize(fnormal), lightDirection), 0.0);
    vec3 diffuse = diffuseBrightness * light.diffuse;
    
    vec3 viewDirection = normalize(viewCoord - FragCoord);
    vec3 reflectDirection = reflect(-lightDirection, normalize(fnormal));

    float specularBrightness = light.intensity * pow(max(dot(viewDirection, reflectDirection), 0.0), 64);
    vec3 specular = 0.5 * specularBrightness * light.specular;

    outColor = vec4((diffuse + specular), 1.0);
}