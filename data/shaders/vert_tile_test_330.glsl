#version 330

layout(location = 0) in vec2 position;
layout(location = 1) in vec3 color;
layout(location = 2) in vec2 texcoord;

out vec3 Color;
out vec2 Texcoord;

uniform vec2 tileSize;
uniform ivec2 tile;

void main() {
	Color = color;
    Texcoord = (texcoord * tileSize) + (tile * tileSize);
	gl_Position = vec4(position, 0.0, 1.0);
}