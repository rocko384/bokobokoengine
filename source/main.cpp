#include <engine/engine.h>
#include "engine/physics/PhysicsEngine.h"
#include <binding/binding.h>

#include <chrono>

using namespace dankengine;
using namespace binding;
using namespace physics;

int main(int argc, char** argv) {
	
	size_t width = 800;
	size_t height = 600;
	
	if (argc > 1) {
		if (std::string(argv[1]) == "auto") {
			std::vector<sf::VideoMode> vms = sf::VideoMode::getFullscreenModes();
			// Find a viable resolution
			for (auto it = vms.begin(); it != vms.end(); it++) {
				// SFML sorts best colors first so if we reach poor ones, break
				if (it->bitsPerPixel < 24) {
					break;
				}
				// Check widescreen resolutions first
				if (it->width / 16 == it->height / 9) {
					width = it->width;
					height = it->height;
					// For really high-DPI screens, fast-divide by 2
					// REMOVE this after we go to a full screen context
					if (it->height > 1440) {
						width >>= 1;
						height >>= 1;
					}
					break;
				}
				// If a good widescreen mode isn't out there, go to 4:3
				if (it->width / 4 == it->height / 3) {
					width = it->width;
					height = it->height;
					break;
				}
			}
		}
		else if (argc == 3) {
			width = std::stoi(std::string(argv[1]));
			height = std::stoi(std::string(argv[2]));
		}
	}
	
	Renderer r(width, height, "Bird", 0);
	InputSwitchboard switchBoard;

	PhysicsEngine physicsEngine;

	float TileSize = 256.f;
	float SheetSize = 256.f;
	Handle<Sprite> s = std::make_shared<Sprite>();

	// Draw Color test
	s->setTileSheet(RESOURCE_DIR_STR + "textures/test_colors.png");
	s->setTileSize(glm::vec2(TileSize));
	s->setSheetSize(glm::vec2(SheetSize));
	s->setTile(0, 0);

	Handle<Mesh> m = std::make_shared<Mesh>();
	
					// Position							Normal												Color								UV
	m->pushVertex(	glm::vec3(0.5f, 0.5f, 0.5f),		glm::normalize(glm::vec3(0.5f, 0.5f, 0.5f)),		glm::vec3(1.0f, 0.0f, 0.0f),		glm::vec2(1.0f, 0.0f));	// 0 Top Right Front
	m->pushVertex(	glm::vec3(0.5f, -0.5f, 0.5f),		glm::normalize(glm::vec3(0.5f, -0.5f, 0.5f)),		glm::vec3(0.0f, 1.0f, 0.0f),		glm::vec2(1.0f, 1.0f));	// 1 Bottom Right Front
	m->pushVertex(	glm::vec3(-0.5f, 0.5f, 0.5f),		glm::normalize(glm::vec3(-0.5f, 0.5f, 0.5f)),		glm::vec3(0.0f, 0.0f, 1.0f),		glm::vec2(0.0f, 0.0f));	// 2 Top Left Front
	m->pushVertex(	glm::vec3(-0.5f, -0.5f, 0.5f),		glm::normalize(glm::vec3(-0.5f, -0.5f, 0.5f)),		glm::vec3(1.0f, 1.0f, 0.0f),		glm::vec2(0.0f, 1.0f));	// 3 Bottom Left Front
	m->pushVertex(	glm::vec3(0.5f, 0.5f, -0.5f),		glm::normalize(glm::vec3(0.5f, 0.5f, -0.5f)),		glm::vec3(1.0f, 0.0f, 1.0f),		glm::vec2(1.0f, 0.0f));	// 4 Top Right Back
	m->pushVertex(	glm::vec3(0.5f, -0.5f, -0.5f),		glm::normalize(glm::vec3(0.5f, -0.5f, -0.5f)),		glm::vec3(0.0f, 1.0f, 1.0f),		glm::vec2(1.0f, 1.0f));	// 5 Bottom Right Back
	m->pushVertex(	glm::vec3(-0.5f, 0.5f, -0.5f),		glm::normalize(glm::vec3(-0.5f, 0.5f, -0.5f)),		glm::vec3(0.5f, 0.0f, 0.0f),		glm::vec2(0.0f, 0.0f));	// 6 Top Left Back
	m->pushVertex(	glm::vec3(-0.5f, -0.5f, -0.5f),		glm::normalize(glm::vec3(-0.5f, -0.5f, -0.5f)),		glm::vec3(1.0f, 1.0f, 0.5f),		glm::vec2(0.0f, 1.0f));	// 7 Bottom Left Back

	m->setIndices({
			0, 3, 1, // Front Face
			0, 2, 3,
			0, 1, 5, // Right Face
			5, 4, 0,
			7, 3, 2, // Left Face
			2, 6, 7,
			5, 7, 4, // Back Face
			7, 6, 4,
			0, 4, 6, // Top Face
			2, 0, 6,
			7, 5, 1, // Bottom Face
			7, 1, 3
		});

	m->compileVertices();

	Handle<Mesh> apple = std::make_shared<Mesh>();
	Resource res(RESOURCE_DIR_STR + "models/apple/appletexturedobj.obj");
	apple->loadMeshFromOBJ(res);
	apple->shaderProfile = names::defaultMeshPhongProfileName;

	Handle<SceneNode> appleNode = std::make_shared<SceneNode>();
	appleNode->tie(apple);
	appleNode->name = "Apple";
	appleNode->scale = glm::vec3(0.025f);
	appleNode->position = glm::vec3(0, 0, -5);
	appleNode->rotation = glm::angleAxis(glm::radians(45.0f), glm::normalize(glm::vec3(1.0f, 1.0f, 0.0f)));


	Scene scene;

	Handle<SceneNode> c = std::make_shared<SceneNode>();
	c->name = "Camera1";

	Handle<Camera> cam = std::make_shared<Camera>();
	cam->perspective(800, 600, 45, 0.1f, 100);
//	cam->orthographic(800, 600);
	c->tie(cam);
	cam->active = true;
	c->position.z = 2;

	Handle<SceneNode> n = std::make_shared<SceneNode>();
	n->tie(s);
	n->name = "Ogo";
	n->scale = glm::vec3(1);
	n->position = glm::vec3(0, 0, -4);
	n->rotation = glm::angleAxis(glm::radians(45.0f), glm::normalize(glm::vec3(0.0f, 1.0f, 1.0f)));

	Handle<SceneNode> moving_box = std::make_shared<SceneNode>();
	moving_box->tie(m);
	moving_box->name = "MovingBox";
	moving_box->position = glm::vec3(1.5, 0, -5);
	moving_box->rotation = glm::angleAxis(glm::radians(45.0f), glm::normalize(glm::vec3(1.0f, 1.0f, 0.0f)));

	// add physics to moving_box
	physicsEngine.addPhysicsObject(moving_box, glm::vec3(5, 15, -15), physics::GRAVITY);

	Handle<SceneNode> pointNode = std::make_shared<SceneNode>();
	Handle<Light> pointLight = std::make_shared<Light>();
	pointLight->type = Light::Type::POINT;
	pointLight->diffuse = { 0.5f, 0.5f, 0.5f };
	pointLight->specular = pointLight->diffuse;
	pointLight->intensity = 1.0f;

	pointNode->name = "Point1";
	pointNode->tie(pointLight);
	pointNode->position = { -5.0f, 0.0f, 0.0f };

	Handle<SceneNode> pointNodeStatic = std::make_shared<SceneNode>();
	Handle<Light> pointLightStatic = std::make_shared<Light>();
	pointLightStatic->type = Light::Type::POINT;
	pointLightStatic->diffuse = { 0.0f, 0.0f, 1.0f };
	pointLightStatic->specular = pointLightStatic->diffuse;
	pointLightStatic->intensity = 1.0f;

	pointNodeStatic->name = "Point2";
	pointNodeStatic->tie(pointLightStatic);
	pointNodeStatic->position = { 5.0f, 0.0f, 0.0f };

	Handle<SceneNode> directionalNodeGreen = std::make_shared<SceneNode>();
	Handle<Light> directionalLightGreen = std::make_shared<Light>();
	directionalLightGreen->type = Light::Type::DIRECTIONAL;
	directionalLightGreen->diffuse = { 0.0f, 1.0f, 0.0f };
	directionalLightGreen->specular = directionalLightGreen->diffuse;
	directionalLightGreen->intensity = 1.0f;
	directionalLightGreen->direction = { -1.0f, -1.0f, 0.0f };

	directionalNodeGreen->name = "Directional";
	directionalNodeGreen->tie(directionalLightGreen);

	Handle<SceneNode> spotNode = std::make_shared<SceneNode>();
	Handle<Light> spotLight = std::make_shared<Light>();
	spotLight->type = Light::Type::SPOT;
	spotLight->diffuse = { 1.0f, 0.0f, 0.0f };
	spotLight->specular = spotLight->diffuse;
	spotLight->intensity = 1.0f;
	spotLight->aperture = glm::half_pi<float>() * 0.5f;
	spotLight->direction = { -1.0f, 0.0f, 0.0f };
	
	spotNode->name = "Spot";
	spotNode->tie(spotLight);
	spotNode->position = { 5.0f, 0.0f, -5.0f };


	scene.root.addChild(n);
	scene.root.addChild(c);
	scene.root.addChild(appleNode);
	scene.root.addChild(moving_box);
	scene.root.addChild(pointNode);
	//scene.root.addChild(pointNodeStatic);
	scene.root.addChild(directionalNodeGreen);
	scene.root.addChild(spotNode);

	auto start = std::chrono::steady_clock::now();
	auto now = std::chrono::steady_clock::now();
	auto last_tick = std::chrono::steady_clock::now();

	bool color = true;

	float JUMPBOI = 0.0f;

	while (r.window.isOpen()) {
		switchBoard.pollEventsFromWindow(r.window);

		if (switchBoard.isKeyPressed(sf::Keyboard::Space)) {
			JUMPBOI = 1.0f;
		}
		else {
			JUMPBOI = 0.0f;
		}

		now = std::chrono::steady_clock::now();

		std::chrono::milliseconds now_s = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());

		// appleNode->rotation = glm::angleAxis(glm::radians((float)(now_s.count() / 10)), glm::normalize(glm::vec3(1.0f, 1.0f, 0.0f)));
		appleNode->position = glm::vec3(-1.5, JUMPBOI, -5.0f + glm::sin(glm::radians((float)(now_s.count() / 10))));
		moving_box->rotation = glm::angleAxis(glm::radians((float)(now_s.count() / 10)), glm::normalize(glm::vec3(1.0f, 1.0f, 0.0f)));

		pointNode->position = glm::vec3(5.0f * glm::sin(glm::radians((float)(now_s.count() / 20))), -1.0f, 5.0f * glm::cos(glm::radians((float)(now_s.count() / 20))));
		directionalLightGreen->direction = { glm::sin(glm::radians((float)(now_s.count() / 20))), -1.0f, 0.0f };


		if (now.time_since_epoch() - start.time_since_epoch() > std::chrono::seconds(1)) {
			if (color) {
				s->setTileSheet(RESOURCE_DIR_STR + "textures/test_colors_inverted.png");
				n->position = { 0.0, 0, -4 };
				color = false;
			}
			else {
				s->setTileSheet(RESOURCE_DIR_STR + "textures/test_colors.png");
				n->position = { 0.0, 0, -8 };
				color = true;
			}

			start = now;
		}

		// do physics
		std::chrono::duration<float> elapsed_time = now.time_since_epoch() - last_tick.time_since_epoch();
		physicsEngine.tick(elapsed_time);
		last_tick = now;

		r.render(scene);
		r.flip();
	}

	return 0;
}
