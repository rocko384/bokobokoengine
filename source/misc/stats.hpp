#pragma once

#include <numeric>

template<class T>
T mean(const std::vector<T>& v) {
	std::vector<T> accv(v);
	return (std::accumulate(accv.begin(), accv.end(), 0) / accv.size());
}

template<class T>
T median(const std::vector<T>& v) {
	// Create an explicit deep copy of v that we can modify
	std::vector<T> sortv(v);
	
	size_t n = sortv.size() / 2;
	std::nth_element(sortv.begin(), sortv.begin() + n, sortv.end());
	
	T nth = sortv[n];
	
	if (v.size() & 1) {
		return nth;
	}
	else {
		std::nth_element(sortv.begin(), sortv.begin() + (n - 1), sortv.end());
		return (nth + sortv[n - 1]) / 2;
	}
	
}
