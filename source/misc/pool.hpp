#pragma once

#include <functional>
#include <memory>
#include <mutex>
#include <thread>
#include <tuple>
#include <vector>


template <class ... Ts>
class pool {
public:	
	// Create a pool object which will launch requested number of threads. See 
	// the behavior of this->resize(0) for the default setting.
	pool(size_t num_threads = 0) {
		
		auto default_func = [](Ts...) {
			;
		};
		set_worker(std::move(default_func));
		
		resize(num_threads);
		
		// For some (presumably stupid) reason, I can't construct a thread
		// from a internal_worker as a private member function, so here it is
		// as a private member std::function instead.
		internal_worker = [this]() {
			while (!done.try_lock()) {
				data_ready.lock();
					if (data.empty()) {
						data_ready.unlock();
						std::this_thread::yield();
						continue;
					}
					std::tuple<Ts...> payload = data.back();
					data.pop_back();
				data_ready.unlock();
				std::apply(*func_ptr, payload);
			}
			done.unlock();
		};
	};
	
	// Set the worker function for the pool. Data pushed to the pool will be
	// popped and pass as arguments to the this function.
	// 
	// This method is not thread-safe and should only be called when the pool
	// is stopped. It is, by design, the only method in this class which may not
	// be called while the pool is working.
	void set_worker(std::function<void(Ts...)>&& func) {
		func_ptr = std::make_shared<std::function<void(Ts...)>>(func);
	};
	void set_worker(std::shared_ptr<std::function<void(Ts...)>> func) {
		func_ptr = func;
	};
	
	// Push a pack of arguments to the pool's data buffer.
	// 
	// Thread-safe.
	// May be called when pool is working.
	void push(Ts ...args) {
		data_ready.lock();
		data.push_back(std::make_tuple(args...));
		data_ready.unlock();
	};
	void push(Ts&& ...args) {
		data_ready.lock();
		data.push_back(std::move(std::make_tuple(args...)));
		data_ready.unlock();
	};
	
	// Empty the pool's internal data buffer.
	// 
	// Thread-safe.
	// May be called when pool is working.
	void clear() {
		data_ready.lock();
		data.clear();
		data_ready.unlock();
	};
	
	// Change the number of threads which the pool should use. Passing no 
	// arguments or the value 0 will cause the pool to attempt to resize to
	// the maximum supported number of hardware threads. If this value is not 
	// available, it will be set to a default value of 1.
	// 
	// NOT thread-safe. Modifies the pool.
	// May be called when pool is working, but the changes might not be
	// reflected until the pool is stopped.
	void resize(size_t num_threads = 0) {
		if (num_threads == 0) {
			num_threads = std::thread::hardware_concurrency();
			nthreads = num_threads > 0 ? num_threads : 1;
			return;
		}
		nthreads = num_threads;
	};
	
	// Create the required number of worker threads (see this->resize()) and 
	// begin processing stored data.
	// 
	// NOT thread-safe.
	// May be called when the pool is working in which case it does nothing.
	void start() {
		if (!workers.empty()) {
			return;
		}
		done.lock();
		for (size_t i = 0; i < nthreads; i++) {
			workers.push_back(std::move(
				std::thread(std::function<void()>(internal_worker))
			));
		}
	};
	
	// Allow all threads to exit and join() with them. After the threads have
	// joined, destroy them.
	// 
	// NOT thread-safe.
	// May be called when the pool is running. Does nothing if called when the
	// pool is not running.
	void stop() {
		done.unlock();
		join_all();
		workers.clear();
	};
	
private:
	void join_all() {
		for (auto it = workers.begin(); it != workers.end(); it++) {
			it->join();
		}
	};
	
	std::function<void()> internal_worker;
	std::shared_ptr<std::function<void(Ts...)>> func_ptr;
	std::vector<std::thread> workers;
	std::vector<std::tuple<Ts...>> data;
	std::mutex done;
	std::mutex data_ready;
	
	size_t nthreads = 1;
};
