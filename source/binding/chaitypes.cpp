#include <binding/binding.h>
#include <chaiscript/chaiscript_stdlib.hpp>

using binding::AccessoryType;
using binding::UnitData;
using binding::MovementProfile;
using binding::Weapon;
using binding::Faction;

void binding::ChaiContext::BindTypes(chaiscript::ModulePtr m) {
	
	chaiscript::utility::add_class<Weapon>(*m,
		"Weapon",
		{
			chaiscript::constructor<Weapon()>(),
			chaiscript::constructor<Weapon(const Weapon&)>()
		},
		{
			{chaiscript::fun(&Weapon::weaponType), "weaponType"},
			{chaiscript::fun(&Weapon::payload), "payload"},
			{chaiscript::fun(&Weapon::targeting), "targeting"},
			
			{chaiscript::fun(&Weapon::damage), "damage"},
			{chaiscript::fun(&Weapon::armorMultiplier), "armorMultiplier"},
			{chaiscript::fun(&Weapon::shieldMultiplier), "shieldMultiplier"},
			
			{chaiscript::fun(&Weapon::firerate), "firerate"},
			{chaiscript::fun(&Weapon::duration), "duration"},
			{chaiscript::fun(&Weapon::cooldown), "cooldown"},
			
			{chaiscript::fun(&Weapon::velocity), "velocity"},
			{chaiscript::fun(&Weapon::blastRadius), "blastRadius"},
			{chaiscript::fun(&Weapon::range), "range"}
		}
	);

	chaiscript::utility::add_class<MovementProfile>(*m,
		"MovementProfile",
		{
			chaiscript::constructor<MovementProfile()>(),
			chaiscript::constructor<MovementProfile(const MovementProfile&)>()
		},
		{
			{chaiscript::fun(&MovementProfile::maxVelocity), "maxVelocity"},
			{chaiscript::fun(&MovementProfile::acceleration), "acceleration"},
			{chaiscript::fun(&MovementProfile::pivotSpeed), "pivotSpeed"},
			{chaiscript::fun(&MovementProfile::minVelocity), "minVelocity"},
			{chaiscript::fun(&MovementProfile::fuel), "fuel"},
			{chaiscript::fun(&MovementProfile::strafes), "strafes"}
		}
	);

	chaiscript::utility::add_class<UnitData>(*m,
		"UnitData",
		{
			chaiscript::constructor<UnitData()>(),
			chaiscript::constructor<UnitData(const UnitData&)>()
		},
		{
			{chaiscript::fun(&UnitData::displayName), "displayName"},
			{chaiscript::fun(&UnitData::meshFile), "meshFile"},
			// Stats
			{chaiscript::fun(&UnitData::hp), "hp"},
			{chaiscript::fun(&UnitData::vision), "vision"},
			{chaiscript::fun(&UnitData::mcost), "mcost"},
			{chaiscript::fun(&UnitData::ecost), "ecost"},
			{chaiscript::fun(&UnitData::time), "time"},
			{chaiscript::fun(&UnitData::tier), "tier"},
			{chaiscript::fun(&UnitData::size), "size"},
			
			// Targeting
			{chaiscript::fun(&UnitData::targetedByAir), "targetedByAir"},
			{chaiscript::fun(&UnitData::targetedByLand), "targetedByLand"},
			{chaiscript::fun(&UnitData::targetedBySea), "targetedBySea"},
			
			// Building
			{chaiscript::fun(&UnitData::buildsInLand), "buildsInLand"},
			{chaiscript::fun(&UnitData::buildsInSea), "buildsInSea"},
			{chaiscript::fun(&UnitData::buildsInAir), "buildsInAir"},
			{chaiscript::fun(&UnitData::buildsInFactory), "buildsInFactory"},
			
			// Attributes
			{chaiscript::fun(&UnitData::amphibious), "amphibious"},
			{chaiscript::fun(&UnitData::capturable), "capturable"},
			{chaiscript::fun(&UnitData::crushes), "crushes"},
			{chaiscript::fun(&UnitData::explodes), "explodes"},
			{chaiscript::fun(&UnitData::hovers), "hovers"},
			{chaiscript::fun(&UnitData::invulnerable), "invulnerable"},
			{chaiscript::fun(&UnitData::regenerates), "regenerates"},
			{chaiscript::fun(&UnitData::repairable), "repairable"},
			
			// Actions
			{chaiscript::fun(&UnitData::assists), "assists"},
			{chaiscript::fun(&UnitData::builds), "builds"},
			{chaiscript::fun(&UnitData::captures), "captures"},
			{chaiscript::fun(&UnitData::reclaims), "reclaims"},
			{chaiscript::fun(&UnitData::repairs), "repairs"},
			{chaiscript::fun(&UnitData::sacrifices), "sacrifices"},
			{chaiscript::fun(&UnitData::submerges), "submerges"},
			{chaiscript::fun(&UnitData::transports), "transports"},
			{chaiscript::fun(&UnitData::teleports), "teleports"},
			
			// Other Members
			{chaiscript::fun(&UnitData::movement), "movement"},
			{chaiscript::fun(&UnitData::setMovement), "setMovement"},
			{chaiscript::fun(&UnitData::accessories), "accessories"},
			{chaiscript::fun(&UnitData::addAccessory), "addAccessory"},
			{chaiscript::fun(&UnitData::weapons), "weapons"},
			{chaiscript::fun(&UnitData::addWeapon), "addWeapon"},
			{chaiscript::fun(&UnitData::callback), "callback"},
			{chaiscript::fun(&UnitData::setDeathCallback), "setDeathCallback"}
		}
	);

	chaiscript::utility::add_class<Faction>(*m,
		"Faction",
		{
			chaiscript::constructor<Faction()>(),
			chaiscript::constructor<Faction(const Faction&)>()
		},
		{
			{chaiscript::fun(&Faction::name), "name"},
			{chaiscript::fun(&Faction::keys), "keys"},
			{chaiscript::fun(&Faction::addUnitKey), "addUnitKey"}
		}
	);
}

void binding::ChaiContext::BindAccessories(chaiscript::ModulePtr m) {
	chaiscript::utility::add_class<binding::Shield>(*m,
		"Shield",
		{
			chaiscript::constructor<binding::Shield()>(),
			chaiscript::constructor<binding::Shield(const binding::Shield&)>()
		},
		{
			{chaiscript::fun(&binding::Shield::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::Shield::hp), "hp"},
			{chaiscript::fun(&binding::Shield::ecost), "ecost"},
			{chaiscript::fun(&binding::Shield::radius), "radius"}
		}
	);
	chaiscript::utility::add_class<binding::Cloak>(*m,
		"Cloak",
		{
			chaiscript::constructor<binding::Cloak()>(),
			chaiscript::constructor<binding::Cloak(const binding::Cloak&)>()
		},
		{
			{chaiscript::fun(&binding::Cloak::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::Cloak::ecost), "ecost"}
		}
	);
	chaiscript::utility::add_class<binding::Stealth>(*m,
		"Stealth",
		{
			chaiscript::constructor<binding::Stealth()>(),
			chaiscript::constructor<binding::Stealth(const binding::Stealth&)>()
		},
		{
			{chaiscript::fun(&binding::Stealth::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::Stealth::ecost), "ecost"}
		}
	);
	chaiscript::utility::add_class<binding::Jammer>(*m,
		"Jammer",
		{
			chaiscript::constructor<binding::Jammer()>(),
			chaiscript::constructor<binding::Jammer(const binding::Jammer&)>()
		},
		{
			{chaiscript::fun(&binding::Jammer::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::Jammer::ecost), "ecost"}
		}
	);
	chaiscript::utility::add_class<binding::Radar>(*m,
		"Radar",
		{
			chaiscript::constructor<binding::Radar()>(),
			chaiscript::constructor<binding::Radar(const binding::Radar&)>()
		},
		{
			{chaiscript::fun(&binding::Radar::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::Radar::ecost), "ecost"},
			{chaiscript::fun(&binding::Radar::radius), "radius"}
		}
	);
	chaiscript::utility::add_class<binding::Sonar>(*m,
		"Sonar",
		{
			chaiscript::constructor<binding::Sonar()>(),
			chaiscript::constructor<binding::Sonar(const binding::Sonar&)>()
		},
		{
			{chaiscript::fun(&binding::Sonar::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::Sonar::ecost), "ecost"},
			{chaiscript::fun(&binding::Sonar::radius), "radius"}
		}
	);
	chaiscript::utility::add_class<binding::Omni>(*m,
		"Omni",
		{
			chaiscript::constructor<binding::Omni()>(),
			chaiscript::constructor<binding::Omni(const binding::Omni&)>()
		},
		{
			{chaiscript::fun(&binding::Omni::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::Omni::radius), "radius"}
		}
	);
	chaiscript::utility::add_class<binding::Extractor>(*m,
		"Extractor",
		{
			chaiscript::constructor<binding::Extractor()>(),
			chaiscript::constructor<binding::Extractor(const binding::Extractor&)>()
		},
		{
			{chaiscript::fun(&binding::Extractor::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::Extractor::mgen), "mgen"},
			{chaiscript::fun(&binding::Extractor::ecost), "ecost"}
		}
	);
	chaiscript::utility::add_class<binding::Generator>(*m,
		"Generator",
		{
			chaiscript::constructor<binding::Generator()>(),
			chaiscript::constructor<binding::Generator(const binding::Generator&)>()
		},
		{
			{chaiscript::fun(&binding::Generator::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::Generator::egen), "egen"}
		}
	);
	chaiscript::utility::add_class<binding::MStorage>(*m,
		"MStorage",
		{
			chaiscript::constructor<binding::MStorage()>(),
			chaiscript::constructor<binding::MStorage(const binding::MStorage&)>()
		},
		{
			{chaiscript::fun(&binding::MStorage::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::MStorage::mstor), "mstor"}
		}
	);
	chaiscript::utility::add_class<binding::EStorage>(*m,
		"EStorage",
		{
			chaiscript::constructor<binding::EStorage()>(),
			chaiscript::constructor<binding::EStorage(const binding::EStorage&)>()
		},
		{
			{chaiscript::fun(&binding::EStorage::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::EStorage::estor), "estor"}
		}
	);
	chaiscript::utility::add_class<binding::TMLauncher>(*m,
		"TMLauncher",
		{
			chaiscript::constructor<binding::TMLauncher()>(),
			chaiscript::constructor<binding::TMLauncher(const binding::TMLauncher&)>()
		},
		{
			{chaiscript::fun(&binding::TMLauncher::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::TMLauncher::mcost), "mcost"},
			{chaiscript::fun(&binding::TMLauncher::ecost), "ecost"},
			{chaiscript::fun(&binding::TMLauncher::time), "time"},
			{chaiscript::fun(&binding::TMLauncher::capacity), "capacity"},
			{chaiscript::fun(&binding::TMLauncher::missile), "missile"}
		}
	);
	chaiscript::utility::add_class<binding::SMLauncher>(*m,
		"SMLauncher",
		{
			chaiscript::constructor<binding::SMLauncher()>(),
			chaiscript::constructor<binding::SMLauncher(const binding::SMLauncher&)>()
		},
		{
			{chaiscript::fun(&binding::SMLauncher::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::SMLauncher::mcost), "mcost"},
			{chaiscript::fun(&binding::SMLauncher::ecost), "ecost"},
			{chaiscript::fun(&binding::SMLauncher::time), "time"},
			{chaiscript::fun(&binding::SMLauncher::capacity), "capacity"},
			{chaiscript::fun(&binding::SMLauncher::missile), "missile"}
		}
	);
	chaiscript::utility::add_class<binding::Factory>(*m,
		"Factory",
		{
			chaiscript::constructor<binding::Factory()>(),
			chaiscript::constructor<binding::Factory(const binding::Factory&)>()
		},
		{
			{chaiscript::fun(&binding::Factory::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::Factory::tier), "tier"},
			{chaiscript::fun(&binding::Factory::buildsLand), "buildsLand"},
			{chaiscript::fun(&binding::Factory::buildsSea), "buildsSea"},
			{chaiscript::fun(&binding::Factory::buildsAir), "buildsAir"},
			{chaiscript::fun(&binding::Factory::buildRate), "buildRate"}
		}
	);
	chaiscript::utility::add_class<binding::Engineering>(*m,
		"Engineering",
		{
			chaiscript::constructor<binding::Engineering()>(),
			chaiscript::constructor<binding::Engineering(const binding::Engineering&)>()
		},
		{
			{chaiscript::fun(&binding::Engineering::getAccessoryType), "getAccessoryType"},
			{chaiscript::fun(&binding::Engineering::tier), "tier"},
			{chaiscript::fun(&binding::Engineering::radius), "radius"},
			{chaiscript::fun(&binding::Engineering::buildRate), "buildRate"}
		}
	);
	
	m->add(chaiscript::user_type<Accessory>(), "Accessory");
	m->add(chaiscript::base_class<Accessory, binding::Shield>());
	m->add(chaiscript::base_class<Accessory, binding::Cloak>());
	m->add(chaiscript::base_class<Accessory, binding::Stealth>());
	m->add(chaiscript::base_class<Accessory, binding::Jammer>());
	m->add(chaiscript::base_class<Accessory, binding::Radar>());
	m->add(chaiscript::base_class<Accessory, binding::Sonar>());
	m->add(chaiscript::base_class<Accessory, binding::Omni>());
	m->add(chaiscript::base_class<Accessory, binding::Extractor>());
	m->add(chaiscript::base_class<Accessory, binding::Generator>());
	m->add(chaiscript::base_class<Accessory, binding::MStorage>());
	m->add(chaiscript::base_class<Accessory, binding::EStorage>());
	m->add(chaiscript::base_class<Accessory, binding::TMLauncher>());
	m->add(chaiscript::base_class<Accessory, binding::SMLauncher>());
	m->add(chaiscript::base_class<Accessory, binding::Factory>());
	m->add(chaiscript::base_class<Accessory, binding::Engineering>());
}
