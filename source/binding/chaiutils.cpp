#include <binding/binding.h>
#include <chaiscript/chaiscript_stdlib.hpp>

#include <iostream>
#include <fstream>
#include <vector>

using binding::AccessoryType;
using binding::UnitData;
using binding::MovementProfile;
using binding::Weapon;
using binding::Faction;

void binding::ChaiContext::BindUtils() {
	
	// I need to figure out some stuff regarding the internal chai <-> json 
	// bindings before I can do anything here
	
	auto ns_utils = [](chaiscript::Namespace& utils) {
		
		utils["readfile"] = chaiscript::var(chaiscript::fun([](const std::string& path) {		
			std::string s = "";
			std::ifstream f(path, std::ios_base::binary);
			if (!f.good()) {
				return std::string("");
			}
			
			f.seekg(0, std::ios_base::end);
			size_t fsize = f.tellg();
			f.seekg(0, std::ios_base::beg);
			
			s.resize(fsize + 1);
			f.read(&s[0], fsize);
			f.close();
			s[fsize] = '\0';
			
			return s;
		}));
		
		std::map<
			std::string, 
			std::function<void(std::shared_ptr<binding::UnitData>, const json::JSON&)>
			> keyfuncs;
		
		keyfuncs.emplace("displayName", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->displayName = jsondata.to_string();
		});
		keyfuncs.emplace("meshFile", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->meshFile = jsondata.to_string();
		});
		keyfuncs.emplace("hp", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->hp = (unsigned)(jsondata.to_int());
		});
		keyfuncs.emplace("vision", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->vision = jsondata.to_float();
		});
		keyfuncs.emplace("mcost", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->mcost = (unsigned)(jsondata.to_int());
		});
		keyfuncs.emplace("ecost", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->ecost = (unsigned)(jsondata.to_int());
		});
		keyfuncs.emplace("time", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->time = (unsigned)(jsondata.to_int());
		});
		keyfuncs.emplace("tier", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->tier = (unsigned)(jsondata.to_int());
		});
		keyfuncs.emplace("size", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->size = (unsigned)(jsondata.to_int());
		});
		keyfuncs.emplace("targetedByAir", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->targetedByAir = jsondata.to_bool();
		});
		keyfuncs.emplace("targetedByLand", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->targetedByLand = jsondata.to_bool();
		});
		keyfuncs.emplace("targetedBySea", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->targetedBySea = jsondata.to_bool();
		});
		keyfuncs.emplace("buildsInLand", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->buildsInLand = jsondata.to_bool();
		});
		keyfuncs.emplace("buildsInSea", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->buildsInSea = jsondata.to_bool();
		});
		keyfuncs.emplace("buildsInAir", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->buildsInAir = jsondata.to_bool();
		});
		keyfuncs.emplace("buildsInFactory", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->buildsInFactory = jsondata.to_bool();
		});
		keyfuncs.emplace("amphibious", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->amphibious = jsondata.to_bool();
		});
		keyfuncs.emplace("capturable", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->capturable = jsondata.to_bool();
		});
		keyfuncs.emplace("crushes", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->crushes = jsondata.to_bool();
		});
		keyfuncs.emplace("explodes", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->explodes = jsondata.to_bool();
		});
		keyfuncs.emplace("hovers", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->hovers = jsondata.to_bool();
		});
		keyfuncs.emplace("invulnerable", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->invulnerable = jsondata.to_bool();
		});
		keyfuncs.emplace("regenerates", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->regenerates = jsondata.to_bool();
		});
		keyfuncs.emplace("repairable", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->repairable = jsondata.to_bool();
		});
		keyfuncs.emplace("assists", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->assists = jsondata.to_bool();
		});
		keyfuncs.emplace("builds", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->builds = jsondata.to_bool();
		});
		keyfuncs.emplace("captures", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->captures = jsondata.to_bool();
		});
		keyfuncs.emplace("reclaims", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->reclaims = jsondata.to_bool();
		});
		keyfuncs.emplace("repairs", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->repairs = jsondata.to_bool();
		});
		keyfuncs.emplace("sacrifices", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->sacrifices = jsondata.to_bool();
		});
		keyfuncs.emplace("submerges", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->submerges = jsondata.to_bool();
		});
		keyfuncs.emplace("transports", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->transports = jsondata.to_bool();
		});
		keyfuncs.emplace("teleports", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->teleports = jsondata.to_bool();
		});
		keyfuncs.emplace("movement", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			u->movement = std::make_shared<binding::MovementProfile>();
			if (jsondata.has_key("maxVelocity")) {
				u->movement->maxVelocity = jsondata.at("maxVelocity").to_float();
			}
			if (jsondata.has_key("acceleration")) {
				u->movement->acceleration = jsondata.at("acceleration").to_float();
			}
			if (jsondata.has_key("pivotSpeed")) {
				u->movement->pivotSpeed = jsondata.at("pivotSpeed").to_float();
			}
			if (jsondata.has_key("strafes")) {
				u->movement->strafes = jsondata.at("strafes").to_bool();
			}
			if (jsondata.has_key("minVelocity")) {
				u->movement->minVelocity = jsondata.at("minVelocity").to_float();
			}
			if (jsondata.has_key("fuel")) {
				u->movement->fuel = (unsigned)(jsondata.at("fuel").to_int());
			}
		});
		auto build_weapon = [](std::shared_ptr<binding::Weapon> w, const json::JSON& jsondata) {
			if (jsondata.has_key("weaponType")) {
				std::string wtstring = jsondata.at("weaponType").to_string();
				if (wtstring == "WEAPON_GUN") {
					w->weaponType = binding::Weapon::WeaponType::GUN;
				}
				else if (wtstring == "WEAPON_CANNON") {
					w->weaponType = binding::Weapon::WeaponType::CANNON;
				}
				else if (wtstring == "WEAPON_ARTILLERY") {
					w->weaponType = binding::Weapon::WeaponType::ARTILLERY;
				}
				else if (wtstring == "WEAPON_MISSILE") {
					w->weaponType = binding::Weapon::WeaponType::MISSILE;
				}
				else {
					std::cerr << "Invalid weapon type" << wtstring << "\n";
				}
			}
			if (jsondata.has_key("payload")) {
				std::string ptstring = jsondata.at("payload").to_string();
				if (ptstring == "PAYLOAD_BULLET") {
					w->payload = binding::Weapon::PayloadType::BULLET;
				}
				else if (ptstring == "PAYLOAD_BEAM") {
					w->payload = binding::Weapon::PayloadType::BEAM;
				}
				else if (ptstring == "PAYLOAD_BOMB") {
					w->payload = binding::Weapon::PayloadType::BOMB;
				}
				else if (ptstring == "PAYLOAD_SHELL") {
					w->payload = binding::Weapon::PayloadType::SHELL;
				}
				else {
					std::cerr << "Invalid payload type" << ptstring << "\n";
				}
			}
			if (jsondata.has_key("targeting")) {
				std::string ttstring = jsondata.at("targeting").to_string();
				if (ttstring == "TARGET_AIR") {
					w->targeting = binding::Weapon::TargetingType::AIR;
				}
				else if (ttstring == "TARGET_LAND") {
					w->targeting = binding::Weapon::TargetingType::LAND;
				}
				else if (ttstring == "TARGET_SEA") {
					w->targeting = binding::Weapon::TargetingType::SEA;
				}
				else {
					std::cerr << "Invalid targeting type" << ttstring << "\n";
				}
			}
			if (jsondata.has_key("damage")) {
				w->damage = jsondata.at("damage").to_float();
			}
			if (jsondata.has_key("armorMultiplier")) {
				w->armorMultiplier = jsondata.at("armorMultiplier").to_float();
			}
			if (jsondata.has_key("shieldMultiplier")) {
				w->shieldMultiplier = jsondata.at("shieldMultiplier").to_float();
			}
			if (jsondata.has_key("firerate")) {
				w->firerate = jsondata.at("firerate").to_float();
			}
			if (jsondata.has_key("duration")) {
				w->duration = jsondata.at("duration").to_float();
			}
			if (jsondata.has_key("cooldown")) {
				w->cooldown = jsondata.at("cooldown").to_float();
			}
			if (jsondata.has_key("velocity")) {
				w->velocity = jsondata.at("velocity").to_float();
			}
			if (jsondata.has_key("blastRadius")) {
				w->blastRadius = jsondata.at("blastRadius").to_float();
			}
			if (jsondata.has_key("range")) {
				w->range = jsondata.at("range").to_float();
			}
		};
		keyfuncs.emplace("weapons", 
			[build_weapon](
				std::shared_ptr<binding::UnitData> u, 
				const json::JSON& jsondata
			) {
			for (size_t i = 0; i < jsondata.length(); i++) {
				std::shared_ptr<binding::Weapon> w = std::make_shared<binding::Weapon>();
				build_weapon(w, jsondata.at(i));	
				u->addWeapon(w);
			}
		});
		keyfuncs.emplace("shield", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::Shield> a = std::make_shared<binding::Shield>();
			if (jsondata.has_key("hp")) {
				a->hp = (unsigned)(jsondata.at("hp").to_int());
			}
			if (jsondata.has_key("ecost")) {
				a->ecost = (unsigned)(jsondata.at("ecost").to_int());
			}
			if (jsondata.has_key("radius")) {
				a->radius = jsondata.at("radius").to_float();
			}
			u->addAccessory(binding::AccessoryType::SHIELD, a);
		});
		
		keyfuncs.emplace("cloak", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::Cloak> a = std::make_shared<binding::Cloak>();
			if (jsondata.has_key("ecost")) {
				a->ecost = (unsigned)(jsondata.at("ecost").to_int());
			}
			u->addAccessory(binding::AccessoryType::CLOAK, a);
		});
		keyfuncs.emplace("stealth", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::Stealth> a = std::make_shared<binding::Stealth>();
			if (jsondata.has_key("ecost")) {
				a->ecost = (unsigned)(jsondata.at("ecost").to_int());
			}
			u->addAccessory(binding::AccessoryType::STEALTH, a);
		});
		keyfuncs.emplace("jammer", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::Jammer> a = std::make_shared<binding::Jammer>();
			if (jsondata.has_key("ecost")) {
				a->ecost = (unsigned)(jsondata.at("ecost").to_int());
			}
			u->addAccessory(binding::AccessoryType::JAMMER, a);
		});
		keyfuncs.emplace("radar", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::Radar> a = std::make_shared<binding::Radar>();
			if (jsondata.has_key("ecost")) {
				a->ecost = (unsigned)(jsondata.at("ecost").to_int());
			}
			if (jsondata.has_key("radius")) {
				a->radius = jsondata.at("radius").to_float();
			}
			u->addAccessory(binding::AccessoryType::RADAR, a);
		});
		keyfuncs.emplace("sonar", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::Sonar> a = std::make_shared<binding::Sonar>();
			if (jsondata.has_key("ecost")) {
				a->ecost = (unsigned)(jsondata.at("ecost").to_int());
			}
			if (jsondata.has_key("radius")) {
				a->radius = jsondata.at("radius").to_float();
			}
			u->addAccessory(binding::AccessoryType::SONAR, a);
		});
		keyfuncs.emplace("omni", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::Omni> a = std::make_shared<binding::Omni>();
			if (jsondata.has_key("radius")) {
				a->radius = jsondata.at("radius").to_float();
			}
			u->addAccessory(binding::AccessoryType::OMNI, a);
		});
		keyfuncs.emplace("extractor", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::Extractor> a = std::make_shared<binding::Extractor>();
			if (jsondata.has_key("mgen")) {
				a->mgen = (unsigned)(jsondata.at("mgen").to_int());
			}
			if (jsondata.has_key("ecost")) {
				a->ecost = (unsigned)(jsondata.at("ecost").to_int());
			}
			u->addAccessory(binding::AccessoryType::EXTRACTOR, a);
		});
		keyfuncs.emplace("generator", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::Generator> a = std::make_shared<binding::Generator>();
			if (jsondata.has_key("egen")) {
				a->egen = (unsigned)(jsondata.at("egen").to_int());
			}
			u->addAccessory(binding::AccessoryType::GENERATOR, a);
		});
		keyfuncs.emplace("mstorage", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::MStorage> a = std::make_shared<binding::MStorage>();
			if (jsondata.has_key("mstor")) {
				a->mstor = (unsigned)(jsondata.at("mstor").to_int());
			}
			u->addAccessory(binding::AccessoryType::MSTORAGE, a);
		});
		keyfuncs.emplace("estorage", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::EStorage> a = std::make_shared<binding::EStorage>();
			if (jsondata.has_key("estor")) {
				a->estor = (unsigned)(jsondata.at("estor").to_int());
			}
			u->addAccessory(binding::AccessoryType::ESTORAGE, a);
		});
		keyfuncs.emplace("tmlauncher", 
			[build_weapon](
				std::shared_ptr<binding::UnitData> u, 
				const json::JSON& jsondata
			) {
			std::shared_ptr<binding::TMLauncher> a = std::make_shared<binding::TMLauncher>();
			if (jsondata.has_key("mcost")) {
				a->mcost = (unsigned)(jsondata.at("mcost").to_int());
			}
			if (jsondata.has_key("ecost")) {
				a->ecost = (unsigned)(jsondata.at("ecost").to_int());
			}
			if (jsondata.has_key("time")) {
				a->time = (unsigned)(jsondata.at("time").to_int());
			}
			if (jsondata.has_key("capacity")) {
				a->capacity = (unsigned)(jsondata.at("capacity").to_int());
			}
			if (jsondata.has_key("weapon")) {
				std::shared_ptr<binding::Weapon> w = std::make_shared<binding::Weapon>();
				build_weapon(w, jsondata.at("weapon"));
				a->missile = w;
			}
			u->addAccessory(binding::AccessoryType::TMLAUNCHER, a);
		});
		keyfuncs.emplace("smlauncher", 
			[build_weapon](
				std::shared_ptr<binding::UnitData> u, 
				const json::JSON& jsondata
			) {
			std::shared_ptr<binding::SMLauncher> a = std::make_shared<binding::SMLauncher>();
			if (jsondata.has_key("mcost")) {
				a->mcost = (unsigned)(jsondata.at("mcost").to_int());
			}
			if (jsondata.has_key("ecost")) {
				a->ecost = (unsigned)(jsondata.at("ecost").to_int());
			}
			if (jsondata.has_key("time")) {
				a->time = (unsigned)(jsondata.at("time").to_int());
			}
			if (jsondata.has_key("capacity")) {
				a->capacity = (unsigned)(jsondata.at("capacity").to_int());
			}
			if (jsondata.has_key("weapon")) {
				std::shared_ptr<binding::Weapon> w = std::make_shared<binding::Weapon>();
				build_weapon(w, jsondata.at("weapon"));
				a->missile = w;
			}
			u->addAccessory(binding::AccessoryType::SMLAUNCHER, a);
		});
		keyfuncs.emplace("factory", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::Factory> a = std::make_shared<binding::Factory>();
			if (jsondata.has_key("tier")) {
				unsigned int tl = (unsigned)(jsondata.at("tier").to_int());
				switch (tl) {
					case 1:
						a->tier = binding::Tier::TIER_1;
						break;
					case 2:
						a->tier = binding::Tier::TIER_2;
						break;
					case 3:
						a->tier = binding::Tier::TIER_3;
						break;
					case 4:
						a->tier = binding::Tier::TIER_4;
						break;
					default:
						break;
				}
			}
			if (jsondata.has_key("buildsLand")) {
				a->buildsLand = jsondata.at("buildsLand").to_bool();
			}
			if (jsondata.has_key("buildsSea")) {
				a->buildsSea = jsondata.at("buildsSea").to_bool();
			}
			if (jsondata.has_key("buildsAir")) {
				a->buildsAir = jsondata.at("buildsAir").to_bool();
			}
			if (jsondata.has_key("buildRate")) {
				a->buildRate = (unsigned)(jsondata.at("buildRate").to_int());
			}
			u->addAccessory(binding::AccessoryType::FACTORY, a);
		});
		keyfuncs.emplace("engineering", [](std::shared_ptr<binding::UnitData> u, const json::JSON& jsondata) {
			std::shared_ptr<binding::Engineering> a = std::make_shared<binding::Engineering>();
			if (jsondata.has_key("tier")) {
				unsigned int tl = (unsigned)(jsondata.at("tier").to_int());
				switch (tl) {
					case 1:
						a->tier = binding::Tier::TIER_1;
						break;
					case 2:
						a->tier = binding::Tier::TIER_2;
						break;
					case 3:
						a->tier = binding::Tier::TIER_3;
						break;
					case 4:
						a->tier = binding::Tier::TIER_4;
						break;
					default:
						break;
				}
			}
			if (jsondata.has_key("radius")) {
				a->radius = jsondata.at("radius").to_float();
			}
			u->addAccessory(binding::AccessoryType::ENGINEERING, a);
		});
		
		// Build a UnitData "factory" from a JSON string
		utils["JSONtoUnit"] = chaiscript::var(chaiscript::fun([keyfuncs](const std::string& data) {
			
			// Construct a JSON object from the input string
			json::JSON jsondata = json::JSON::Load(data);
			
			// Create a UnitData object that we can pass around
			std::shared_ptr<binding::UnitData> u = std::make_shared<UnitData>();
			
			// Iterate over the JSON object's keys
			auto jsdatamap = jsondata.object_range();
			for (auto it = jsdatamap.begin(); it != jsdatamap.end(); it++) {
				// Look up keyvalues and call the proper handler to populate 'u'
				keyfuncs.at(it->first)(u, it->second);
			}
			
			// I have no idea if this works the way that I want it to...
			// 
			// In theory, it's supposed to capture the object referenced by
			// u by copy and then use that as an argument to the copy
			// constructor of UnitData to make future units of this type.
			return [ucpy = *u]() {
				return std::make_shared<UnitData>(ucpy);
			};
			
		}));
		
	};
	
	this->register_namespace(ns_utils, "utils");
}

