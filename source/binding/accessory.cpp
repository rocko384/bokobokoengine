
#include <binding/accessory.h>

using binding::AccessoryType;

AccessoryType binding::Shield::getAccessoryType() {
	return AccessoryType::SHIELD;
}

AccessoryType binding::Cloak::getAccessoryType() {
	return AccessoryType::CLOAK;
}

AccessoryType binding::Stealth::getAccessoryType() {
	return AccessoryType::STEALTH;
}

AccessoryType binding::Jammer::getAccessoryType() {
	return AccessoryType::JAMMER;
}

AccessoryType binding::Radar::getAccessoryType() {
	return AccessoryType::RADAR;
}

AccessoryType binding::Sonar::getAccessoryType() {
	return AccessoryType::SONAR;
}

AccessoryType binding::Omni::getAccessoryType() {
	return AccessoryType::OMNI;
}

AccessoryType binding::Extractor::getAccessoryType() {
	return AccessoryType::EXTRACTOR;
}

AccessoryType binding::Generator::getAccessoryType() {
	return AccessoryType::GENERATOR;
}

AccessoryType binding::MStorage::getAccessoryType() {
	return AccessoryType::MSTORAGE;
}

AccessoryType binding::EStorage::getAccessoryType() {
	return AccessoryType::ESTORAGE;
}

AccessoryType binding::TMLauncher::getAccessoryType() {
	return AccessoryType::TMLAUNCHER;
}

AccessoryType binding::SMLauncher::getAccessoryType() {
	return AccessoryType::SMLAUNCHER;
}

AccessoryType binding::Factory::getAccessoryType() {
	return AccessoryType::FACTORY;
}

AccessoryType binding::Engineering::getAccessoryType() {
	return AccessoryType::ENGINEERING;
}