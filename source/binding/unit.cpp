
#include <binding/unit.h>


binding::MovementProfile::MovementProfile() {
	;
}

binding::MovementProfile::~MovementProfile() {
	;
}

// Unit Class Bindings
binding::UnitData::UnitData() {
	;
}

binding::UnitData::~UnitData() {
	;
}

void binding::UnitData::setMovement(std::shared_ptr<binding::MovementProfile> p) {
	this->movement = p;
}

void binding::UnitData::addWeapon(std::shared_ptr<Weapon> p) {
	this->weapons.push_back(p);
}

void binding::UnitData::addAccessory(binding::AccessoryType t, std::shared_ptr<binding::Accessory> p) {
	this->accessories.insert(std::make_pair(t, p));
}

void binding::UnitData::setDeathCallback(std::shared_ptr<std::function<void(std::shared_ptr<UnitData>)>> cb) {
	this->callback = cb;
}


binding::Unit::Unit(std::shared_ptr<UnitData> ud) {
	info = ud;
	node = std::make_shared<dankengine::SceneNode>();
}

binding::Unit::~Unit() {
	
}

