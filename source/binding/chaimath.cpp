#include <binding/binding.h>
#include <chaiscript/chaiscript_stdlib.hpp>

#include <cmath>

// I'm gonna be putting these all over the place but they're going out 
// of scope pretty quick so there's little point in const-ing them.
#define PI_FLOAT	3.14159265f
#define PI_DOUBLE	3.14159265358979323

void binding::ChaiContext::BindMath() {
	
	auto ns_math = [](chaiscript::Namespace& math) {
		
		math["fpi"] = chaiscript::const_var(PI_FLOAT);
		math["dpi"] = chaiscript::const_var(PI_DOUBLE);
		math["pi"] = math["dpi"];
		math["nan"] = chaiscript::const_var(NAN);
		math["inf"] = chaiscript::const_var(INFINITY);
		
		// Get degrees from radians
		math["deg"] = chaiscript::var(chaiscript::fun([](double x) {
			return x * (180.0 / PI_DOUBLE);
		}));
		
		// Get radians from degrees
		math["rad"] = chaiscript::var(chaiscript::fun([](double x) {
			return x * (PI_DOUBLE / 180.0);
		}));
		
		// C Standard Library Trig 
		// We'll provide multiple precisions because these could be
		// used a lot ... (C++ should auto-deduce the return type)
		
		// Double Variants
		math["sin"] = chaiscript::var(chaiscript::fun([](double x) {
			return std::sin(x);
		}));
		math["cos"] = chaiscript::var(chaiscript::fun([](double x) {
			return std::cos(x);
		}));
		math["tan"] = chaiscript::var(chaiscript::fun([](double x) {
			return std::tan(x);
		}));
		math["asin"] = chaiscript::var(chaiscript::fun([](double x) {
			return std::asin(x);
		}));
		math["acos"] = chaiscript::var(chaiscript::fun([](double x) {
			return std::acos(x);
		}));
		math["atan"] = chaiscript::var(chaiscript::fun([](double x) {
			return std::atan(x);
		}));
		math["atan2"] = chaiscript::var(chaiscript::fun([](double y, double x) {
			return std::atan2(y, x);
		}));
		
		// Float Variants
		math["sinf"] = chaiscript::var(chaiscript::fun([](float x) {
			return std::sin(x);
		}));
		math["cosf"] = chaiscript::var(chaiscript::fun([](float x) {
			return std::cos(x);
		}));
		math["tanf"] = chaiscript::var(chaiscript::fun([](float x) {
			return std::tan(x);
		}));
		math["asinf"] = chaiscript::var(chaiscript::fun([](float x) {
			return std::asin(x);
		}));
		math["acosf"] = chaiscript::var(chaiscript::fun([](float x) {
			return std::acos(x);
		}));
		math["atanf"] = chaiscript::var(chaiscript::fun([](float x) {
			return std::atan(x);
		}));
		math["atan2f"] = chaiscript::var(chaiscript::fun([](float y, float x) {
			return std::atan2(y, x);
		}));
		
		// Other Math (double)
		math["sqrt"] = chaiscript::var(chaiscript::fun([](double x) {
			return std::sqrt(x);
		}));
		math["abs"] = chaiscript::var(chaiscript::fun([](double x) {
			return std::abs(x);
		}));
		math["pow"] = chaiscript::var(chaiscript::fun([](double x, double y) {
			return std::pow(x, y);
		}));
		
		// Other Math (float)
		math["sqrtf"] = chaiscript::var(chaiscript::fun([](float x) {
			return std::sqrt(x);
		}));
		math["absf"] = chaiscript::var(chaiscript::fun([](float x) {
			return std::abs(x);
		}));
		math["powf"] = chaiscript::var(chaiscript::fun([](float x, float y) {
			return std::pow(x, y);
		}));

	};
	
	this->register_namespace(ns_math, "math");
	
}
