
#include <binding/binding.h>
#include <chaiscript/chaiscript_stdlib.hpp>

using binding::AccessoryType;
using binding::UnitData;
using binding::MovementProfile;
using binding::Weapon;
using binding::Faction;


binding::ChaiContext::ChaiContext(const std::string& data_path) 
	: binding_interface{data_path} {
	
	this->add(chaiscript::user_type<binding::Binding>(), "Binding");
	
	// Chaiscript namespaces and utilities
	this->BindMath();
	this->BindUtils();
	
	chaiscript::ModulePtr m = chaiscript::ModulePtr(new chaiscript::Module());
	
	// Defined Symbols
	this->BindEnums();
	this->BindAccessories(m);	
	this->BindTypes(m);
	
	this->add(m);
	
	this->add(chaiscript::var(std::ref(binding_interface)), "binding");
	this->add(
		chaiscript::fun(&binding::Binding::registerUnitFunc), "registerUnitFunc");
	this->add(chaiscript::fun(&binding::Binding::addFaction), "addFaction");
	this->add(chaiscript::fun(&binding::Binding::getDataPath), "getDataPath");
}

binding::ChaiContext::~ChaiContext() {
	;
}

void binding::ChaiContext::BindEnums() {
	
	// Accessory Constants
	this->add_global_const(
		chaiscript::const_var(AccessoryType::SHIELD), "ACCESSORY_SHIELD");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::CLOAK), "ACCESSORY_CLOAK");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::STEALTH), "ACCESSORY_STEALTH");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::JAMMER), "ACCESSORY_JAMMER");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::RADAR), "ACCESSORY_RADAR");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::SONAR), "ACCESSORY_SONAR");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::OMNI), "ACCESSORY_OMNI");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::EXTRACTOR), "ACCESSORY_EXTRACTOR");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::GENERATOR), "ACCESSORY_GENERATOR");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::MSTORAGE), "ACCESSORY_MSTORAGE");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::ESTORAGE), "ACCESSORY_ESTORAGE");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::TMLAUNCHER), "ACCESSORY_TMLAUNCHER");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::SMLAUNCHER), "ACCESSORY_SMLAUNCHER");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::FACTORY), "ACCESSORY_FACTORY");
	this->add_global_const(
		chaiscript::const_var(AccessoryType::ENGINEERING), "ACCESSORY_ENGINEERING");
	
		
	this->add(chaiscript::fun(
		[](const AccessoryType lhs, const AccessoryType rhs) {
			return (lhs==rhs);
		}),"==");
	this->add(chaiscript::fun(
		[](AccessoryType& lhs, const AccessoryType rhs) {
			return (lhs=rhs);
		}), "=");
		
	this->add_global_const(
		chaiscript::const_var(Tier::TIER_1), "TIER_1");
	this->add_global_const(
		chaiscript::const_var(Tier::TIER_1), "TIER_2");
	this->add_global_const(
		chaiscript::const_var(Tier::TIER_1), "TIER_3");
	this->add_global_const(
		chaiscript::const_var(Tier::TIER_1), "TIER_4");
		
	this->add(chaiscript::fun(
		[](const Tier lhs, const Tier rhs) {
			return (lhs==rhs);
		}),"==");
	this->add(chaiscript::fun(
		[](Tier& lhs, const Tier rhs) {
			return (lhs==rhs);
		}),"=");
	
	// Weapon Constants
	this->add_global_const(
		chaiscript::const_var(Weapon::WeaponType::GUN), "WEAPON_GUN");
	this->add_global_const(
		chaiscript::const_var(Weapon::WeaponType::CANNON), "WEAPON_CANNON");
	this->add_global_const(
		chaiscript::const_var(Weapon::WeaponType::ARTILLERY), "WEAPON_ARTILLERY");
	this->add_global_const(
		chaiscript::const_var(Weapon::WeaponType::MISSILE), "WEAPON_MISSILE");
		
	this->add(chaiscript::fun(
		[](const Weapon::WeaponType lhs, const Weapon::WeaponType rhs) {
			return (lhs==rhs);
		}),"==");
	this->add(chaiscript::fun(
		[](Weapon::WeaponType& lhs, const Weapon::WeaponType rhs) {
			return (lhs=rhs);
		}), "=");
		
	// Weapon Payload Constants
	this->add_global_const(
		chaiscript::const_var(Weapon::PayloadType::BULLET), "PAYLOAD_BULLET");
	this->add_global_const(
		chaiscript::const_var(Weapon::PayloadType::BEAM), "PAYLOAD_BEAM");
	this->add_global_const(
		chaiscript::const_var(Weapon::PayloadType::BOMB), "PAYLOAD_BOMB");
	this->add_global_const(
		chaiscript::const_var(Weapon::PayloadType::SHELL), "PAYLOAD_SHELL");
		
	this->add(chaiscript::fun(
		[](const Weapon::PayloadType lhs, const Weapon::PayloadType rhs) {
			return (lhs==rhs);
		}),"==");
	this->add(chaiscript::fun(
		[](Weapon::PayloadType& lhs, const Weapon::PayloadType rhs) {
			return (lhs=rhs);
		}), "=");
		
	// Weapon Targeting Constants
	this->add_global_const(
		chaiscript::const_var(Weapon::TargetingType::AIR), "TARGET_AIR");
	this->add_global_const(
		chaiscript::const_var(Weapon::TargetingType::LAND), "TARGET_LAND");
	this->add_global_const(
		chaiscript::const_var(Weapon::TargetingType::SEA), "TARGET_SEA");
		
	this->add(chaiscript::fun(
		[](const Weapon::TargetingType lhs, const Weapon::TargetingType rhs) {
			return (lhs==rhs);
		}),"==");
	this->add(chaiscript::fun(
		[](Weapon::TargetingType& lhs, const Weapon::TargetingType rhs) {
			return (lhs=rhs);
		}), "=");
	
}

binding::Binding& binding::ChaiContext::getBindings() {
	return binding_interface;
}

