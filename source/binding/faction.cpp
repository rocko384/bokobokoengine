
#include <binding/faction.h>

binding::Faction::Faction() {
	;
}

binding::Faction::~Faction() {
	;
}

void binding::Faction::addUnitKey(const Tier& tl, const std::string& name) {
	if (keys.size() < (unsigned int)tl + 1) {
		keys.resize((unsigned int)tl + 1);
	}
	keys[(unsigned int)tl].push_back(name);
}


const std::vector<std::string>& binding::Faction::getTierList(const Tier& tl) {
	return keys[(unsigned int)tl];
}


