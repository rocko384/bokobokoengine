
#include <binding/terrain.h>

#include <misc/pool.hpp>

#include <functional>
#include <iostream>
#include <thread>

binding::Terrain::Terrain(const std::string& path) : heightmap(path) {
	;
}

binding::Terrain::Terrain(const std::filesystem::path& path) : heightmap(path) {
	;
}

void binding::Terrain::open(const std::string& path) {
	heightmap.loadFromFile(path);
}

void binding::Terrain::setInterpolationFunction(
	std::function<void(
		glm::vec3*, 
		const dankengine::HeightMap&, 
		unsigned int x, 
		unsigned int y
	)> f
) {
	interpolate = f;
}

std::shared_ptr<dankengine::Mesh> binding::Terrain::getMesh(unsigned int lod) {
	if (meshes.count(lod) == 0) {
		generate(lod);
	}
	return meshes[lod];
}


// Generate a terrain mesh at the given level of detail using the currently-set
// interpolation function
void binding::Terrain::generate(unsigned int lod) {
	
	// Because interpolation is very complex, ::generate accelerates the
	// process by using a thread pool.
	
	std::vector<std::thread> threads;
	std::vector<glm::vec3> vertices;
	
	unsigned int hmax = heightmap.getHeight();
	unsigned int wmax = heightmap.getWidth();
	vertices.resize(wmax * hmax);
	
	
	auto interp_worker = [this, lod](glm::vec3* v, unsigned int x, unsigned int y) {
		if (this->isCanonical(lod, x, y)) {
			v->x = x;
			v->z = y;
			dankengine::Pixel<float> px = this->heightmap.getPixel<float>(x, y);
		}
		else {
			(this->interpolate)(v, this->heightmap, x, y);
		}
	};

	pool<glm::vec3*, unsigned int, unsigned int> interp_pool(8);
	interp_pool.set_worker(std::move(interp_worker));
	interp_pool.start();
	
	std::cout << "Interpolating terrain vertices...\n";
	for (unsigned int j = 0; j < hmax; j++) {
		for (unsigned int i = 0; i < wmax; i++) {
			unsigned int offset = j * wmax + i;
			interp_pool.push(&vertices[offset], i, j);
		}
	}
	
	interp_pool.stop();
	
	// Calculate the number of triangles needed for the terrain mesh (squares * 2)
	size_t num_tris = (hmax - 1) * (wmax - 1) * 2;
	
	// Allocate enough room for however many triangles ahead of time
	// There are three indices per triangle
	std::vector<unsigned int> indices(num_tris * 3);
	
	// The shared hypotenuse of the triangles will run from top-left to bottom-
	// right; we will refer to the abutting triangles as NE and SW. (Remember
	// to draw vertices anticlockwise)
	
	auto index_worker = [=, &indices](size_t idx, size_t x, size_t y) {
		indices[idx]     = (y)   * wmax + (x);
		indices[idx + 1] = (y+1) * wmax + (x+1);
		indices[idx + 2] = (y)   * wmax + (x+1);
		
		// SE triangle from top left, anticlockwise
		indices[idx + 3] = (y)   * wmax + (x);
		indices[idx + 4] = (y+1) * wmax + (x);
		indices[idx + 5] = (y+1) * wmax + (x+1);
	};
	
	pool<size_t, size_t, size_t> index_pool(8);
	index_pool.set_worker(std::move(index_worker));
	index_pool.start();
	
	std::cout << "Triangulating terrain...\n";
	size_t index = 0;
	for (unsigned int j = 0; j < hmax - 1; j++) {
		for (unsigned int i = 0; i < wmax - 1; i++) {
			index_pool.push(index, i, j);
			index += 6;
		}
	}
	
	index_pool.stop();
	
	// Create FACE normals based on the indices
	
	std::vector<glm::vec3> face_normals(num_tris);
	
	auto fnormal_worker = [&](size_t tri) {
		glm::vec3 A = vertices[indices[tri*3]];
		glm::vec3 B = vertices[indices[tri*3+1]];
		glm::vec3 C = vertices[indices[tri*3+2]];
		face_normals[tri] = glm::normalize(glm::cross(B - A, C - A));
	};
	
	pool<size_t> fnormal_pool(8);
	fnormal_pool.set_worker(std::move(fnormal_worker));
	fnormal_pool.start();
	
	std::cout << "Calculating face normals...\n";
	for (size_t i = 0; i < num_tris; i++) {
		fnormal_pool.push(i);
	}
	
	fnormal_pool.stop();
	
	// Create VERTEX normals by averaging face normals (I guess)
	
	std::vector<glm::vec3> vertex_normals(vertices.size());
	
	auto vnormal_worker = [&](size_t x, size_t y) {
		
		size_t horiz_tris = (wmax - 1) * 2;
		
		// Each (internal) vertex has an edge going N, S, E, W, NW, and SE
		// The triangles (going clockwise) are NE, ESE, SSE, SW, WNW, and NNW
		glm::vec3 normal = face_normals[y * horiz_tris + (x*2)]; // ESE
		normal += face_normals[y * horiz_tris + (x*2) + 1]; // SSE
		normal += face_normals[y * horiz_tris + ((x-1)*2)]; // SW
		normal += face_normals[(y-1) * horiz_tris + ((x-1)*2)+1]; // WNW
		normal += face_normals[(y-1) * horiz_tris + ((x-1)*2)]; // NNW
		normal += face_normals[(y-1) * horiz_tris + (x*2)+1]; // NW
		
		vertex_normals[y * wmax + x] = normal * float(1.0/6.0);
	};
	
	pool<size_t, size_t> vnormal_pool(8);
	vnormal_pool.set_worker(std::move(vnormal_worker));
	vnormal_pool.start();
	
	std::cout << "Averaging face normals to create vertex normals...\n";
	// We need to skip the outer perimeter because it won't average properly
	for (size_t j = 1; j < hmax - 1; j++) {
		for (size_t i = 1; i < wmax - 1; i++) {
			vnormal_pool.push(i, j);
		}
	}
	
	vnormal_pool.stop();
	
	// Emplace vertical normals along the edges of the map
	for (size_t i = 0; i < wmax; i++) {
		vertex_normals[i] = glm::vec3(0.f, 1.f, 0.f);
		vertex_normals[(hmax - 1) * wmax + i] = glm::vec3(0.f, 1.f, 0.f);
	}
	for (size_t j = 1; j < hmax - 1; j++) {
		vertex_normals[j * wmax] = glm::vec3(0.f, 1.f, 0.f);
		vertex_normals[j * wmax + wmax - 1] = glm::vec3(0.f, 1.f, 0.f);
	}
	
	// perform additional processing ...
	// we need texture coords from a tileset and splash map
	// This can be a TODO for later or something
	
	// Create the mesh object for this Level of Detail
	meshes[lod] = std::make_shared<dankengine::Mesh>();
	
	std::cout << "Creating terrain mesh...\n";
	for (size_t i = 0; i < vertices.size(); i++) {
		meshes[lod]->pushVertex(vertices[i], vertex_normals[i], glm::vec3(0.5f, 0.5f, 0.5f), glm::vec2(0.f, 0.f));
	}
	
	// Set the index buffer
	meshes[lod]->setIndices(indices);
	
	meshes[lod]->compileVertices();
}


// Determine determine whether or not a point is a '#' given the pattern
// displayed below. This should scale to any given LOD, probably...
// 
//  LOD 1
//  # - # - # - # - # - # - #
//  - # - # - # - # - # - # -
//  # - # - # - # - # - # - #
//  - # - # - # - # - # - # -
//  # - # - # - # - # - # - #
//  
//  LOD 2
//  # . - . # . - . # . - . #
//  . . . . . . . . . . . . .
//  - . # . - . # . - . # . -
//  . . . . . . . . . . . . .
//  # . - . # . - . # . - . #
//  
//  LOD 3
//  
//  # . . - . . # . . - . . #
//  . . . . . . . . . . . . .
//  . . . . . . . . . . . . .
//  - . . # . . - . . # . . -
//  . . . . . . . . . . . . .
//  . . . . . . . . . . . . .
//  # . . - . . # . . - . . #
bool binding::Terrain::isCanonical(unsigned int lod, unsigned int x, unsigned int y) {
	
	if (x >= heightmap.getWidth() || y >= heightmap.getHeight()) {
		return false;
	}
	
	if (y % lod) {
		return false;
	}
	
	// aligned case
	if (y % (lod * 2)) {
		if (x % lod) {
			return false;
		}
		
		if (x % (lod * 2)) {
			return true;
		}
		else {
			return false;
		}
	}
	// offset case
	else {
		if (x % lod) {
			return false;
		}
		
		if (x % (lod * 2)) {
			return false;
		}
		else {
			return true;
		}
	}
}

// Remember, image coordinates are xy, but terrain coordinates are xz
void binding::Terrain::NONE(
	glm::vec3* v, const dankengine::HeightMap& hm, unsigned int x, unsigned int z
) {
	v->x = x;
	v->z = z;
	
	// heightmaps are grayscale, we can use any non-alpha value here
	dankengine::Pixel<float> px = hm.getPixel<float>(x, z);
	v->y = px.r;
}


void binding::Terrain::LINEAR(
	glm::vec3* v, const dankengine::HeightMap& hm, unsigned int x, unsigned int z
) {
	
	v->x = x;
	v->z = z;
	
	unsigned int w = hm.getWidth();
	unsigned int h = hm.getHeight();
	
	unsigned int zm1 = z > 0 ? z - 1 : 0;
	unsigned int zp1 = z < h - 1 ? z + 1 : h - 1;
	unsigned int xm1 = x > 0 ? x - 1 : 0;
	unsigned int xp1 = x < w - 1 ? x + 1 : w - 1;
	
	// For lines in the Z direction...
	glm::vec2 zminus(zm1, hm.getPixel<float>(x, zm1).r);
	glm::vec2 zplus(zp1, hm.getPixel<float>(x, zp1).r);
	float yz = POINT_ON_LINE(z, zplus, zminus);
	
	// For lines in the X direction...
	glm::vec2 xminus(xm1, hm.getPixel<float>(xm1, z).r);
	glm::vec2 xplus(xp1, hm.getPixel<float>(xp1, z).r);
	float yx = POINT_ON_LINE(x, xplus, xminus);
	
	float yy = hm.getPixel<float>(x, z).r;
	
	v->y = (yy + yx + yz) / 3.0;
}


void binding::Terrain::CUBIC(
	glm::vec3* v, const dankengine::HeightMap& hm, unsigned int x, unsigned int z
) {
	
	v->x = x;
	v->z = z;
	
	unsigned int w = hm.getWidth();
	unsigned int h = hm.getHeight();
	
	float yy = hm.getPixel<float>(x, z).r;
	
	// In event that x/z is on an edge, these are left unchanged
	float yz = yy;
	float yx = yy;
	
	glm::vec2 zminus(z - 1, hm.getPixel<float>(x, z - 1).r);
	glm::vec2 zplus(z + 1, hm.getPixel<float>(x, z + 1).r);
	
	// handle the near-edge cases for Z
	if ( z == 1 || z == (h - 2)) {
		yz = POINT_ON_LINE(z, zplus, zminus);
	}
	// handle all other (non-edge) cases for Z
	else if ((z != 0) && (z != h - 1)) {
		glm::vec2 zminus2(z - 2, hm.getPixel<float>(x, z - 2).r);
		glm::vec2 zplus2(z + 2, hm.getPixel<float>(x, z + 2).r);
		yz = POINT_ON_CUBIC(z, zplus2, zplus, zminus, zminus2);
	}
	
	glm::vec2 xminus(x - 1, hm.getPixel<float>(x - 1, z).r);
	glm::vec2 xplus(x + 1, hm.getPixel<float>(x + 1, z).r);
	
	// handle the near-edge cases for X
	if (x == 1 || x == (w - 2)) {
		yx = POINT_ON_LINE(x, xplus, xminus);
	}
	// handle all other (non-edge) cases for X
	else if ((z != 0) && (z != h - 1)) {
		glm::vec2 xminus2(x - 2, hm.getPixel<float>(x - 2, z).r);
		glm::vec2 xplus2(x + 2, hm.getPixel<float>(x + 2, z).r);
		yx = POINT_ON_CUBIC(x, xplus2, xplus, xminus, xminus2);
	}
	
	v->y = (yy + yx + yz) / 3.0;
}

// Return the value of f(t) as defined by pt1---pt2 at the given t
float binding::Terrain::POINT_ON_LINE(
	float t, const glm::vec2& pt1, const glm::vec2& pt0
) {
	float m = (pt1.y - pt0.y) / (pt1.x - pt0.x);
	return m * (t - pt1.x) + pt1.y;
}

// Return the value of f(t) at the given t
float binding::Terrain::POINT_ON_CUBIC(
	float t, 
	const glm::vec2& pt3, const glm::vec2& pt2, 
	const glm::vec2& pt1, const glm::vec2& pt0
) {
	// Get the slopes of lines 3_2 and 1_0
	float m32 = (pt3.y - pt2.y) / (pt3.x - pt2.x);
	float m10 = (pt1.y - pt0.y) / (pt1.x - pt0.x);
	
	// // Given the following form of a cubic polynomial...
	// f(x) = ax^3 + bx^2 + cx + d
	// 
	// // We get the following equations
	// a*(pt2.x)^3 + b*(pt2.x)^2 + c*(pt2.x) + d = pt2.y
	// a*(pt1.x)^3 + b*(pt1.x)^2 + c*(pt1.x) + d = pt1.y
	// 
	// // Given the derivative of the polynomial...
	// 3ax^2 + 2bx + c = f'(x)
	// 
	// // The we get these equations
	// 3*a*(pt2.x)^2 + 2*b*(pt2.x) + c = m32 // because f'(pt2.x) = m32 for our spline
	// 3*a*(pt1.x)^2 + 2*b*(pt1.x) + c = m10 // because f'(pt1.x) = m10 for our spline
	// 
	// // Expressed as a system of equations...
	// {    a*(pt2.x)^3 +   b*(pt2.x)^2 + c*(pt2.x) + d = pt2.y
	// |    a*(pt1.x)^3 +   b*(pt1.x)^2 + c*(pt1.x) + d = pt1.y
	// |  3*a*(pt2.x)^2 + 2*b*(pt2.x)   + c             = m32
	// {  3*a*(pt1.x)^2 + 2*b*(pt1.x)   + c             = m10
	// 
	// // Let's solve some shit...
	// c = ((-3)*(pt1.x)^2)*a + ((-2)*(pt1.x))*b + m10
	// 
	// E = ((-3)*(pt1.x)^2)
	// F = ((-2)*(pt1.x))
	float E = (-3)*(pt1.x*pt1.x);
	float F = (-2)*(pt1.x);
	
	// c = E*a + F*b + m10
	// 
	// (3*(pt2.x)^2)*a + (2*(pt2.x))*b + E*a + F*b + m10 = m32
	// ((3*(pt2.x)^2) + E)*a + ((2*(pt2.x)) + F)*b = (m32 - m10)
	// 
	// G = ((3*(pt2.x)^2) + E)
	// H = ((2*(pt2.x)) + F)
	float G = (3*pt2.x*pt2.x) + E;
	float H = (2*pt2.x) + F;
	
	// G*a - (m32 - m10) = -H*b
	// b = (G/-H)*a - ((m32 - m10)/-H)
	// 
	// I = (G/-H)
	// J = ((m32 - m10)/-H)
	float I = G/(-H);
	float J = (m32 - m10)/(-H);
	
	// b = I*a - J
	// 
	// ((pt1.x)^3)*a + ((pt1.x)^2)*(I*a - J) + (pt1.x)*(E*a + F*(I*a - J)) + d = pt1.y
	// ((pt1.x)^3)*a + ((pt1.x)^2)*I*a - ((pt1.x)^2)*J + (pt1.x)*E*a + (pt1.x)*F*I*a - (pt1.x)*F*J + d = pt1.y
	// 
	// K = (pt1.x)^3
	// L = ((pt1.x)^2)*I
	// M = ((pt1.x)^2)*J
	// N = (pt1.x)*E
	// O = (pt1.x)*F
	float K = pt1.x * pt1.x * pt1.x;
	float L = pt1.x * pt1.x * I;
	float M = pt1.x * pt1.x * J;
	float N = pt1.x * E;
	float O = pt1.x * F;
	
	// K*a + L*a - M + N*a + O*I*a - O*J + d = pt1.y
	// (K+L+N+(O*I))*a - M - O*J - pt1.y = -d
	// d = -(K+L+N+(O*I))*a + M + O*J + pt1.y
	// 
	// ((pt2.x)^3)*a + ((pt2.x)^2)(I*a - J) + (pt2.x)*(E*a + F*(I*a - J)) - (K+L+N+(O*I))*a + M + O*J + pt1.y = pt2.y
	// ((pt2.x)^3)*a + ((pt2.x)^2)*I*a - ((pt2.x)^2)*J + (pt2.x)*E*a + (pt2.x)*F*I*a - (pt2.x)*F*J - (K+L+N+(O*I))*a + M + O*J + pt1.y = pt2.y
	// 
	// P = (pt2.x)^3
	// Q = ((pt2.x)^2)*I
	// R = ((pt2.x)^2)*J
	// S = (pt2.x)*E
	// T = (pt2.x)*F
	float P = pt2.x * pt2.x * pt2.x;
	float Q = pt2.x * pt2.x * I;
	float R = pt2.x * pt2.x * J;
	float S = pt2.x * E;
	float T = pt2.x * F;
	
	// P*a + Q*a - R + S*a + T*I*a - T*J - (K+L+N+(O*I))*a + M + O*J + pt1.y = pt2.y
	// (P + Q + S + T*I - (K+L+N+(O*I)))*a - R - T*J + M + O*J + pt1.y = pt2.y
	// (P + Q + S + T*I - (K+L+N+(O*I)))*a = pt2.y + R + T*J - M - O*J - pt1.y
	// a = (pt2.y + R + T*J - M - O*J - pt1.y) / (P + Q + S + T*I - (K+L+N+(O*I)))

	float a = (pt2.y + R + T*J - M - O*J - pt1.y) / (P + Q + S + T*I - K + L + N + O*I);
	
	// recall...
	// b = I*a - J
	float b = I*a - J;
	
	// recall...
	// c = E*a + F*b + m10
	float c = E*a + F*b + m10;
	
	// recall...
	// d = -(K+L+N+(O*I))*a + M + O*J + pt1.y
	float d = (-1)*(K + L + N + O*I)*a + M + O*J + pt1.y;
	
	// And lastly...
	// f(x) = ax^3 + bx^2 + cx + d
	return (a * t*t*t) + (b * t*t) + (c * t) + d;
}

