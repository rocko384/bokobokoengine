#pragma once

namespace binding {

	class Weapon {	
	public:
		
		enum WeaponType {
			GUN,
			CANNON,
			ARTILLERY,
			MISSILE,
		};
		
		enum PayloadType {
			BULLET,
			BEAM,
			BOMB,
			SHELL,
		};
		
		enum TargetingType {
			AIR,
			LAND,
			SEA,
		};
		
		Weapon();
		~Weapon();
		
		WeaponType weaponType;
		PayloadType payload;
		TargetingType targeting;
		
		// Multipliers
		float damage; // amount per shot
		float armorMultiplier; // damage multiplier applied to armored targets
		float shieldMultiplier; // damage multiplier applied to shields
		
		// Rate Stats
		float firerate; // projectiles per second
		float duration; // amount of time spent firing before cooldown (seconds)
		float cooldown; // idle time between firing cycles (seconds)
		
		// Projectile Info
		float velocity; // projectile speed
		float blastRadius; // how big is the impact area
		float range;
		
	};
	
}
