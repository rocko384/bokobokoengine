#pragma once

#include <binding/terrain.h>
#include <binding/structure.h>
#include <binding/weapon.h>
#include <binding/unit.h>
#include <binding/accessory.h>
#include <binding/faction.h>

#include <chaiscript/chaiscript.hpp>

#include <engine/graphics/3D/mesh.h>

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>

namespace binding {

	// Provided by binding.cpp
	class Binding {
	public:
		Binding(const std::string& data_path);
		~Binding();
		
		bool registerUnitFunc(const std::string&, std::function<std::shared_ptr<UnitData>()>);
		bool addFaction(const std::string&, std::shared_ptr<Faction>);
		std::shared_ptr<Unit> createNewUnit(const std::string&);
		const std::string& getDataPath();
		
		void printFactionStats(const std::string&, const binding::Tier&);
		
	private:
		std::unordered_map<std::string, std::function<std::shared_ptr<UnitData>()>> unit_creators;
		std::unordered_map<std::string, std::shared_ptr<Faction>> factions;
		
		// List of currentload loaded meshes for tying
		std::unordered_map<std::string, std::shared_ptr<dankengine::Mesh>> meshes;
		
		std::string data_path;
	};
	
	// Provided by chai.cpp
	class ChaiContext : public chaiscript::ChaiScript {
	public:
		ChaiContext(const std::string& data_path);
		~ChaiContext();
		
		Binding& getBindings();
	private:
		Binding binding_interface;
		void BindEnums();
		void BindMath(); // provided by chaimath.cpp
		void BindTypes(chaiscript::ModulePtr); // provided by chaitypes.cpp 
		void BindAccessories(chaiscript::ModulePtr); // provided by chaitypes.cpp
		void BindUtils(); // provided by chaiutils.cpp
	};

}
