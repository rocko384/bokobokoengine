#pragma once

#include <binding/accessory.h>

#include <engine/graphics/scene/scenenode.h>

#include <functional>
#include <map>
#include <memory>
#include <vector>

namespace binding {

	class MovementProfile {
	public:
		
		MovementProfile();
		~MovementProfile();
		
		float maxVelocity;
		float acceleration;
		float pivotSpeed; // (rad/s)
		// note: in flight, pivotSpeed applies to pitch, yaw, and roll
		
		bool strafes;
		
		float minVelocity; // minimum velocity when in flight
		unsigned int fuel; // total fuel in seconds of flight
		
	};
	
	class UnitData {
	public:
		
		UnitData();
		~UnitData();
		
		std::string displayName;
		std::string meshFile;
		
		// Stats
		unsigned int hp;
		float vision;
		
		// Build Info
		unsigned int mcost;
		unsigned int ecost;
		unsigned int time; // total build time in seconds
		unsigned int tier;
		
		// Transport Info
		unsigned int size; // number of slots occupied in transport
		
		// Targeting info
		bool targetedByAir;
		bool targetedByLand;
		bool targetedBySea;
		
		// Build info
		bool buildsInLand;
		bool buildsInSea;
		bool buildsInAir;
		bool buildsInFactory;
		
		// Special Attributes
		bool amphibious;
		bool capturable;
		bool crushes; // can pass over small things, but deals damage to them
		bool explodes; // explodes on death
		bool hovers;
		bool invulnerable;
		bool regenerates;
		bool repairable;
		
		// Actions
		bool assists;
		bool builds;
		bool captures;
		bool reclaims;
		bool repairs;
		bool sacrifices;
		bool submerges;
		bool transports;
		bool teleports;
		
		std::shared_ptr<MovementProfile> movement;
		void setMovement(std::shared_ptr<MovementProfile>);
		
		std::vector<std::shared_ptr<Weapon>> weapons;
		void addWeapon(std::shared_ptr<Weapon>);
		std::map<binding::AccessoryType, std::shared_ptr<Accessory>> accessories;
		void addAccessory(binding::AccessoryType, std::shared_ptr<Accessory>);
		
		std::shared_ptr<std::function<void(std::shared_ptr<UnitData>)>> callback;
		void setDeathCallback(std::shared_ptr<std::function<void(std::shared_ptr<UnitData>)>>);
	};
	
	class Unit {
	public:
		Unit(std::shared_ptr<UnitData>);
		~Unit();
		
		std::shared_ptr<UnitData> info;
		std::shared_ptr<dankengine::SceneNode> node;
	};
	
}

