#pragma once

#include <binding/weapon.h>

#include <memory>

namespace binding {
	
	enum class Tier : unsigned int {
		TIER_1,
		TIER_2,
		TIER_3,
		TIER_4,
	};
	
	enum class AccessoryType : unsigned int {
		SHIELD,
		CLOAK,
		STEALTH,
		JAMMER,
		RADAR,
		SONAR,
		OMNI,
		EXTRACTOR,
		GENERATOR,
		MSTORAGE,
		ESTORAGE,
		TMLAUNCHER,
		SMLAUNCHER,
		FACTORY,
		ENGINEERING,
	};

	class Accessory {
	public:
		virtual AccessoryType getAccessoryType() = 0;
	};

	class Shield : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		unsigned int hp;
		unsigned int ecost;
		float radius;
	};

	class Cloak : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		unsigned int ecost;
	};
	
	class Stealth : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		unsigned int ecost;
	};

	class Jammer : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		unsigned int ecost;
	};
	
	class Radar : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		unsigned int ecost;
		float radius;
	};
	
	class Sonar : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		unsigned int ecost;
		float radius;
	};
	
	class Omni : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		float radius;
	};
	
	class Extractor : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		unsigned int mgen;
		unsigned int ecost;
	};
	
	class Generator : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		unsigned int egen;
	};
	
	class MStorage : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		unsigned int mstor;
	};
	
	class EStorage : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		unsigned int estor;
	};
	
	class TMLauncher : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		
		// Build stats for the missile
		unsigned int mcost;
		unsigned int ecost;
		unsigned int time;
		
		unsigned int capacity;
		
		void setMissile(std::shared_ptr<Weapon>);
		std::shared_ptr<Weapon> missile;
	};
	
	class SMLauncher : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		
		// Build stats for the missile
		unsigned int mcost;
		unsigned int ecost;
		unsigned int time;
		
		unsigned int capacity; // number of missiles which fit in silo
		
		void setMissile(std::shared_ptr<Weapon>);
		std::shared_ptr<Weapon> missile;
	};
	
	class Factory : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		
		Tier tier;
		bool buildsLand;
		bool buildsSea;
		bool buildsAir;
		
		unsigned int buildRate;
	};
	
	class Engineering : public Accessory {
	public:
		AccessoryType getAccessoryType() override;
		
		Tier tier;
		float radius;
		
		unsigned int buildRate;
	};
}

