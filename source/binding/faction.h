#pragma once

#include <binding/unit.h>

#include <memory>
#include <string>
#include <vector>

namespace binding {

	class Faction {
	public:
		Faction();
		~Faction();
		
		std::string name;
		
		// Vectors of unit names keyed by Tier
		std::vector<std::vector<std::string>> keys;
		
		void addUnitKey(const Tier&, const std::string&);
		
		const std::vector<std::string>& getTierList(const Tier&);
	};
	
}
