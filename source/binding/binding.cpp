
#include <binding/binding.h>
#include <engine/debug.h>
#include <misc/stats.hpp>


#include <algorithm>
#include <iostream>
#include <numeric>


binding::Binding::Binding(const std::string& data_path) {
	this->data_path = data_path;
}

binding::Binding::~Binding() {
	;
}

bool binding::Binding::registerUnitFunc(
	const std::string& id, 
	std::function<std::shared_ptr<binding::UnitData>()> u
) {
	if (unit_creators.count(id) == 0) {
		_DEBUG_PRINT("Registering unit with id: %s\n", id.c_str());
		unit_creators[id] = u;
		return true;
	}
	return false;
}

bool binding::Binding::addFaction(
	const std::string& name, 
	std::shared_ptr<binding::Faction> faction
) {
	if (factions.count(name) == 0) {
		std::cout << "Adding faction: " << name << "\n";
		factions[name] = faction;
		return true;
	}
	return false;
}

std::shared_ptr<binding::Unit> binding::Binding::createNewUnit(const std::string& id) {
	
	if (unit_creators.count(id) == 0) {
		std::cerr << "Couldn't create a unit from id: " << id << "\n";
		return NULL;
	}
	
	std::shared_ptr<Unit> new_unit = std::make_shared<Unit>(unit_creators[id]());
	
	if (meshes.count(new_unit->info->meshFile) == 0) {
		std::cout << "Creating new mesh from " << new_unit->info->meshFile << "\n";
		meshes[new_unit->info->meshFile] = std::make_shared<dankengine::Mesh>();
		dankengine::Resource res(getDataPath() + "/" + new_unit->info->meshFile);
		meshes[new_unit->info->meshFile]->loadMeshFromOBJ(res);
		meshes[new_unit->info->meshFile]->shaderProfile = dankengine::names::defaultMeshTexturedProfileName;
	}
	
	new_unit->node->tie(meshes[new_unit->info->meshFile]);
	
	return new_unit;
}

const std::string& binding::Binding::getDataPath() {
	return this->data_path;
}


void binding::Binding::printFactionStats(
	const std::string& id, 
	const binding::Tier& tl
) {
	
	std::vector<std::string> tier = factions[id]->getTierList(tl);
	
	std::cout << "Populating faction stats for key: " << id << " ...\n";
	
	std::vector<unsigned int> hps(tier.size(), 0);
	std::vector<unsigned int> mcosts(tier.size(), 0);
	std::vector<unsigned int> ecosts(tier.size(), 0);
	
	std::vector<float> vision_ranges(tier.size(), 0.f);
	std::vector<float> velocities(tier.size(), 0.f);
	std::vector<float> pivot_speeds(tier.size(), 0.f);
	
	for (unsigned int i = 0; i < tier.size(); i++) {
		
		std::shared_ptr<binding::Unit> u = this->createNewUnit(tier[i]);
		// std::cout << "Unit: " << tier[i] << "\n";
		// std::cout << "Component Cost: [" << u->mcost << "," << u->ecost << "]\n";
		
		hps[i] = u->info->hp;
		mcosts[i] = u->info->mcost;
		ecosts[i] = u->info->ecost;
		
		vision_ranges[i] = u->info->vision;
		velocities[i] = u->info->movement->maxVelocity;
		pivot_speeds[i] = u->info->movement->pivotSpeed;

	}
	
	velocities.erase(std::remove(velocities.begin(), velocities.end(), 0), velocities.end());
	pivot_speeds.erase(std::remove(pivot_speeds.begin(), pivot_speeds.end(), 0), pivot_speeds.end());
	
	std::cout << "Tier " << (unsigned int)tl + 1 << " (" << tier.size() << " units)\n";
	std::cout << "Averages\n";
	std::cout << "========\n"
	<< "HP:       "
	<< mean(hps) << "\n"
	<< "Cost:     ["
	<< mean(mcosts) << ", "
	<< mean(ecosts) << "]\n"
	<< "Vision:   "
	<< mean(vision_ranges) << "\n"
	<< "Velocity: "
	<< mean(velocities) << "\n"
	<< "Pivot:    "
	<< mean(pivot_speeds) << "\n\n";
	
	std::cout << "Medians\n";
	std::cout << "=======\n"
	<< "HP:       "
	<< median(hps) << "\n"
	<< "Cost:     ["
	<< median(mcosts) << ", "
	<< median(ecosts) << "]\n"
	<< "Vision:   "
	<< median(vision_ranges) << "\n"
	<< "Velocity: "
	<< median(velocities) << "\n"
	<< "Pivot:    "
	<< median(pivot_speeds) << "\n\n";
	
}

