#pragma once

#include <engine/resources/heightmap.h>
#include <engine/graphics/3D/mesh.h>

#include <glm/glm.hpp>

#include <filesystem>
#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>

namespace binding {

	class Terrain {
	public:
		
		// These constants correspond to a floating point scalar value
		// to which a normalized terrain mesh can be scaled

		Terrain();
		Terrain(const std::string&);
		Terrain(const std::filesystem::path&);
		
		static void NONE(glm::vec3*, const dankengine::HeightMap&, unsigned int x, unsigned int y);
		static void LINEAR(glm::vec3*, const dankengine::HeightMap&, unsigned int x, unsigned int y);
		static void CUBIC(glm::vec3*, const dankengine::HeightMap&, unsigned int x, unsigned int y);
		// I'm not even sure that this is possible, but I'll include it here just in case
		// void QUINTIC(glm::vec3*, const dankengine::HeightMap&, unsigned int x, unsigned int y);
		void setInterpolationFunction(std::function<
				void(glm::vec3*, const dankengine::HeightMap&, unsigned int x, unsigned int y)>);
		
		std::shared_ptr<dankengine::Mesh> getMesh(unsigned int lod);
		
		void open(const std::string&);
		void generate(unsigned int lod);
		
	private:
		
		static float POINT_ON_LINE(float t, const glm::vec2& pt1, const glm::vec2& pt0);
		
		static float POINT_ON_CUBIC(float t, const glm::vec2& pt3, const glm::vec2& pt2, 
											 const glm::vec2& pt1, const glm::vec2& pt0);
		
		bool isCanonical(unsigned int lod, unsigned int x, unsigned int y);
		
		std::function<void(glm::vec3*, const dankengine::HeightMap&, unsigned int x, unsigned int y)> interpolate;
		std::unordered_map<unsigned int, std::shared_ptr<dankengine::Mesh>> meshes;
		
		dankengine::HeightMap heightmap;
		
	};

}