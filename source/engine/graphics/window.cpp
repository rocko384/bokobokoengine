#include "window.h"
#include "engine/debug.h"

dankengine::Window::Window() {
}

dankengine::Window::Window(size_t Width, size_t Height, const std::string& Title, int MSAA) {
	open(Width, Height, Title, MSAA);
}

dankengine::Window::~Window() {
}

int dankengine::Window::open(size_t Width, size_t Height, const std::string& Title, int MSAA) {
	m_window.create(sf::VideoMode(static_cast<unsigned int>(Width), static_cast<unsigned int>(Height)), Title, sf::Style::Default, sf::ContextSettings(24, 8, MSAA, 4, 3));

	if (m_window.isOpen() != true) {
		return NOWINDOW;
	}
/*
	glewExperimental = GL_TRUE;
	auto e = glewInit();

	if (e != GLEW_OK) {
		return NOGLEW;
	}*/

	if (!gladLoadGL()) {
		return NOGLAD;
	}
	_DEBUG_PRINT("OpenGL %d.%d\n", GLVersion.major, GLVersion.minor);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

#ifdef _DEBUG
	glDebugMessageCallback(
		[](GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
			std::string msg(message);
			_DEBUG_PRINT("OpenGL Error\n\tSource: %i\n\tType: %i\n\tID: %i\n\tSeverity: %i\n\tMessage: %s", source, type, id, severity, msg.c_str());
		},
		nullptr
	);
#endif // _DEBUG

	return SUCCESS;
}

void dankengine::Window::close() {
	m_window.close();
}

bool dankengine::Window::isOpen() {
	return m_window.isOpen();
}

size_t dankengine::Window::width() {
	return m_window.getSize().x;
}

size_t dankengine::Window::height() {
	return m_window.getSize().y;
}

void dankengine::Window::clear() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void dankengine::Window::display() {
	m_window.display();
}

void dankengine::Window::setSize(size_t Width, size_t Height) {
	m_window.setSize(sf::Vector2u(static_cast<unsigned int>(Width), static_cast<unsigned int>(Height)));
}

void dankengine::Window::setActive(bool Active) {
	m_window.setActive(Active);
}

void dankengine::Window::enableVSync(bool Flag) {
	m_window.setVerticalSyncEnabled(Flag);
}

std::optional<sf::Event> dankengine::Window::pollEvent() {
	sf::Event _ret;
	bool exists = m_window.pollEvent(_ret);

	if (exists) {
		return _ret;
	}
	
	return {};
}

