#pragma once

#include "engine/dank.h"
#include "engine/resources/resource.h"

#include "engine/graphics/renderable.h"

#include <vector>

namespace dankengine {

	class DankExport Mesh : public Renderable {

	public:

		enum Format {
			OBJ,
			JSON
		};

	public:

		Mesh();
		Mesh(const std::string& Path, Format f = OBJ);
		Mesh(Resource& resource, Format f = OBJ);
		~Mesh();

		void loadMeshFromOBJ(const std::string& Path);
		void loadMeshFromOBJ(Resource& resource);

		void pushVertex(const glm::vec3& Position, const glm::vec3& Normal, const glm::vec3& Color, const glm::vec2& TexCoords);
		void pushVertex(float X, float Y, float Z, float Nx, float Ny, float Nz, float R, float G, float B, float U, float V);
		void clearVertices();
		void compileVertices();

		void setIndices(const std::vector<uint32_t>& IndexList);
		void setIndices(const std::initializer_list<uint32_t>& IndexList);

	protected:

		std::vector<float> m_meshBuildingBuffer;

	};

}