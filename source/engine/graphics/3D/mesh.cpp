#include "mesh.h"

#include <sstream>
#include <tinyobjloader/tiny_obj_loader.h>

using _v = std::array<float, 11>;

namespace std {
	template<> struct hash<_v> {
		size_t operator()(const _v& vertex) const {
			std::u32string s;
			for (float f : vertex) {
				s += std::hash<float>()(f);
			}
			return std::hash<std::u32string>()(s);
		}
	};
}

inline size_t hash_v(const _v& v) {
	std::u32string s;
	for (float f : v) {
		s += std::hash<float>{}(f);
	}
	return std::hash<std::u32string>{}(s);
}

dankengine::Mesh::Mesh() {
	setDefaultVAOLayout3D();

	shaderProfile = names::defaultMeshProfileName;
}

dankengine::Mesh::Mesh(const std::string& Path, Format f) {
	if (f == OBJ) {
		loadMeshFromOBJ(Path);
	}
}

dankengine::Mesh::Mesh(Resource& resource, Format f) {
	if (f == OBJ) {
		loadMeshFromOBJ(resource);
	}
}

dankengine::Mesh::~Mesh() {
}

void dankengine::Mesh::loadMeshFromOBJ(const std::string& Path) {
	Resource res(Path);
	loadMeshFromOBJ(res);
}

void dankengine::Mesh::loadMeshFromOBJ(Resource& resource) {

	std::istringstream body(resource.getDataAsString());


	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string err;

	tinyobj::MaterialFileReader mfr(resource.getPath().parent_path().string() + "/");

	tinyobj::LoadObj(&attrib, &shapes, &materials, &err, &body, &mfr);

	std::map<_v, uint32_t> _vertexmap;
	std::vector<uint32_t> indices;

	for (const auto& idx : shapes[0].mesh.indices) {

		float vx = attrib.vertices[3 * idx.vertex_index + 0];
		float vy = attrib.vertices[3 * idx.vertex_index + 1];
		float vz = attrib.vertices[3 * idx.vertex_index + 2];

		float nx = 0.0f;
		float ny = 0.0f;
		float nz = 0.0f;

		if (idx.normal_index != -1) {
			nx = attrib.normals[3 * idx.normal_index + 0];
			ny = attrib.normals[3 * idx.normal_index + 1];
			nz = attrib.normals[3 * idx.normal_index + 2];
		}

		float r = attrib.colors[3 * idx.vertex_index + 0];
		float g = attrib.colors[3 * idx.vertex_index + 1];
		float b = attrib.colors[3 * idx.vertex_index + 2];

		float tu = attrib.texcoords[2 * idx.texcoord_index + 0];
		float tv = 1.0f - attrib.texcoords[2 * idx.texcoord_index + 1];

		_v vert = { vx, vy, vz, nx, ny, nz, r, g, b, tu, tv };
			
		if (_vertexmap.count(vert) == 0) {
			_vertexmap[vert] = static_cast<uint32_t>(_vertexmap.size());
			pushVertex(vx, vy, vz, nx, ny, nz, r, g, b, tu, tv);
		}

		indices.push_back(_vertexmap[vert]);
	}

	setIndices(indices);

	compileVertices();
	clearVertices();


	if (!materials[0].diffuse_texname.empty()) {
		material->diffuseMap->loadFromFile2D(resource.getPath().parent_path().string() + "/" + materials[0].diffuse_texname);

		material->diffuseMap->setParameter(Texture::TEXTURE_WRAP_S, Texture::REPEAT, Texture::TEXTURE_2D);
		material->diffuseMap->setParameter(Texture::TEXTURE_WRAP_T, Texture::REPEAT, Texture::TEXTURE_2D);

		material->diffuseMap->setParameter(Texture::TEXTURE_BORDER_COLOR, { 1.0f, 0.0f, 0.0f, 1.0f });
		material->diffuseMap->setParameter(Texture::TEXTURE_MIN_FILTER, Texture::LINEAR);
		material->diffuseMap->setParameter(Texture::TEXTURE_MIN_FILTER, Texture::LINEAR);
	}
	else {
		material->diffuseMap = Texture::MakeUnit();
	}

	if (!materials[0].specular_texname.empty()) {
		material->specularMap->loadFromFile2D(resource.getPath().parent_path().string() + "/" + materials[0].specular_texname);

		material->specularMap->setParameter(Texture::TEXTURE_WRAP_S, Texture::REPEAT, Texture::TEXTURE_2D);
		material->specularMap->setParameter(Texture::TEXTURE_WRAP_T, Texture::REPEAT, Texture::TEXTURE_2D);

		material->specularMap->setParameter(Texture::TEXTURE_BORDER_COLOR, { 1.0f, 0.0f, 0.0f, 1.0f });
		material->specularMap->setParameter(Texture::TEXTURE_MIN_FILTER, Texture::LINEAR);
		material->specularMap->setParameter(Texture::TEXTURE_MIN_FILTER, Texture::LINEAR);
	}
	else {
		material->specularMap = Texture::MakeUnit();
	}

	if (!materials[0].ambient_texname.empty()) {
		material->ambientMap->loadFromFile2D(resource.getPath().parent_path().string() + "/" + materials[0].ambient_texname);

		material->ambientMap->setParameter(Texture::TEXTURE_WRAP_S, Texture::REPEAT, Texture::TEXTURE_2D);
		material->ambientMap->setParameter(Texture::TEXTURE_WRAP_T, Texture::REPEAT, Texture::TEXTURE_2D);

		material->ambientMap->setParameter(Texture::TEXTURE_BORDER_COLOR, { 1.0f, 0.0f, 0.0f, 1.0f });
		material->ambientMap->setParameter(Texture::TEXTURE_MIN_FILTER, Texture::LINEAR);
		material->ambientMap->setParameter(Texture::TEXTURE_MIN_FILTER, Texture::LINEAR);
	}
	else {
		material->ambientMap = Texture::MakeUnit();
	}

	if (!materials[0].ambient_texname.empty()) {
		material->bumpMap->loadFromFile2D(resource.getPath().parent_path().string() + "/" + materials[0].bump_texname);
	}
	else {
		material->bumpMap = Texture::MakeUnit();
	}

	
	material->shininess = materials[0].shininess;

}

void dankengine::Mesh::pushVertex(const glm::vec3& Position, const glm::vec3& Normal, const glm::vec3& Color, const glm::vec2& TexCoords) {
	pushVertex(Position.x, Position.y, Position.z, Normal.x, Normal.y, Normal.z, Color.r, Color.g, Color.b, TexCoords.x, TexCoords.y);
}

void dankengine::Mesh::pushVertex(float X, float Y, float Z, float Nx, float Ny, float Nz, float R, float G, float B, float U, float V) {
	m_meshBuildingBuffer.push_back(X);
	m_meshBuildingBuffer.push_back(Y);
	m_meshBuildingBuffer.push_back(Z);
	m_meshBuildingBuffer.push_back(Nx);
	m_meshBuildingBuffer.push_back(Ny);
	m_meshBuildingBuffer.push_back(Nz);
	m_meshBuildingBuffer.push_back(R);
	m_meshBuildingBuffer.push_back(G);
	m_meshBuildingBuffer.push_back(B);
	m_meshBuildingBuffer.push_back(U);
	m_meshBuildingBuffer.push_back(V);

}

void dankengine::Mesh::clearVertices() {
	m_meshBuildingBuffer.clear();
}

void dankengine::Mesh::compileVertices() {
	vertexBuffer->setData(m_meshBuildingBuffer);
}

void dankengine::Mesh::setIndices(const std::vector<uint32_t>& IndexList) {
	indexBuffer->setData(IndexList, GPUBuffer::STATIC_DRAW, GPUBuffer::ELEMENT_ARRAY);
	m_vertexCount = IndexList.size();
}

void dankengine::Mesh::setIndices(const std::initializer_list<uint32_t>& IndexList) {
	auto v = std::vector(IndexList);
	setIndices(v);
}
