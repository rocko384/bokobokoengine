#pragma once

#include "engine/dank.h"

#include "gl/shaderprogram.h"
#include "gl/texture.h"

#include "engine/handle.h"

namespace dankengine {

	class DankExport Material {

	public:

		Material();
		Material(const Handle<Texture>& Texture);
		~Material();

		uint32_t ID();

		Handle<Texture> diffuseMap;
		Handle<Texture> specularMap;
		Handle<Texture> ambientMap;
		Handle<Texture> bumpMap;

		float shininess;

		void bind();
		void unbind();

	private:

		static uint32_t g_createdMaterials;
		uint32_t m_id;

	};

}