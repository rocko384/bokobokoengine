#pragma once

#include "engine/dank.h"

#include <glad/glad.h>
#include <KHR/khrplatform.h>
#include <SFML/Window.hpp>

#include <cinttypes>
#include <optional>
#include <string>

namespace dankengine {

	class DankExport Window {

	public:

		enum {
			SUCCESS,
			NOWINDOW,
			NOGLEW,
			NOGLAD
		};

	public:

		Window();
		Window(size_t Width, size_t Height, const std::string& Title, int MSAA = 0);
		~Window();

		int open(size_t Width, size_t Height, const std::string& Title, int MSAA = 0);
		void close();

		bool isOpen();
		size_t width();
		size_t height();

		void clear();
		void display();

		void setSize(size_t Width, size_t Height);
		void setActive(bool Active);

		void enableVSync(bool Flag);

		std::optional<sf::Event> pollEvent();

	private:

		sf::Window m_window;

	};

}