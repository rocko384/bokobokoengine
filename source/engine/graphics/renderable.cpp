#include "renderable.h"

dankengine::Renderable::Renderable() {

	vertexArrayObject = std::make_shared<VertexArrayObject>();
	vertexBuffer = std::make_shared<GPUBuffer>();
	indexBuffer = std::make_shared<GPUBuffer>();
	material = std::make_shared<Material>();

	callback = [](Renderable&) {};

}

dankengine::Renderable::~Renderable() {
}

void dankengine::Renderable::setDefaultVAOLayout2D() {
	vertexArrayObject->configVertexAttribPointer(*vertexBuffer, 0, 2, VertexArrayObject::FLOAT, false, 10, 0); // Position (2)
	vertexArrayObject->configVertexAttribPointer(*vertexBuffer, 1, 3, VertexArrayObject::FLOAT, false, 10, 2); // Normal (3)
	vertexArrayObject->configVertexAttribPointer(*vertexBuffer, 2, 3, VertexArrayObject::FLOAT, false, 10, 5); // Color (3)
	vertexArrayObject->configVertexAttribPointer(*vertexBuffer, 3, 2, VertexArrayObject::FLOAT, false, 10, 8); // UV (2)
}

void dankengine::Renderable::setDefaultVAOLayout3D() {
	vertexArrayObject->configVertexAttribPointer(*vertexBuffer, 0, 3, VertexArrayObject::FLOAT, false, 11, 0); // Position (3)
	vertexArrayObject->configVertexAttribPointer(*vertexBuffer, 1, 3, VertexArrayObject::FLOAT, false, 11, 3); // Normal (3)
	vertexArrayObject->configVertexAttribPointer(*vertexBuffer, 2, 3, VertexArrayObject::FLOAT, false, 11, 6); // Color (3)
	vertexArrayObject->configVertexAttribPointer(*vertexBuffer, 3, 2, VertexArrayObject::FLOAT, false, 11, 9); // UV (2)
}

void dankengine::Renderable::setUniforms(ShaderProgram& program) {
}

size_t dankengine::Renderable::vertexCount() {
	return m_vertexCount;
}

void dankengine::Renderable::update() {
}
