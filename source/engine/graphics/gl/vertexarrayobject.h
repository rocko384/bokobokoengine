#pragma once

#include "engine/dank.h"

#include "gpubuffer.h"
#include "shaderprogram.h"

namespace dankengine {

	class DankExport VertexArrayObject {

	public:

		enum AttribType {
			BYTE =						GL_BYTE,
			UNSIGNED_BYTE =				GL_UNSIGNED_BYTE,
			SHORT =						GL_SHORT,
			UNSIGNED_SHORT =			GL_UNSIGNED_SHORT,
			INT =						GL_INT,
			UNSIGNED_INT =				GL_UNSIGNED_INT,
			FLOAT =						GL_FLOAT,
			HALF_FLOAT =				GL_HALF_FLOAT,
			DOUBLE =					GL_DOUBLE,
			INT_2_10_10_10 =			GL_INT_2_10_10_10_REV,
			UNSIGNED_INT_2_10_10_10 =	GL_UNSIGNED_INT_2_10_10_10_REV
		};

	public:

		VertexArrayObject();
		~VertexArrayObject();

		void configVertexAttribPointer(GPUBuffer& Buffer, GLuint AttribLocation, size_t Size, AttribType Type, bool Normalized, size_t Stride, uint64_t Offset);
		void configVertexAttribPointer(GPUBuffer& Buffer, ShaderProgram& Program, const std::string& Name, size_t Size, AttribType Type, bool Normalized, size_t Stride, uint64_t Offset);

		void bind();
		void unbind();

	private:

		GLuint m_glid;		// GL "pointer" thing
		static GLuint g_lastObjectBind;		// Last bound shader

	};

}