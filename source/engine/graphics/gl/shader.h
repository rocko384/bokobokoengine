#pragma once

#include "engine/dank.h"

#include <glad/glad.h>

#include <cstring>
#include <vector>
#include <string>
#include <iostream>

#include "engine/resources/resource.h"

namespace dankengine {

	class DankExport Shader {

	public:
		enum Type {
			VERTEX_SHADER =				GL_VERTEX_SHADER,
			FRAGMENT_SHADER =			GL_FRAGMENT_SHADER,
			GEOMETRY_SHADER =			GL_GEOMETRY_SHADER,
			COMPUTE_SHADER =			GL_COMPUTE_SHADER,
			TESS_CONTROL_SHADER =		GL_TESS_CONTROL_SHADER,
			TESS_EVALUATION_SHADER =	GL_TESS_EVALUATION_SHADER
		};

	public:

		Shader();
		Shader(Type ShaderType);
		Shader(const std::string& Source);
		Shader(Resource& Resource);
		~Shader();

		void create(Type ShaderType);

		void addToSource(const std::string& Source);
		void addToSource(Resource& Resource);

		void addToSourceFromFile(const std::string& Filepath);

		std::string compile(bool OutputLog = false);

		GLuint glObject();

	private:

		std::vector<std::string> m_source;

		GLuint m_glid;

	};

}