#include "gpubuffer.h"

GLuint dankengine::GPUBuffer::g_lastObjectBind = 0;

dankengine::GPUBuffer::GPUBuffer() {
	glGenBuffers(1, &m_glid);
}

dankengine::GPUBuffer::~GPUBuffer() {
	glDeleteBuffers(1, &m_glid);
}

void dankengine::GPUBuffer::bind(Bind Use) {
	if ((m_lastBind != Use) || (g_lastObjectBind != m_glid)) {
		glBindBuffer(Use, m_glid);
		m_lastBind = Use;
		g_lastObjectBind = m_glid;
	}
}

void dankengine::GPUBuffer::unbind() {
	glBindBuffer(m_lastBind, 0);
	m_lastBind = 0;
	g_lastObjectBind = 0;
}

GLuint dankengine::GPUBuffer::glObject() {
	return m_glid;
}
