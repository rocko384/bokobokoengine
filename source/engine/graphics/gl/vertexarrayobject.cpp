#include "vertexarrayobject.h"

GLuint dankengine::VertexArrayObject::g_lastObjectBind = 0;

dankengine::VertexArrayObject::VertexArrayObject() {
	glGenVertexArrays(1, &m_glid);
}

dankengine::VertexArrayObject::~VertexArrayObject() {
	if (m_glid > 0) {
		glDeleteVertexArrays(1, &m_glid);
	}
}

void dankengine::VertexArrayObject::configVertexAttribPointer(GPUBuffer& Buffer, GLuint AttribLocation, size_t Size, AttribType Type, bool Normalized, size_t Stride, uint64_t Offset) {
	bind();
	Buffer.bind(GPUBuffer::ARRAY);

	size_t typeSize = 0;

	switch (Type) {
	case BYTE:
		typeSize = 1;
		break;
	case UNSIGNED_BYTE:
		typeSize = 1;
		break;
	case SHORT:
		typeSize = 2;
		break;
	case UNSIGNED_SHORT:
		typeSize = 2;
		break;
	case INT:
		typeSize = 4;
		break;
	case UNSIGNED_INT:
		typeSize = 4;
		break;
	case FLOAT:
		typeSize = 4;
		break;
	case HALF_FLOAT:
		typeSize = 2;
		break;
	case DOUBLE:
		typeSize = 8;
		break;
	case INT_2_10_10_10:
		typeSize = 4;
		break;
	case UNSIGNED_INT_2_10_10_10:
		typeSize = 4;
		break;
	}

	glVertexAttribPointer(AttribLocation, static_cast<GLint>(Size), Type, Normalized ? GL_TRUE : GL_FALSE, static_cast<GLsizei>(Stride * typeSize), reinterpret_cast<const void*>(Offset * typeSize));
	glEnableVertexAttribArray(AttribLocation);
}

void dankengine::VertexArrayObject::configVertexAttribPointer(GPUBuffer& Buffer, ShaderProgram& Program, const std::string& Name, size_t Size, AttribType Type, bool Normalized, size_t Stride, uint64_t Offset) {
	configVertexAttribPointer(Buffer, Program.getAttribLocation(Name), Size, Type, Normalized, Stride, Offset);
}

void dankengine::VertexArrayObject::bind() {
	if (g_lastObjectBind != m_glid) {
		glBindVertexArray(m_glid);
	}
}

void dankengine::VertexArrayObject::unbind() {
	if (g_lastObjectBind == m_glid) {
		glBindVertexArray(0);
	}
}
