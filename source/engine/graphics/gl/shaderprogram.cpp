#include "shaderprogram.h"

GLuint dankengine::ShaderProgram::g_lastObjectBind = 0;

dankengine::ShaderProgram::ShaderProgram() {
	m_glid = glCreateProgram();
}

dankengine::ShaderProgram::~ShaderProgram() {
	if (m_glid > 0) {
		glDeleteProgram(m_glid);
	}
}

void dankengine::ShaderProgram::attachShader(Shader& Shader) {
	glAttachShader(m_glid, Shader.glObject());
}

void dankengine::ShaderProgram::detachShader(Shader& Shader) {
	glDetachShader(m_glid, Shader.glObject());
}

void dankengine::ShaderProgram::bindFragDataLocation(GLuint ColorNumber, const std::string& Name) {
	glBindFragDataLocation(m_glid, ColorNumber, Name.c_str());
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, const glm::vec2& Value) {
	glUniform2fv(getUniformLocation(Name), 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, const glm::ivec2& Value) {
	glUniform2iv(getUniformLocation(Name), 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, const glm::uvec2& Value) {
	glUniform2uiv(getUniformLocation(Name), 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, const glm::vec3& Value) {
	glUniform3fv(getUniformLocation(Name), 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, const glm::ivec3& Value) {
	glUniform3iv(getUniformLocation(Name), 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, const glm::uvec3& Value) {
	glUniform3uiv(getUniformLocation(Name), 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, const glm::vec4& Value) {
	glUniform4fv(getUniformLocation(Name), 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, const glm::ivec4& Value) {
	glUniform4iv(getUniformLocation(Name), 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, const glm::uvec4& Value) {
	glUniform4uiv(getUniformLocation(Name), 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform1(const std::string& Name, float A) {
	glUniform1f(getUniformLocation(Name), A);
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, float A, float B) {
	glUniform2f(getUniformLocation(Name), A, B);
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, float A, float B, float C) {
	glUniform3f(getUniformLocation(Name), A, B, C);
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, float A, float B, float C, float D) {
	glUniform4f(getUniformLocation(Name), A, B, C, D);
}

void dankengine::ShaderProgram::setUniform1(const std::string& Name, int32_t A) {
	glUniform1i(getUniformLocation(Name), A);
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, int32_t A, int32_t B) {
	glUniform2i(getUniformLocation(Name), A, B);
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, int32_t A, int32_t B, int32_t C) {
	glUniform3i(getUniformLocation(Name), A, B, C);
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, int32_t A, int32_t B, int32_t C, int32_t D) {
	glUniform4i(getUniformLocation(Name), A, B, C, D);
}

void dankengine::ShaderProgram::setUniform1(const std::string& Name, uint32_t A) {
	glUniform1ui(getUniformLocation(Name), A);
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, uint32_t A, uint32_t B) {
	glUniform2ui(getUniformLocation(Name), A, B);
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, uint32_t A, uint32_t B, uint32_t C) {
	glUniform3ui(getUniformLocation(Name), A, B, C);
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, uint32_t A, uint32_t B, uint32_t C, uint32_t D) {
	glUniform4ui(getUniformLocation(Name), A, B, C, D);
}

void dankengine::ShaderProgram::setUniform1(const std::string& Name, const std::vector<float>& Values) {
	glUniform1fv(getUniformLocation(Name), static_cast<GLsizei>(Values.size()), Values.data());
}

void dankengine::ShaderProgram::setUniform1(const std::string& Name, const std::initializer_list<float>& Values) {
	glUniform1fv(getUniformLocation(Name), static_cast<GLsizei>(std::vector<float>(Values).size()), std::vector<float>(Values).data());
}

void dankengine::ShaderProgram::setUniform1(const std::string& Name, size_t Length, const float* Values) {
	glUniform1fv(getUniformLocation(Name), static_cast<GLsizei>(Length), Values);
}

void dankengine::ShaderProgram::setUniform1(const std::string& Name, const std::vector<int32_t>& Values) {
	glUniform1iv(getUniformLocation(Name), static_cast<GLsizei>(Values.size()), Values.data());
}

void dankengine::ShaderProgram::setUniform1(const std::string& Name, const std::initializer_list<int32_t>& Values) {
	glUniform1iv(getUniformLocation(Name), static_cast<GLsizei>(std::vector<int32_t>(Values).size()), std::vector<int32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform1(const std::string& Name, size_t Length, const int32_t* Values) {
	glUniform1iv(getUniformLocation(Name), static_cast<GLsizei>(Length), Values);
}

void dankengine::ShaderProgram::setUniform1(const std::string& Name, const std::vector<uint32_t>& Values) {
	glUniform1uiv(getUniformLocation(Name), static_cast<GLsizei>(Values.size()), Values.data());
}

void dankengine::ShaderProgram::setUniform1(const std::string& Name, const std::initializer_list<uint32_t>& Values) {
	glUniform1uiv(getUniformLocation(Name), static_cast<GLsizei>(std::vector<uint32_t>(Values).size()), std::vector<uint32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform1(const std::string& Name, size_t Length, const uint32_t* Values) {
	glUniform1uiv(getUniformLocation(Name), static_cast<GLsizei>(Length), Values);
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, const std::vector<float>& Values) {
	glUniform2fv(getUniformLocation(Name), static_cast<GLsizei>(Values.size() / 2), Values.data());
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, const std::initializer_list<float>& Values) {
	glUniform2fv(getUniformLocation(Name), static_cast<GLsizei>(std::vector<float>(Values).size() / 2), std::vector<float>(Values).data());
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, size_t Length, const float* Values) {
	glUniform2fv(getUniformLocation(Name), static_cast<GLsizei>(Length / 2), Values);
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, const std::vector<int32_t>& Values) {
	glUniform2iv(getUniformLocation(Name), static_cast<GLsizei>(Values.size() / 2), Values.data());
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, const std::initializer_list<int32_t>& Values) {
	glUniform2iv(getUniformLocation(Name), static_cast<GLsizei>(std::vector<int32_t>(Values).size() / 2), std::vector<int32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, size_t Length, const int32_t* Values) {
	glUniform2iv(getUniformLocation(Name), static_cast<GLsizei>(Length / 2), Values);
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, const std::vector<uint32_t>& Values) {
	glUniform2uiv(getUniformLocation(Name), static_cast<GLsizei>(Values.size() / 2), Values.data());
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, const std::initializer_list<uint32_t>& Values) {
	glUniform2uiv(getUniformLocation(Name), static_cast<GLsizei>(std::vector<uint32_t>(Values).size() / 2), std::vector<uint32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform2(const std::string& Name, size_t Length, const uint32_t* Values) {
	glUniform2uiv(getUniformLocation(Name), static_cast<GLsizei>(Length / 2), Values);
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, const std::vector<float>& Values) {
	glUniform3fv(getUniformLocation(Name), static_cast<GLsizei>(Values.size() / 3), Values.data());
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, const std::initializer_list<float>& Values) {
	glUniform3fv(getUniformLocation(Name), static_cast<GLsizei>(std::vector<float>(Values).size() / 3), std::vector<float>(Values).data());
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, size_t Length, const float* Values) {
	glUniform3fv(getUniformLocation(Name), static_cast<GLsizei>(Length / 3), Values);
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, const std::vector<int32_t>& Values) {
	glUniform3iv(getUniformLocation(Name), static_cast<GLsizei>(Values.size() / 3), Values.data());
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, const std::initializer_list<int32_t>& Values) {
	glUniform3iv(getUniformLocation(Name), static_cast<GLsizei>(std::vector<int32_t>(Values).size() / 3), std::vector<int32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, size_t Length, const int32_t* Values) {
	glUniform3iv(getUniformLocation(Name), static_cast<GLsizei>(Length / 3), Values);
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, const std::vector<uint32_t>& Values) {
	glUniform3uiv(getUniformLocation(Name), static_cast<GLsizei>(Values.size() / 3), Values.data());
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, const std::initializer_list<uint32_t>& Values) {
	glUniform3uiv(getUniformLocation(Name), static_cast<GLsizei>(std::vector<uint32_t>(Values).size() / 3), std::vector<uint32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform3(const std::string& Name, size_t Length, const uint32_t* Values) {
	glUniform3uiv(getUniformLocation(Name), static_cast<GLsizei>(Length / 3), Values);
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, const std::vector<float>& Values) {
	glUniform4fv(getUniformLocation(Name), static_cast<GLsizei>(Values.size() / 4), Values.data());
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, const std::initializer_list<float>& Values) {
	glUniform4fv(getUniformLocation(Name), static_cast<GLsizei>(std::vector<float>(Values).size() / 4), std::vector<float>(Values).data());
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, size_t Length, const float* Values) {
	glUniform4fv(getUniformLocation(Name), static_cast<GLsizei>(Length / 4), Values);
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, const std::vector<int32_t>& Values) {
	glUniform4iv(getUniformLocation(Name), static_cast<GLsizei>(Values.size() / 4), Values.data());
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, const std::initializer_list<int32_t>& Values) {
	glUniform4iv(getUniformLocation(Name), static_cast<GLsizei>(std::vector<int32_t>(Values).size() / 4), std::vector<int32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, size_t Length, const int32_t* Values) {
	glUniform4iv(getUniformLocation(Name), static_cast<GLsizei>(Length / 4), Values);
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, const std::vector<uint32_t>& Values) {
	glUniform4uiv(getUniformLocation(Name), static_cast<GLsizei>(Values.size() / 4), Values.data());
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, const std::initializer_list<uint32_t>& Values) {
	glUniform4uiv(getUniformLocation(Name), static_cast<GLsizei>(std::vector<uint32_t>(Values).size() / 4), std::vector<uint32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform4(const std::string& Name, size_t Length, const uint32_t* Values) {
	glUniform4uiv(getUniformLocation(Name), static_cast<GLsizei>(Length / 4), Values);
}

void dankengine::ShaderProgram::setUniformMatrix2(const std::string& Name, bool Transpose, const glm::mat2& Matrix) {
	glUniformMatrix2fv(getUniformLocation(Name), 1, Transpose ? GL_TRUE : GL_FALSE, glm::value_ptr(Matrix));
}

void dankengine::ShaderProgram::setUniformMatrix3(const std::string& Name, bool Transpose, const glm::mat3& Matrix) {
	glUniformMatrix3fv(getUniformLocation(Name), 1, Transpose ? GL_TRUE : GL_FALSE, glm::value_ptr(Matrix));
}

void dankengine::ShaderProgram::setUniformMatrix4(const std::string& Name, bool Transpose, const glm::mat4& Matrix) {
	glUniformMatrix4fv(getUniformLocation(Name), 1, Transpose ? GL_TRUE : GL_FALSE, glm::value_ptr(Matrix));
}


void dankengine::ShaderProgram::setUniform2(GLint Location, const glm::vec2& Value) {
	glUniform2fv(Location, 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform2(GLint Location, const glm::ivec2& Value) {
	glUniform2iv(Location, 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform2(GLint Location, const glm::uvec2& Value) {
	glUniform2uiv(Location, 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform3(GLint Location, const glm::vec3& Value) {
	glUniform3fv(Location, 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform3(GLint Location, const glm::ivec3& Value) {
	glUniform3iv(Location, 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform3(GLint Location, const glm::uvec3& Value) {
	glUniform3uiv(Location, 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform4(GLint Location, const glm::vec4& Value) {
	glUniform4fv(Location, 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform4(GLint Location, const glm::ivec4& Value) {
	glUniform4iv(Location, 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform4(GLint Location, const glm::uvec4& Value) {
	glUniform4uiv(Location, 1, glm::value_ptr(Value));
}

void dankengine::ShaderProgram::setUniform1(GLint Location, float A) {
	glUniform1f(Location, A);
}

void dankengine::ShaderProgram::setUniform2(GLint Location, float A, float B) {
	glUniform2f(Location, A, B);
}

void dankengine::ShaderProgram::setUniform3(GLint Location, float A, float B, float C) {
	glUniform3f(Location, A, B, C);
}

void dankengine::ShaderProgram::setUniform4(GLint Location, float A, float B, float C, float D) {
	glUniform4f(Location, A, B, C, D);
}

void dankengine::ShaderProgram::setUniform1(GLint Location, int32_t A) {
	glUniform1i(Location, A);
}

void dankengine::ShaderProgram::setUniform2(GLint Location, int32_t A, int32_t B) {
	glUniform2i(Location, A, B);
}

void dankengine::ShaderProgram::setUniform3(GLint Location, int32_t A, int32_t B, int32_t C) {
	glUniform3i(Location, A, B, C);
}

void dankengine::ShaderProgram::setUniform4(GLint Location, int32_t A, int32_t B, int32_t C, int32_t D) {
	glUniform4i(Location, A, B, C, D);
}

void dankengine::ShaderProgram::setUniform1(GLint Location, uint32_t A) {
	glUniform1ui(Location, A);
}

void dankengine::ShaderProgram::setUniform2(GLint Location, uint32_t A, uint32_t B) {
	glUniform2ui(Location, A, B);
}

void dankengine::ShaderProgram::setUniform3(GLint Location, uint32_t A, uint32_t B, uint32_t C) {
	glUniform3ui(Location, A, B, C);
}

void dankengine::ShaderProgram::setUniform4(GLint Location, uint32_t A, uint32_t B, uint32_t C, uint32_t D) {
	glUniform4ui(Location, A, B, C, D);
}

void dankengine::ShaderProgram::setUniform1(GLint Location, const std::vector<float>& Values) {
	glUniform1fv(Location, static_cast<GLsizei>(Values.size()), Values.data());
}

void dankengine::ShaderProgram::setUniform1(GLint Location, const std::initializer_list<float>& Values) {
	glUniform1fv(Location, static_cast<GLsizei>(std::vector<float>(Values).size()), std::vector<float>(Values).data());
}

void dankengine::ShaderProgram::setUniform1(GLint Location, size_t Length, const float* Values) {
	glUniform1fv(Location, static_cast<GLsizei>(Length), Values);
}

void dankengine::ShaderProgram::setUniform1(GLint Location, const std::vector<int32_t>& Values) {
	glUniform1iv(Location, static_cast<GLsizei>(Values.size()), Values.data());
}

void dankengine::ShaderProgram::setUniform1(GLint Location, const std::initializer_list<int32_t>& Values) {
	glUniform1iv(Location, static_cast<GLsizei>(std::vector<int32_t>(Values).size()), std::vector<int32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform1(GLint Location, size_t Length, const int32_t* Values) {
	glUniform1iv(Location, static_cast<GLsizei>(Length), Values);
}

void dankengine::ShaderProgram::setUniform1(GLint Location, const std::vector<uint32_t>& Values) {
	glUniform1uiv(Location, static_cast<GLsizei>(Values.size()), Values.data());
}

void dankengine::ShaderProgram::setUniform1(GLint Location, const std::initializer_list<uint32_t>& Values) {
	glUniform1uiv(Location, static_cast<GLsizei>(std::vector<uint32_t>(Values).size()), std::vector<uint32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform1(GLint Location, size_t Length, const uint32_t* Values) {
	glUniform1uiv(Location, static_cast<GLsizei>(Length), Values);
}

void dankengine::ShaderProgram::setUniform2(GLint Location, const std::vector<float>& Values) {
	glUniform2fv(Location, static_cast<GLsizei>(Values.size() / 2), Values.data());
}

void dankengine::ShaderProgram::setUniform2(GLint Location, const std::initializer_list<float>& Values) {
	glUniform2fv(Location, static_cast<GLsizei>(std::vector<float>(Values).size() / 2), std::vector<float>(Values).data());
}

void dankengine::ShaderProgram::setUniform2(GLint Location, size_t Length, const float* Values) {
	glUniform2fv(Location, static_cast<GLsizei>(Length / 2), Values);
}

void dankengine::ShaderProgram::setUniform2(GLint Location, const std::vector<int32_t>& Values) {
	glUniform2iv(Location, static_cast<GLsizei>(Values.size() / 2), Values.data());
}

void dankengine::ShaderProgram::setUniform2(GLint Location, const std::initializer_list<int32_t>& Values) {
	glUniform2iv(Location, static_cast<GLsizei>(std::vector<int32_t>(Values).size() / 2), std::vector<int32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform2(GLint Location, size_t Length, const int32_t* Values) {
	glUniform2iv(Location, static_cast<GLsizei>(Length / 2), Values);
}

void dankengine::ShaderProgram::setUniform2(GLint Location, const std::vector<uint32_t>& Values) {
	glUniform2uiv(Location, static_cast<GLsizei>(Values.size() / 2), Values.data());
}

void dankengine::ShaderProgram::setUniform2(GLint Location, const std::initializer_list<uint32_t>& Values) {
	glUniform2uiv(Location, static_cast<GLsizei>(std::vector<uint32_t>(Values).size() / 2), std::vector<uint32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform2(GLint Location, size_t Length, const uint32_t* Values) {
	glUniform2uiv(Location, static_cast<GLsizei>(Length / 2), Values);
}

void dankengine::ShaderProgram::setUniform3(GLint Location, const std::vector<float>& Values) {
	glUniform3fv(Location, static_cast<GLsizei>(Values.size() / 3), Values.data());
}

void dankengine::ShaderProgram::setUniform3(GLint Location, const std::initializer_list<float>& Values) {
	glUniform3fv(Location, static_cast<GLsizei>(std::vector<float>(Values).size() / 3), std::vector<float>(Values).data());
}

void dankengine::ShaderProgram::setUniform3(GLint Location, size_t Length, const float* Values) {
	glUniform3fv(Location, static_cast<GLsizei>(Length / 3), Values);
}

void dankengine::ShaderProgram::setUniform3(GLint Location, const std::vector<int32_t>& Values) {
	glUniform3iv(Location, static_cast<GLsizei>(Values.size() / 3), Values.data());
}

void dankengine::ShaderProgram::setUniform3(GLint Location, const std::initializer_list<int32_t>& Values) {
	glUniform3iv(Location, static_cast<GLsizei>(std::vector<int32_t>(Values).size() / 3), std::vector<int32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform3(GLint Location, size_t Length, const int32_t* Values) {
	glUniform3iv(Location, static_cast<GLsizei>(Length / 3), Values);
}

void dankengine::ShaderProgram::setUniform3(GLint Location, const std::vector<uint32_t>& Values) {
	glUniform3uiv(Location, static_cast<GLsizei>(Values.size() / 3), Values.data());
}

void dankengine::ShaderProgram::setUniform3(GLint Location, const std::initializer_list<uint32_t>& Values) {
	glUniform3uiv(Location, static_cast<GLsizei>(std::vector<uint32_t>(Values).size() / 3), std::vector<uint32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform3(GLint Location, size_t Length, const uint32_t* Values) {
	glUniform3uiv(Location, static_cast<GLsizei>(Length / 3), Values);
}

void dankengine::ShaderProgram::setUniform4(GLint Location, const std::vector<float>& Values) {
	glUniform4fv(Location, static_cast<GLsizei>(Values.size() / 4), Values.data());
}

void dankengine::ShaderProgram::setUniform4(GLint Location, const std::initializer_list<float>& Values) {
	glUniform4fv(Location, static_cast<GLsizei>(std::vector<float>(Values).size() / 4), std::vector<float>(Values).data());
}

void dankengine::ShaderProgram::setUniform4(GLint Location, size_t Length, const float* Values) {
	glUniform4fv(Location, static_cast<GLsizei>(Length / 4), Values);
}

void dankengine::ShaderProgram::setUniform4(GLint Location, const std::vector<int32_t>& Values) {
	glUniform4iv(Location, static_cast<GLsizei>(Values.size() / 4), Values.data());
}

void dankengine::ShaderProgram::setUniform4(GLint Location, const std::initializer_list<int32_t>& Values) {
	glUniform4iv(Location, static_cast<GLsizei>(std::vector<int32_t>(Values).size() / 4), std::vector<int32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform4(GLint Location, size_t Length, const int32_t* Values) {
	glUniform4iv(Location, static_cast<GLsizei>(Length / 4), Values);
}

void dankengine::ShaderProgram::setUniform4(GLint Location, const std::vector<uint32_t>& Values) {
	glUniform4uiv(Location, static_cast<GLsizei>(Values.size() / 4), Values.data());
}

void dankengine::ShaderProgram::setUniform4(GLint Location, const std::initializer_list<uint32_t>& Values) {
	glUniform4uiv(Location, static_cast<GLsizei>(std::vector<uint32_t>(Values).size() / 4), std::vector<uint32_t>(Values).data());
}

void dankengine::ShaderProgram::setUniform4(GLint Location, size_t Length, const uint32_t* Values) {
	glUniform4uiv(Location, static_cast<GLsizei>(Length / 4), Values);
}

void dankengine::ShaderProgram::setUniformMatrix2(GLint Location, bool Transpose, const glm::mat2& Matrix) {
	glUniformMatrix2fv(Location, 1, Transpose ? GL_TRUE : GL_FALSE, glm::value_ptr(Matrix));
}

void dankengine::ShaderProgram::setUniformMatrix3(GLint Location, bool Transpose, const glm::mat3& Matrix) {
	glUniformMatrix3fv(Location, 1, Transpose ? GL_TRUE : GL_FALSE, glm::value_ptr(Matrix));
}

void dankengine::ShaderProgram::setUniformMatrix4(GLint Location, bool Transpose, const glm::mat4& Matrix) {
	glUniformMatrix4fv(Location, 1, Transpose ? GL_TRUE : GL_FALSE, glm::value_ptr(Matrix));
}


void dankengine::ShaderProgram::link() {
	glLinkProgram(m_glid);
}

void dankengine::ShaderProgram::bind() {
	if (g_lastObjectBind != m_glid) {
		glUseProgram(m_glid);

		g_lastObjectBind = m_glid;
	}
}

void dankengine::ShaderProgram::unbind() {
	if (g_lastObjectBind == m_glid) {
		glUseProgram(0);
	}
}

GLuint dankengine::ShaderProgram::getAttribLocation(const std::string& Name) {
	if (m_attributeLocationCache.find(Name) == m_attributeLocationCache.end()) {
		m_attributeLocationCache[Name] = glGetAttribLocation(m_glid, Name.c_str());
	}

	return m_attributeLocationCache[Name];
}

GLuint dankengine::ShaderProgram::getUniformLocation(const std::string& Name) {
	if (m_uniformLocationCache.find(Name) == m_uniformLocationCache.end()) {
		m_uniformLocationCache[Name] = glGetUniformLocation(m_glid, Name.c_str());
	}

	return m_uniformLocationCache[Name];
}

GLuint dankengine::ShaderProgram::glObject() {
	return m_glid;
}
