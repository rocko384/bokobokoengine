#include "texture.h"

#include <iostream>

GLuint dankengine::Texture::g_lastObjectBind = 0;

dankengine::Texture::Texture() {
	glGenTextures(1, &m_glid);
}

dankengine::Texture::~Texture() {
	glDeleteTextures(1, &m_glid);
}

int dankengine::Texture::createFromResource2D(Resource& resource) {

	unsigned char* buffer = reinterpret_cast<unsigned char*>(resource.getDataAsPtr());
	int buffer_length = static_cast<int>(resource.getByteCount());

	m_glid = SOIL_load_OGL_texture_from_memory(
		buffer,
		buffer_length,
		SOIL_LOAD_AUTO,
		m_glid,
		SOIL_FLAG_POWER_OF_TWO
	);

#ifdef _DEBUG

	if (m_glid == 0) {
		std::cout << "Image failed to load: " << std::string(SOIL_last_result()) << "\n";
	}

#endif

	return 0;
}

int dankengine::Texture::loadFromFile2D(const std::string& Path) {
	Resource resource(Path, Resource::BINARY);
	return createFromResource2D(resource);;
}

int dankengine::Texture::setParameter(Parameter Type, int Value, Bind Target, int ActiveTexture) {
	bind(Target, ActiveTexture);

	glTexParameteri(Target, Type, Value);

	return 0;
}

int dankengine::Texture::setParameter(Parameter Type, float Value, Bind Target, int ActiveTexture) {
	bind(Target, ActiveTexture);

	glTexParameterf(Target, Type, Value);

	return 0;
}

int dankengine::Texture::setParameter(Parameter Type, const std::vector<int>& Values, Bind Target, int ActiveTexture) {
	bind(Target, ActiveTexture);

	glTexParameteriv(Target, Type, Values.data());

	return 0;
}

int dankengine::Texture::setParameter(Parameter Type, const std::initializer_list<int>& Values, Bind Target, int ActiveTexture) {
	return setParameter(Type, std::vector<int>(Values), Target);
}

int dankengine::Texture::setParameter(Parameter Type, const int* Values, Bind Target, int ActiveTexture) {
	bind(Target, ActiveTexture);

	glTexParameteriv(Target, Type, Values);

	return 0;
}

int dankengine::Texture::setParameter(Parameter Type, const std::vector<float>& Values, Bind Target, int ActiveTexture) {
	bind(Target, ActiveTexture);

	glTexParameterfv(Target, Type, Values.data());

	return 0;
}

int dankengine::Texture::setParameter(Parameter Type, const std::initializer_list<float>& Values, Bind Target, int ActiveTexture) {
	return setParameter(Type, std::vector<float>(Values), Target);
}

int dankengine::Texture::setParameter(Parameter Type, const float* Values, Bind Target, int ActiveTexture) {
	bind(Target, ActiveTexture);

	glTexParameterfv(Target, Type, Values);

	return 0;
}

void dankengine::Texture::bind(Bind Use, int ActiveTexture) {
	if ((m_lastBind != Use) || (g_lastObjectBind != m_glid)) {
		glActiveTexture(ActiveTexture);
		glBindTexture(Use, m_glid);
		m_lastBind = Use;
		g_lastObjectBind = m_glid;
	}
}

void dankengine::Texture::unbind() {
	glBindTexture(m_lastBind, 0);
	m_lastBind = 0;
	g_lastObjectBind = 0;
}

GLuint dankengine::Texture::glObject() {
	return m_glid;
}

std::shared_ptr<dankengine::Texture> dankengine::Texture::MakeUnit() {
	static std::shared_ptr<Texture> unit;
	
	if (!unit) {
		unit = std::make_shared<Texture>();

		unit->loadFromFile2D(RESOURCE_DIR_STR + "textures/unit.png");

		unit->setParameter(Texture::TEXTURE_WRAP_S, Texture::REPEAT, Texture::TEXTURE_2D);
		unit->setParameter(Texture::TEXTURE_WRAP_T, Texture::REPEAT, Texture::TEXTURE_2D);

		unit->setParameter(Texture::TEXTURE_BORDER_COLOR, { 1.0f, 0.0f, 0.0f, 1.0f });
		unit->setParameter(Texture::TEXTURE_MIN_FILTER, Texture::LINEAR);
		unit->setParameter(Texture::TEXTURE_MIN_FILTER, Texture::LINEAR);
	}

	return unit;
}
