#pragma once

#include "engine/dank.h"

#include "shader.h"
#include "gpubuffer.h"

#include <cinttypes>
#include <vector>
#include <array>
#include <map>
#include <initializer_list>

#include <glad/glad.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace dankengine {

	class DankExport ShaderProgram {
	
	public:

		ShaderProgram();
		~ShaderProgram();

		void attachShader(Shader& Shader);
		void detachShader(Shader& Shader);

		void bindFragDataLocation(GLuint ColorNumber, const std::string& Name);

		// Access by name

		void setUniform2(const std::string& Name, const glm::vec2& Value);
		void setUniform2(const std::string& Name, const glm::ivec2& Value);
		void setUniform2(const std::string& Name, const glm::uvec2& Value);

		void setUniform3(const std::string& Name, const glm::vec3& Value);
		void setUniform3(const std::string& Name, const glm::ivec3& Value);
		void setUniform3(const std::string& Name, const glm::uvec3& Value);

		void setUniform4(const std::string& Name, const glm::vec4& Value);
		void setUniform4(const std::string& Name, const glm::ivec4& Value);
		void setUniform4(const std::string& Name, const glm::uvec4& Value);

		void setUniform1(const std::string& Name, float A);
		void setUniform2(const std::string& Name, float A, float B);
		void setUniform3(const std::string& Name, float A, float B, float C);
		void setUniform4(const std::string& Name, float A, float B, float C, float D);

		void setUniform1(const std::string& Name, int32_t A);
		void setUniform2(const std::string& Name, int32_t A, int32_t B);
		void setUniform3(const std::string& Name, int32_t A, int32_t B, int32_t C);
		void setUniform4(const std::string& Name, int32_t A, int32_t B, int32_t C, int32_t D);

		void setUniform1(const std::string& Name, uint32_t A);
		void setUniform2(const std::string& Name, uint32_t A, uint32_t B);
		void setUniform3(const std::string& Name, uint32_t A, uint32_t B, uint32_t C);
		void setUniform4(const std::string& Name, uint32_t A, uint32_t B, uint32_t C, uint32_t D);

		void setUniform1(const std::string& Name, const std::vector<float>& Values);
		void setUniform1(const std::string& Name, const std::initializer_list<float>& Values);
		void setUniform1(const std::string& Name, size_t Length, const float* Values);
		template<size_t Length>
		void setUniform1(const std::string& Name, const std::array<float, Length>& Values);

		void setUniform1(const std::string& Name, const std::vector<int32_t>& Values);
		void setUniform1(const std::string& Name, const std::initializer_list<int32_t>& Values);
		void setUniform1(const std::string& Name, size_t Length, const int32_t* Values);
		template<size_t Length>
		void setUniform1(const std::string& Name, const std::array<int32_t, Length>& Values);

		void setUniform1(const std::string& Name, const std::vector<uint32_t>& Values);
		void setUniform1(const std::string& Name, const std::initializer_list<uint32_t>& Values);
		void setUniform1(const std::string& Name, size_t Length, const uint32_t* Values);
		template<size_t Length>
		void setUniform1(const std::string& Name, const std::array<uint32_t, Length>& Values);

		void setUniform2(const std::string& Name, const std::vector<float>& Values);
		void setUniform2(const std::string& Name, const std::initializer_list<float>& Values);
		void setUniform2(const std::string& Name, size_t Length, const float* Values);
		template<size_t Length>
		void setUniform2(const std::string& Name, const std::array<float, Length>& Values);

		void setUniform2(const std::string& Name, const std::vector<int32_t>& Values);
		void setUniform2(const std::string& Name, const std::initializer_list<int32_t>& Values);
		void setUniform2(const std::string& Name, size_t Length, const int32_t* Values);
		template<size_t Length>
		void setUniform2(const std::string& Name, const std::array<int32_t, Length>& Values);

		void setUniform2(const std::string& Name, const std::vector<uint32_t>& Values);
		void setUniform2(const std::string& Name, const std::initializer_list<uint32_t>& Values);
		void setUniform2(const std::string& Name, size_t Length, const uint32_t* Values);
		template<size_t Length>
		void setUniform2(const std::string& Name, const std::array<uint32_t, Length>& Values);

		void setUniform3(const std::string& Name, const std::vector<float>& Values);
		void setUniform3(const std::string& Name, const std::initializer_list<float>& Values);
		void setUniform3(const std::string& Name, size_t Length, const float* Values);
		template<size_t Length>
		void setUniform3(const std::string& Name, const std::array<float, Length>& Values);

		void setUniform3(const std::string& Name, const std::vector<int32_t>& Values);
		void setUniform3(const std::string& Name, const std::initializer_list<int32_t>& Values);
		void setUniform3(const std::string& Name, size_t Length, const int32_t* Values);
		template<size_t Length>
		void setUniform3(const std::string& Name, const std::array<int32_t, Length>& Values);

		void setUniform3(const std::string& Name, const std::vector<uint32_t>& Values);
		void setUniform3(const std::string& Name, const std::initializer_list<uint32_t>& Values);
		void setUniform3(const std::string& Name, size_t Length, const uint32_t* Values);
		template<size_t Length>
		void setUniform3(const std::string& Name, const std::array<uint32_t, Length>& Values);

		void setUniform4(const std::string& Name, const std::vector<float>& Values);
		void setUniform4(const std::string& Name, const std::initializer_list<float>& Values);
		void setUniform4(const std::string& Name, size_t Length, const float* Values);
		template<size_t Length>
		void setUniform4(const std::string& Name, const std::array<float, Length>& Values);

		void setUniform4(const std::string& Name, const std::vector<int32_t>& Values);
		void setUniform4(const std::string& Name, const std::initializer_list<int32_t>& Values);
		void setUniform4(const std::string& Name, size_t Length, const int32_t* Values);
		template<size_t Length>
		void setUniform4(const std::string& Name, const std::array<int32_t, Length>& Values);

		void setUniform4(const std::string& Name, const std::vector<uint32_t>& Values);
		void setUniform4(const std::string& Name, const std::initializer_list<uint32_t>& Values);
		void setUniform4(const std::string& Name, size_t Length, const uint32_t* Values);
		template<size_t Length>
		void setUniform4(const std::string& Name, const std::array<uint32_t, Length>& Values);

		void setUniformMatrix2(const std::string& Name, bool Transpose, const glm::mat2& Matrix);
		void setUniformMatrix3(const std::string& Name, bool Transpose, const glm::mat3& Matrix);
		void setUniformMatrix4(const std::string& Name, bool Transpose, const glm::mat4& Matrix);

		// Direct Location Access
		
		void setUniform2(GLint Location, const glm::vec2& Value);
		void setUniform2(GLint Location, const glm::ivec2& Value);
		void setUniform2(GLint Location, const glm::uvec2& Value);

		void setUniform3(GLint Location, const glm::vec3& Value);
		void setUniform3(GLint Location, const glm::ivec3& Value);
		void setUniform3(GLint Location, const glm::uvec3& Value);

		void setUniform4(GLint Location, const glm::vec4& Value);
		void setUniform4(GLint Location, const glm::ivec4& Value);
		void setUniform4(GLint Location, const glm::uvec4& Value);

		void setUniform1(GLint Location, float A);
		void setUniform2(GLint Location, float A, float B);
		void setUniform3(GLint Location, float A, float B, float C);
		void setUniform4(GLint Location, float A, float B, float C, float D);

		void setUniform1(GLint Location, int32_t A);
		void setUniform2(GLint Location, int32_t A, int32_t B);
		void setUniform3(GLint Location, int32_t A, int32_t B, int32_t C);
		void setUniform4(GLint Location, int32_t A, int32_t B, int32_t C, int32_t D);

		void setUniform1(GLint Location, uint32_t A);
		void setUniform2(GLint Location, uint32_t A, uint32_t B);
		void setUniform3(GLint Location, uint32_t A, uint32_t B, uint32_t C);
		void setUniform4(GLint Location, uint32_t A, uint32_t B, uint32_t C, uint32_t D);

		void setUniform1(GLint Location, const std::vector<float>& Values);
		void setUniform1(GLint Location, const std::initializer_list<float>& Values);
		void setUniform1(GLint Location, size_t Length, const float* Values);
		template<size_t Length>
		void setUniform1(GLint Location, const std::array<float, Length>& Values);

		void setUniform1(GLint Location, const std::vector<int32_t>& Values);
		void setUniform1(GLint Location, const std::initializer_list<int32_t>& Values);
		void setUniform1(GLint Location, size_t Length, const int32_t* Values);
		template<size_t Length>
		void setUniform1(GLint Location, const std::array<int32_t, Length>& Values);

		void setUniform1(GLint Location, const std::vector<uint32_t>& Values);
		void setUniform1(GLint Location, const std::initializer_list<uint32_t>& Values);
		void setUniform1(GLint Location, size_t Length, const uint32_t* Values);
		template<size_t Length>
		void setUniform1(GLint Location, const std::array<uint32_t, Length>& Values);

		void setUniform2(GLint Location, const std::vector<float>& Values);
		void setUniform2(GLint Location, const std::initializer_list<float>& Values);
		void setUniform2(GLint Location, size_t Length, const float* Values);
		template<size_t Length>
		void setUniform2(GLint Location, const std::array<float, Length>& Values);

		void setUniform2(GLint Location, const std::vector<int32_t>& Values);
		void setUniform2(GLint Location, const std::initializer_list<int32_t>& Values);
		void setUniform2(GLint Location, size_t Length, const int32_t* Values);
		template<size_t Length>
		void setUniform2(GLint Location, const std::array<int32_t, Length>& Values);

		void setUniform2(GLint Location, const std::vector<uint32_t>& Values);
		void setUniform2(GLint Location, const std::initializer_list<uint32_t>& Values);
		void setUniform2(GLint Location, size_t Length, const uint32_t* Values);
		template<size_t Length>
		void setUniform2(GLint Location, const std::array<uint32_t, Length>& Values);

		void setUniform3(GLint Location, const std::vector<float>& Values);
		void setUniform3(GLint Location, const std::initializer_list<float>& Values);
		void setUniform3(GLint Location, size_t Length, const float* Values);
		template<size_t Length>
		void setUniform3(GLint Location, const std::array<float, Length>& Values);

		void setUniform3(GLint Location, const std::vector<int32_t>& Values);
		void setUniform3(GLint Location, const std::initializer_list<int32_t>& Values);
		void setUniform3(GLint Location, size_t Length, const int32_t* Values);
		template<size_t Length>
		void setUniform3(GLint Location, const std::array<int32_t, Length>& Values);

		void setUniform3(GLint Location, const std::vector<uint32_t>& Values);
		void setUniform3(GLint Location, const std::initializer_list<uint32_t>& Values);
		void setUniform3(GLint Location, size_t Length, const uint32_t* Values);
		template<size_t Length>
		void setUniform3(GLint Location, const std::array<uint32_t, Length>& Values);

		void setUniform4(GLint Location, const std::vector<float>& Values);
		void setUniform4(GLint Location, const std::initializer_list<float>& Values);
		void setUniform4(GLint Location, size_t Length, const float* Values);
		template<size_t Length>
		void setUniform4(GLint Location, const std::array<float, Length>& Values);

		void setUniform4(GLint Location, const std::vector<int32_t>& Values);
		void setUniform4(GLint Location, const std::initializer_list<int32_t>& Values);
		void setUniform4(GLint Location, size_t Length, const int32_t* Values);
		template<size_t Length>
		void setUniform4(GLint Location, const std::array<int32_t, Length>& Values);

		void setUniform4(GLint Location, const std::vector<uint32_t>& Values);
		void setUniform4(GLint Location, const std::initializer_list<uint32_t>& Values);
		void setUniform4(GLint Location, size_t Length, const uint32_t* Values);
		template<size_t Length>
		void setUniform4(GLint Location, const std::array<uint32_t, Length>& Values);

		void setUniformMatrix2(GLint Location, bool Transpose, const glm::mat2& Matrix);
		void setUniformMatrix3(GLint Location, bool Transpose, const glm::mat3& Matrix);
		void setUniformMatrix4(GLint Location, bool Transpose, const glm::mat4& Matrix);



		void link();

		void bind();
		void unbind();

		GLuint getAttribLocation(const std::string& Name);
		GLuint getUniformLocation(const std::string& Name);

		GLuint glObject();

	private:

		GLuint m_glid;		// GL "pointer" thing
		static GLuint g_lastObjectBind;		// Last bound shader

		std::map<const std::string, GLuint> m_attributeLocationCache;
		std::map<const std::string, GLuint> m_uniformLocationCache;

	};

	template<size_t Length>
	inline void ShaderProgram::setUniform1(const std::string& Name, const std::array<float, Length>& Values) {
		glUniform1fv(getAttribLocation(Name), Values.size(), Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform1(const std::string& Name, const std::array<int32_t, Length>& Values) {
		glUniform1iv(getAttribLocation(Name), Values.size(), Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform1(const std::string& Name, const std::array<uint32_t, Length>& Values) {
		glUniform1uiv(getAttribLocation(Name), Values.size(), Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform2(const std::string& Name, const std::array<float, Length>& Values) {
		glUniform2fv(getAttribLocation(Name), Values.size() / 2, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform2(const std::string& Name, const std::array<int32_t, Length>& Values) {
		glUniform2iv(getAttribLocation(Name), Values.size() / 2, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform2(const std::string& Name, const std::array<uint32_t, Length>& Values) {
		glUniform2uiv(getAttribLocation(Name), Values.size() / 2, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform3(const std::string& Name, const std::array<float, Length>& Values) {
		glUniform3fv(getAttribLocation(Name), Values.size() / 3, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform3(const std::string& Name, const std::array<int32_t, Length>& Values) {
		glUniform3iv(getAttribLocation(Name), Values.size() / 3, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform3(const std::string& Name, const std::array<uint32_t, Length>& Values) {
		glUniform3uiv(getAttribLocation(Name), Values.size() / 3, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform4(const std::string& Name, const std::array<float, Length>& Values) {
		glUniform4fv(getAttribLocation(Name), Values.size() / 4, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform4(const std::string& Name, const std::array<int32_t, Length>& Values) {
		glUniform4iv(getAttribLocation(Name), Values.size() / 4, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform4(const std::string& Name, const std::array<uint32_t, Length>& Values) {
		glUniform4uiv(getAttribLocation(Name), Values.size() / 4, Values.data());
	}


	template<size_t Length>
	inline void ShaderProgram::setUniform1(GLint Location, const std::array<float, Length>& Values) {
		glUniform1fv(Location, Values.size(), Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform1(GLint Location, const std::array<int32_t, Length>& Values) {
		glUniform1iv(Location, Values.size(), Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform1(GLint Location, const std::array<uint32_t, Length>& Values) {
		glUniform1uiv(Location, Values.size(), Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform2(GLint Location, const std::array<float, Length>& Values) {
		glUniform2fv(Location, Values.size() / 2, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform2(GLint Location, const std::array<int32_t, Length>& Values) {
		glUniform2iv(Location, Values.size() / 2, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform2(GLint Location, const std::array<uint32_t, Length>& Values) {
		glUniform2uiv(Location, Values.size() / 2, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform3(GLint Location, const std::array<float, Length>& Values) {
		glUniform3fv(Location, Values.size() / 3, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform3(GLint Location, const std::array<int32_t, Length>& Values) {
		glUniform3iv(Location, Values.size() / 3, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform3(GLint Location, const std::array<uint32_t, Length>& Values) {
		glUniform3uiv(Location, Values.size() / 3, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform4(GLint Location, const std::array<float, Length>& Values) {
		glUniform4fv(Location, Values.size() / 4, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform4(GLint Location, const std::array<int32_t, Length>& Values) {
		glUniform4iv(Location, Values.size() / 4, Values.data());
	}

	template<size_t Length>
	inline void ShaderProgram::setUniform4(GLint Location, const std::array<uint32_t, Length>& Values) {
		glUniform4uiv(Location, Values.size() / 4, Values.data());
	}

}