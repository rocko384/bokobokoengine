#pragma once

#include "engine/dank.h"

#include <glad/glad.h>

#include <SOIL2.h>

#include <vector>
#include <array>
#include <string>
#include <initializer_list>

#include "engine/resources/resource.h"

namespace dankengine {

	class DankExport Texture {

	public:

		enum Bind {
			TEXTURE_1D =					GL_TEXTURE_1D,
			TEXTURE_2D =					GL_TEXTURE_2D,
			TEXTURE_3D =					GL_TEXTURE_3D,
			TEXTURE_1D_ARRAY =				GL_TEXTURE_1D_ARRAY,
			TEXTURE_2D_ARRAY =				GL_TEXTURE_2D_ARRAY,
			TEXTURE_RECTANGLE =				GL_TEXTURE_RECTANGLE,
			TEXTURE_CUBE_MAP =				GL_TEXTURE_CUBE_MAP,
			TEXTURE_BUFFER =				GL_TEXTURE_BUFFER,
			TEXTURE_2D_MULTISAMPLE =		GL_TEXTURE_2D_MULTISAMPLE,
			TEXTURE_2D_MULTISAMPLE_ARRAY =	GL_TEXTURE_2D_MULTISAMPLE_ARRAY
		};

		static int ActiveTexture(int I) {
			return GL_TEXTURE0 + I;
		};

		static std::shared_ptr<Texture> MakeUnit();

		enum InternalFormat {
			RGBA32F =						GL_RGBA32F,
			RGBA32I =						GL_RGBA32I,
			RGBA32UI =						GL_RGBA32UI,
			RGBA16 =						GL_RGBA16,
			RGBA16F =						GL_RGBA16F,
			RGBA16I =						GL_RGBA16I,
			RGBA16UI =						GL_RGBA16UI,
			RGBA8 =							GL_RGBA8,
			RGBA8UI =						GL_RGBA8UI,
			SRGB8_ALPHA8 =					GL_SRGB8_ALPHA8,
			RGB10_A2 =						GL_RGB10_A2,
			RGB10_A2UI =					GL_RGB10_A2UI,
			R11F_G11F_B10F =				GL_R11F_G11F_B10F,
			RG32F =							GL_RG32F,
			RG32I =							GL_RG32I,
			RG32UI =						GL_RG32UI,
			RG16 =							GL_RG16,
			RG16F =							GL_RG16F,
			RGB16F =						GL_RGB16F,
			RGB16I =						GL_RGB16I,
			RGB16UI =						GL_RGB16UI,
			RGB16_SNORM =					GL_RGB16_SNORM,
			RGB16 =							GL_RGB16,
			RG8 =							GL_RG8,
			RG8I =							GL_RG8I,
			RG8UI =							GL_RG8UI,
			R32F =							GL_R32F,
			R32I =							GL_R32I,
			R32UI =							GL_R32UI,
			R16F =							GL_R16F,
			R16I =							GL_R16I,
			R16UI =							GL_R16UI,
			R8 =							GL_R8,
			R8I =							GL_R8I,
			R8UI =							GL_R8UI,
			RGBA16_SNORM =					GL_RGBA16_SNORM,
			RGBA8_SNORM =					GL_RGBA8_SNORM,
			RGB32F =						GL_RGB32F,
			RGB32I =						GL_RGB32I,
			RGB32UI =						GL_RGB32UI,
			RGB8_SNORM =					GL_RGB8_SNORM,
			RGB8 =							GL_RGB8,
			RGB8I =							GL_RGB8I,
			RGB8UI =						GL_RGB8UI,
			SRGB8 =							GL_SRGB8,
			RGB9_E5 =						GL_RGB9_E5,
			RG16_SNORM =					GL_RG16_SNORM,
			RG8_SNORM =						GL_RG8_SNORM,
			COMPRESSED_RG_RGTC2 =			GL_COMPRESSED_RG_RGTC2,
			COMPRESSED_SIGNED_RG_RGTC2 =	GL_COMPRESSED_SIGNED_RG_RGTC2,
			R16_SNORM =						GL_R16_SNORM,
			R8_SNORM =						GL_R8_SNORM,
			COMPRESSED_RED_RGTC1 =			GL_COMPRESSED_RED_RGTC1,
			COMPRESSED_SIGNED_RED_RGTC1 =	GL_COMPRESSED_SIGNED_RED_RGTC1,
			DEPTH_COMPONENT32F =			GL_DEPTH_COMPONENT32F,
			DEPTH_COMPONENT24 =				GL_DEPTH_COMPONENT24,
			DEPTH_COMPONENT16 =				GL_DEPTH_COMPONENT16,
			DEPTH32F_STENCIL8 =				GL_DEPTH32F_STENCIL8,
			DEPTH24_STENCIL8 =				GL_DEPTH24_STENCIL8
		};

		enum Format {
			RED =							GL_RED,
			RG =							GL_RG,
			RGB =							GL_RGB,
			BGR =							GL_BGR,
			RGBA =							GL_RGBA,
			BGRA =							GL_BGRA
		};

		enum PixelType {
			UNSIGNED_BYTE =					GL_UNSIGNED_BYTE,
			BYTE =							GL_BYTE,
			UNSIGNED_SHORT =				GL_UNSIGNED_SHORT,
			SHORT =							GL_SHORT,
			UNSIGNED_INT =					GL_UNSIGNED_INT,
			INT =							GL_INT,
			FLOAT =							GL_FLOAT,
			UNSIGNED_BYTE_3_3_2 =			GL_UNSIGNED_BYTE_3_3_2,
			UNSIGNED_BYTE_2_3_3_REV =		GL_UNSIGNED_BYTE_2_3_3_REV,
			UNSIGNED_SHORT_5_6_5 =			GL_UNSIGNED_SHORT_5_6_5,
			UNSIGNED_SHORT_5_6_5_REV =		GL_UNSIGNED_SHORT_5_6_5_REV,
			UNSIGNED_SHORT_4_4_4_4 =		GL_UNSIGNED_SHORT_4_4_4_4,
			UNSIGNED_SHORT_4_4_4_4_REV =	GL_UNSIGNED_SHORT_4_4_4_4_REV,
			UNSIGNED_SHORT_5_5_5_1 =		GL_UNSIGNED_SHORT_5_5_5_1,
			UNSIGNED_SHORT_1_5_5_5_REV =	GL_UNSIGNED_SHORT_1_5_5_5_REV,
			UNSIGNED_INT_8_8_8_8 =			GL_UNSIGNED_INT_8_8_8_8,
			UNSIGNED_INT_8_8_8_8_REV =		GL_UNSIGNED_INT_8_8_8_8_REV,
			UNSIGNED_INT_10_10_10_2 =		GL_UNSIGNED_INT_10_10_10_2,
			UNSIGNED_INT_2_10_10_10_REV =	GL_UNSIGNED_INT_2_10_10_10_REV
		};

		enum Parameter {
			TEXTURE_BASE_LEVEL =			GL_TEXTURE_BASE_LEVEL,
			TEXTURE_BORDER_COLOR =			GL_TEXTURE_BORDER_COLOR,
			TEXTURE_COMPARE_MODE =			GL_TEXTURE_COMPARE_MODE,
			TEXTURE_COMPARE_FUNC =			GL_TEXTURE_COMPARE_FUNC,
			TEXTURE_LOD_BIAS =				GL_TEXTURE_LOD_BIAS,
			TEXTURE_MAG_FILTER =			GL_TEXTURE_MAG_FILTER,
			TEXTURE_MAX_LEVEL =				GL_TEXTURE_MAX_LEVEL,
			TEXTURE_MAX_LOD =				GL_TEXTURE_MAX_LOD,
			TEXTURE_MIN_FILTER =			GL_TEXTURE_MIN_FILTER,
			TEXTURE_MIN_LOD =				GL_TEXTURE_MIN_LOD,
			TEXTURE_SWIZZLE_R =				GL_TEXTURE_SWIZZLE_R,
			TEXTURE_SWIZZLE_G =				GL_TEXTURE_SWIZZLE_G,
			TEXTURE_SWIZZLE_B =				GL_TEXTURE_SWIZZLE_B,
			TEXTURE_SWIZZLE_A =				GL_TEXTURE_SWIZZLE_A,
			TEXTURE_SWIZZLE_RGBA =			GL_TEXTURE_SWIZZLE_RGBA,
			TEXTURE_WRAP_S =				GL_TEXTURE_WRAP_S,
			TEXTURE_WRAP_T =				GL_TEXTURE_WRAP_T,
			TEXTURE_WRAP_R =				GL_TEXTURE_WRAP_R
		};

		enum ParameterValue {
			LESS_EQUAL =				GL_LEQUAL,
			GREATER_EQUAL =				GL_GEQUAL,
			LESS =						GL_LESS,
			GREATER =					GL_GREATER,
			EQUAL =						GL_EQUAL,
			NOTEQUAL =					GL_NOTEQUAL,
			ALWAYS =					GL_ALWAYS,
			NEVER =						GL_NEVER,
			COMPARE_REF_TO_TEXTURE =	GL_COMPARE_REF_TO_TEXTURE,
			NONE =						GL_NONE,
			MAX_TEXTURE_LOD_BIAS =		GL_MAX_TEXTURE_LOD_BIAS,
			NEAREST =					GL_NEAREST,
			LINEAR =					GL_LINEAR,
			NEAREST_MIPMAP_NEAREST =	GL_NEAREST_MIPMAP_NEAREST,
			LINEAR_MIPMAP_NEAREST =		GL_LINEAR_MIPMAP_NEAREST,
			NEAREST_MIPMAP_LINEAR =		GL_NEAREST_MIPMAP_LINEAR,
			LINEAR_MIPMAP_LINEAR =		GL_LINEAR_MIPMAP_LINEAR,
//			RED =						GL_RED,
			GREEN =						GL_GREEN,
			BLUE =						GL_BLUE,
			ALPHA =						GL_ALPHA,
			ZERO =						GL_ZERO,
			ONE =						GL_ONE,
			CLAMP_TO_EDGE =				GL_CLAMP_TO_EDGE,
			CLAMP_TO_BORDER =			GL_CLAMP_TO_BORDER,
			MIRRORED_REPEAT =			GL_MIRRORED_REPEAT,
			REPEAT =					GL_REPEAT
		};

	public:

		Texture();
		~Texture();

		int createFromResource2D(Resource& resource);

		template<typename T>
		int createFromData2D(
			Bind Target,
			GLint Level,
			InternalFormat InternalFormat,
			size_t Width,
			size_t Height,
			Format Format,
			PixelType Type, 
			const std::vector<T>& Data
		);

		template<typename T, size_t Size>
		int createFromData2D(
			Bind Target,
			GLint Level,
			InternalFormat InternalFormat,
			size_t Width,
			size_t Height,
			Format Format,
			PixelType Type,
			const std::array<T, Size>& Data
		);

		template<typename T>
		int createFromData2D(
			Bind Target,
			GLint Level,
			InternalFormat InternalFormat,
			size_t Width,
			size_t Height,
			Format Format,
			PixelType Type,
			const std::initializer_list<T>& Data
		);

		template<typename T>
		int createFromData2D(
			Bind Target,
			GLint Level,
			InternalFormat InternalFormat,
			size_t Width,
			size_t Height,
			Format Format,
			PixelType Type, 
			const T* Data
		);

		int loadFromFile2D(const std::string& Path);

		int setParameter(Parameter Type, int Value, Bind Target = TEXTURE_2D, int ActiveTexture = 0);

		int setParameter(Parameter Type, float Value, Bind Target = TEXTURE_2D, int ActiveTexture = 0);

		template<size_t Size>
		int setParameter(Parameter Type, const std::array<int, Size>& Values, Bind Target = TEXTURE_2D, int ActiveTexture = 0);

		int setParameter(Parameter Type, const std::vector<int>& Values, Bind Target = TEXTURE_2D, int ActiveTexture = 0);

		int setParameter(Parameter Type, const std::initializer_list<int>& Values, Bind Target = TEXTURE_2D, int ActiveTexture = 0);

		int setParameter(Parameter Type, const int* Values, Bind Target = TEXTURE_2D, int ActiveTexture = 0);

		template<size_t Size>
		int setParameter(Parameter Type, const std::array<float, Size>& Values, Bind Target = TEXTURE_2D, int ActiveTexture = 0);

		int setParameter(Parameter Type, const std::vector<float>& Values, Bind Target = TEXTURE_2D, int ActiveTexture = 0);

		int setParameter(Parameter Type, const std::initializer_list<float>& Values, Bind Target = TEXTURE_2D, int ActiveTexture = 0);

		int setParameter(Parameter Type, const float* Values, Bind Target = TEXTURE_2D, int ActiveTexture = 0);

		void bind(Bind Use, int ActiveTexture);
		void unbind();

		GLuint glObject();

	private:

		GLuint m_glid;
		static GLuint g_lastObjectBind;
		GLenum m_lastBind;

	};
	

	template<typename T>
	inline int Texture::createFromData2D(Bind Target, GLint Level, InternalFormat InternalFormat, size_t Width, size_t Height, Format Format, PixelType Type, const std::vector<T>& Data) {
		return createFromData2D(Target, Level, InternalFormat, Width, Height, Format, Type, Data.data());
	}

	template<typename T, size_t Size>
	inline int Texture::createFromData2D(Bind Target, GLint Level, InternalFormat InternalFormat, size_t Width, size_t Height, Format Format, PixelType Type, const std::array<T, Size>& Data) {
		return createFromData2D(Target, Level, InternalFormat, Width, Height, Format, Type, Data.data());
	}

	template<typename T>
	inline int Texture::createFromData2D(Bind Target, GLint Level, InternalFormat InternalFormat, size_t Width, size_t Height, Format Format, PixelType Type, const std::initializer_list<T>& Data) {
		return createFromData2D(Target, Level, InternalFormat, Width, Height, Format, Type, std::vector<T>(Data));
	}

	template<typename T>
	inline int dankengine::Texture::createFromData2D(Bind Target, GLint Level, InternalFormat InternalFormat, size_t Width, size_t Height, Format Format, PixelType Type, const T* Data) {
		bind(Target, Texture::ActiveTexture(0));

		glTexImage2D(Target, Level, InternalFormat, static_cast<GLsizei>(Width), static_cast<GLsizei>(Height), 0, Format, Type, Data);

		return 0;
	}

	template<size_t Size>
	inline int Texture::setParameter(Parameter Type, const std::array<int, Size>& Values, Bind Target, int ActiveTexture) {
		bind(Target, ActiveTexture);

		glTexParameteriv(Target, Type, Values.data());

		return 0;
	}

	template<size_t Size>
	inline int Texture::setParameter(Parameter Type, const std::array<float, Size>& Values, Bind Target, int ActiveTexture) {
		bind(Target, ActiveTexture);

		glTexParameterfv(Target, Type, Values.data());

		return 0;
	}

}