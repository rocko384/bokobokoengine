#pragma once

#include "engine/dank.h"

#include <glad/glad.h>

#include <vector>
#include <array>
#include <initializer_list>

namespace dankengine {

	class DankExport GPUBuffer {

	public:

		enum Access {
			STREAM_READ =			GL_STREAM_READ,
			STREAM_COPY =			GL_STREAM_COPY,
			STREAM_DRAW =			GL_STREAM_DRAW,
			STATIC_READ =			GL_STATIC_READ,
			STATIC_COPY =			GL_STATIC_COPY,
			STATIC_DRAW =			GL_STATIC_DRAW,
			DYNAMIC_READ =			GL_DYNAMIC_READ,
			DYNAMIC_COPY =			GL_DYNAMIC_COPY,
			DYNAMIC_DRAW =			GL_DYNAMIC_DRAW
		};

		enum Bind {
			ARRAY =					GL_ARRAY_BUFFER,
			ATOMIC_COUNTER =		GL_ATOMIC_COUNTER_BUFFER,
			COPY_WRITE =			GL_COPY_WRITE_BUFFER,
			COPY_READ =				GL_COPY_READ_BUFFER,
			DISPATCH_INDIRECT =		GL_DISPATCH_INDIRECT_BUFFER,
			DRAW_INDIRECT =			GL_DRAW_INDIRECT_BUFFER,
			ELEMENT_ARRAY =			GL_ELEMENT_ARRAY_BUFFER,
			PIXEL_PACK =			GL_PIXEL_PACK_BUFFER,
			PIXEL_UNPACK =			GL_PIXEL_UNPACK_BUFFER,
			SHADER_STORAGE =		GL_SHADER_STORAGE_BUFFER,
			TEXTURE =				GL_TEXTURE_BUFFER,
			TRANSFORM_FEEDBACK =	GL_TRANSFORM_FEEDBACK_BUFFER,
			UNIFORM =				GL_UNIFORM_BUFFER
		};

	public:

		GPUBuffer();
		~GPUBuffer();

		template<typename T>
		void setData(const std::vector<T>& Data, Access A = STATIC_DRAW, Bind Use = ARRAY);

		template<typename T, size_t Size>
		void setData(const std::array<T, Size>& Data, Access A = STATIC_DRAW, Bind Use = ARRAY);

		template<typename T>
		void setData(const std::initializer_list<T>& Data, Access A = STATIC_DRAW, Bind Use = ARRAY);

		template<typename T>
		void setData(const T* Data, size_t Size, Access A = STATIC_DRAW, Bind Use = ARRAY);


		template<typename T>
		void setSubData(const std::vector<T>& Data, size_t ByteOffset, Bind Use = ARRAY);

		template<typename T, size_t Size>
		void setSubData(const std::array<T, Size>& Data, size_t ByteOffset, Bind Use = ARRAY);

		template<typename T>
		void setSubData(const std::initializer_list<T>& Data, size_t ByteOffset, Bind Use = ARRAY);

		template<typename T>
		void setSubData(const T* Data, size_t Size, size_t ByteOffset, Bind Use = ARRAY);


		template<typename T>
		void getData(std::vector<T>& Dest, Bind Use = ARRAY);

		template<typename T, size_t Size>
		void getData(std::array<T, Size>& Dest, Bind Use = ARRAY);

		template<typename T>
		void getData(T* Dest, size_t Size, Bind Use = ARRAY);


		template<typename T>
		void getSubData(std::vector<T>& Dest, size_t ByteOffset, size_t Length, Bind Use = ARRAY);

		template<typename T, size_t Size>
		void getSubData(std::array<T, Size>& Dest, size_t ByteOffset, size_t Length, Bind Use = ARRAY);

		template<typename T>
		void getSubData(T* Dest, size_t Size, size_t ByteOffset, size_t Length, Bind Use = ARRAY);


		void bind(Bind Use);
		void unbind();

		GLuint glObject();

	private:

		GLuint m_glid;		// GL "pointer" thing
		static GLuint g_lastObjectBind;		// Last bound buffer
		GLenum m_lastBind;	// Last binding location

	};

	template<typename T>
	inline void GPUBuffer::setData(const std::vector<T>& Data, Access A, Bind Use) {
		bind(Use);

		glBufferData(Use, Data.size() * sizeof(T), Data.data(), A);
	}

	template<typename T, size_t Size>
	inline void GPUBuffer::setData(const std::array<T, Size>& Data, Access A, Bind Use) {
		bind(Use);

		glBufferData(Use, Data.size() * sizeof(T), Data.data(), A);
	}

	template<typename T>
	inline void GPUBuffer::setData(const std::initializer_list<T>& Data, Access A, Bind Use) {
		setData(std::vector<T>(Data), A, Use);
	}

	template<typename T>
	inline void GPUBuffer::setData(const T* Data, size_t Size, Access A, Bind Use) {
		bind(Use);

		glBufferData(Use, Size * sizeof(T), Data, A);
	}

	template<typename T>
	inline void GPUBuffer::setSubData(const std::vector<T>& Data, size_t ByteOffset, Bind Use) {
		bind(Use);

		glBufferSubData(Use, ByteOffset, Data.size() * sizeof(T), Data.data());
	}

	template<typename T, size_t Size>
	inline void GPUBuffer::setSubData(const std::array<T, Size>& Data, size_t ByteOffset, Bind Use) {
		bind(Use);

		glBufferSubData(Use, ByteOffset, Data.size() * sizeof(T), Data.data());
	}

	template<typename T>
	inline void GPUBuffer::setSubData(const std::initializer_list<T>& Data, size_t ByteOffset, Bind Use) {
		setSubData(std::vector<T>(Data), ByteOffset, Use);
	}

	template<typename T>
	inline void GPUBuffer::setSubData(const T* Data, size_t Size, size_t ByteOffset, Bind Use) {
		bind(Use);

		glBufferSubData(Use, ByteOffset, Size * sizeof(T), Data);
	}

	template<typename T>
	inline void GPUBuffer::getData(std::vector<T>& Dest, Bind Use) {
		bind(Use);

		glGetBufferSubData(Use, 0, Dest.size() * sizeof(T), Dest.data());
	}

	template<typename T, size_t Size>
	inline void GPUBuffer::getData(std::array<T, Size>& Dest, Bind Use) {
		bind(Use);

		glGetBufferSubData(Use, 0, Dest.size() * sizeof(T), Dest.data());
	}

	template<typename T>
	inline void GPUBuffer::getData(T* Dest, size_t Size, Bind Use) {
		bind(Use);

		glGetBufferSubData(Use, 0, Size, Dest);
	}

	template<typename T>
	inline void GPUBuffer::getSubData(std::vector<T>& Dest, size_t ByteOffset, size_t Length, Bind Use) {
		bind(Use);

		glGetBufferSubData(Use, ByteOffset, Length, Dest.data());
	}

	template<typename T, size_t Size>
	inline void GPUBuffer::getSubData(std::array<T, Size>& Dest, size_t ByteOffset, size_t Length, Bind Use) {
		bind(Use);

		glGetBufferSubData(Use, ByteOffset, Length, Dest.data());
	}

	template<typename T>
	inline void GPUBuffer::getSubData(T* Dest, size_t Size, size_t ByteOffset, size_t Length, Bind Use) {
		bind(Use);

		glGetBufferSubData(Use, ByteOffset, Length, Dest);
	}

}