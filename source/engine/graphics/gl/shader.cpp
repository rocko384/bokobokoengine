#include "shader.h"

dankengine::Shader::Shader() {
	m_glid = 0;
}

dankengine::Shader::Shader(Type ShaderType) {
	create(ShaderType);
}

dankengine::Shader::Shader(const std::string& Source) {
	addToSource(Source);
}

dankengine::Shader::Shader(Resource& Resource) {
	addToSource(Resource);
}

dankengine::Shader::~Shader(){
	if (m_glid != 0) {
		glDeleteShader(m_glid);
	}
}

void dankengine::Shader::create(Type ShaderType) {
	m_glid = glCreateShader(ShaderType);
}

void dankengine::Shader::addToSource(const std::string& Source) {
	m_source.push_back(Source);
}

void dankengine::Shader::addToSource(Resource& Resource) {
	m_source.push_back(Resource.getDataAsString());
}

void dankengine::Shader::addToSourceFromFile(const std::string& Filepath) {
	m_source.push_back(Resource(Filepath).getDataAsString());
}

std::string dankengine::Shader::compile(bool OutputLog) {
	std::string ret = "success";

	std::vector<char*> rawStrings;
	std::vector<GLint> rawStringLengths;

	std::for_each(m_source.begin(), m_source.end(), [&rawStrings, &rawStringLengths](std::string& s) {
		char* temp = new char[s.length()];
		GLint l = static_cast<GLint>(s.length());

		std::memcpy(temp, s.data(), l);

		rawStrings.push_back(temp);
		rawStringLengths.push_back(l);
	});

	glShaderSource(m_glid, static_cast<GLsizei>(m_source.size()), rawStrings.data(), rawStringLengths.data());

	std::for_each(rawStrings.begin(), rawStrings.end(), [](char* s) {
		delete[] s;
	});

	glCompileShader(m_glid);

	GLint status;
	glGetShaderiv(m_glid, GL_COMPILE_STATUS, &status);

	if (status == GL_FALSE) {
		if (OutputLog) {
			char logString[1024];

			glGetShaderInfoLog(m_glid, 1024, NULL, logString);

			ret = std::string(logString);
		}
		else {
			ret = "error";
		}
	}

	return ret;
}

GLuint dankengine::Shader::glObject() {
	return m_glid;
}
