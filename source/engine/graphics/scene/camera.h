#pragma once

#include "engine/dank.h"

#include <glm/gtc/matrix_transform.hpp>

namespace dankengine {

	class DankExport Camera {

	public:

		Camera();
		~Camera();

		void viewportDimensions(float Width, float Height);
		void fov(float FOV);
		void zBoundaries(float zNear, float zFar);
		void orthographic(float Width, float Height);
		void orthographic();
		void perspective(float Width, float Height, float FOV, float zNear, float zFar);
		void perspective();

		glm::mat4& getMatrix();

		bool active;

	private:

		glm::mat4 m_matrix;
		float m_width;
		float m_height;
		float m_fov;
		float m_zNear;
		float m_zFar;

	};

}