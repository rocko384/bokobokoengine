#pragma once

#include "engine/dank.h"

#include <glm/vec3.hpp>

namespace dankengine {

	class DankExport Light {
		
	public:

		enum class Type {
			POINT,
			DIRECTIONAL,
			SPOT
		};

		Type type;

		float intensity;
		float aperture;

		glm::vec3 direction;

		glm::vec3 diffuse;
		glm::vec3 specular;

	};

}