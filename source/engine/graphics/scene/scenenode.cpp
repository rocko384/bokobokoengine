#include "scenenode.h"

dankengine::SceneNode::SceneNode() {
	position = glm::vec3(0, 0, 0);
	scale = glm::vec3(1, 1, 1);
	rotation = glm::quat(1, 0, 0, 0);
	name = "";

	m_parent = nullptr;
}

dankengine::SceneNode::SceneNode(const std::string& Name) {
	name = Name;
}

dankengine::SceneNode::~SceneNode() {
}

dankengine::Handle<dankengine::SceneNode> dankengine::SceneNode::operator[](int Index) {
	return m_children[Index];
}

std::optional<dankengine::Handle<dankengine::SceneNode>> dankengine::SceneNode::operator[](const std::string& rhs) {
	for (auto& s : m_children) {
		if (s->name == rhs) {
			return s;
		}
	}
	return {};
}

std::optional<dankengine::Handle<dankengine::SceneNode>> dankengine::SceneNode::findChildByName(const std::string& Name) {
	return operator[](Name);
}

dankengine::SceneNode& dankengine::SceneNode::parent() {
	return *m_parent;
}

std::vector<dankengine::Handle<dankengine::SceneNode>>& dankengine::SceneNode::children() {
	return m_children;
}

bool dankengine::SceneNode::addChild(Handle<SceneNode>& Node) {
	
	if (!findChildByName(Node->name)) {
		if (Node->m_parent != nullptr) {
			Node->m_parent->removeChild(Node->name);
		}

		m_children.push_back(Node);
		Node->m_parent = this;

		return true;
	}

	return false;
}

void dankengine::SceneNode::removeChild(const std::string& Name) {
	for (int i = 0; i < m_children.size(); i++) {
		if ((m_children.begin() + i)->operator->()->name == Name) {
			m_children[i]->m_parent = nullptr;
			m_children.erase(m_children.begin() + i);
			return;
		}
	}
}

void dankengine::SceneNode::removeChild(int Index) {
	m_children[Index]->m_parent = nullptr;
	m_children.erase(m_children.begin() + Index);
}

void dankengine::SceneNode::clear() {
	tie();
}

void dankengine::SceneNode::tie(empty_t Item) {
	item = Item;
}
