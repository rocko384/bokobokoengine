#pragma once

#include "engine/dank.h"

#include "scenenode.h"

namespace dankengine {

	class DankExport Scene {

	public:

		Scene();
		~Scene();

		SceneNode root;

	};

}