#pragma once

#include "engine/dank.h"
#include "engine/handle.h"

#include "engine/graphics/renderable.h"
#include "camera.h"
#include "light.h"

#include <vector>
#include <variant>
#include <optional>

#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

namespace dankengine {

	class DankExport SceneNode {

	public:

		SceneNode();
		SceneNode(const std::string& Name);
		~SceneNode();

		Handle<SceneNode> operator[](int Index);
		std::optional<Handle<SceneNode>> operator[](const std::string& Name);

		std::optional<Handle<SceneNode>> findChildByName(const std::string& Name);

		SceneNode& parent();
		std::vector<Handle<SceneNode>>& children();

		bool addChild(Handle<SceneNode>& Node);

		void removeChild(const std::string& Name);
		void removeChild(int Index);



		typedef int empty_t;
		typedef std::variant<empty_t, Handle<Renderable>, Handle<Camera>, Handle<Light>> nodeSlot_t;

		void clear();
		void tie(empty_t Item = 0);

		template<typename T>
		void tie(Handle<T>& Item);

		nodeSlot_t item;

		glm::vec3 position;
		glm::vec3 scale;
		glm::quat rotation;

		std::string name;

	private:

		SceneNode* m_parent;
		std::vector<Handle<SceneNode>> m_children;

	};

	template<typename T>
	inline void SceneNode::tie(Handle<T>& Item) {
		constexpr bool isRenderable = std::is_base_of<Renderable, T>();
		constexpr bool isCamera = std::is_base_of<Camera, T>();
		constexpr bool isLight = std::is_base_of<Light, T>();

		static_assert(isRenderable || isCamera || isLight, "Type is not SceneNode compliant");

		if constexpr (isRenderable) {
			auto handle = std::static_pointer_cast<Renderable>(Item);

			item = handle;
		}
		else if constexpr (isCamera) {
			auto handle = std::static_pointer_cast<Camera>(Item);

			item = handle;
		}
		else if constexpr (isLight) {
			auto handle = std::static_pointer_cast<Light>(Item);
			
			item = handle;
		}
	}

}
