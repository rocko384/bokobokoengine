#include "camera.h"

dankengine::Camera::Camera() {
	active = false;
}

dankengine::Camera::~Camera() {
}

void dankengine::Camera::viewportDimensions(float Width, float Height) {
	m_width = Width;
	m_height = Height;
}

void dankengine::Camera::fov(float FOV) {
	m_fov = glm::radians(FOV);
}

void dankengine::Camera::zBoundaries(float zNear, float zFar) {
	m_zNear = zNear;
	m_zFar = zFar;
}

void dankengine::Camera::orthographic(float Width, float Height) {
	viewportDimensions(Width, Height);
	fov(0);
	zBoundaries(0, 0);
	orthographic();
}

void dankengine::Camera::orthographic() {
	float ratio = m_width / m_height;
	m_matrix = glm::ortho(-ratio / 2.0f, ratio / 2.0f, -1 / 2.0f, 1 / 2.0f);
}

void dankengine::Camera::perspective(float Width, float Height, float FOV, float zNear, float zFar) {
	viewportDimensions(Width, Height);
	fov(FOV);
	zBoundaries(zNear, zFar);
	perspective();
}

void dankengine::Camera::perspective() {
	m_matrix = glm::perspective(m_fov, m_width / m_height, m_zNear, m_zFar);
}

glm::mat4& dankengine::Camera::getMatrix() {
	return m_matrix;
}
