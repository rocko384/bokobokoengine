#include "material.h"

uint32_t dankengine::Material::g_createdMaterials = 0;

dankengine::Material::Material() {
	diffuseMap = std::make_shared<Texture>();
	specularMap = std::make_shared<Texture>();
	ambientMap = std::make_shared<Texture>();
	bumpMap = std::make_shared<Texture>();

	m_id = ++g_createdMaterials;
}

dankengine::Material::Material(const Handle<Texture>& Texture) {
	diffuseMap = Texture;
}

dankengine::Material::~Material() {
}

uint32_t dankengine::Material::ID() {
	return m_id;
}

void dankengine::Material::bind() {
	diffuseMap->bind(Texture::TEXTURE_2D, Texture::ActiveTexture(0));
	specularMap->bind(Texture::TEXTURE_2D, Texture::ActiveTexture(1));
	ambientMap->bind(Texture::TEXTURE_2D, Texture::ActiveTexture(2));
	bumpMap->bind(Texture::TEXTURE_2D, Texture::ActiveTexture(3));

}

void dankengine::Material::unbind() {
	diffuseMap->unbind();
	specularMap->unbind();
	ambientMap->unbind();
	bumpMap->unbind();
}
