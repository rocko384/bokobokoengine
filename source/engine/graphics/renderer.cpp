#include "engine/debug.h"
#include "renderer.h"

#include <glm/glm.hpp>
#include <glad/glad.h>

using namespace dankengine;

Handle<ShaderProgram> createRenderStage(const std::string& vertexShaderPath, const std::string& fragmentShaderPath);

dankengine::Renderer::Renderer() {
}

dankengine::Renderer::Renderer(size_t Width, size_t Height, const std::string& Title, int MSAA){
	window.open(Width, Height, Title, MSAA);
	loadDefaultShaderProfiles();
}

dankengine::Renderer::~Renderer() {
	if (m_lightingStageFramebuffers.size() > 0) {
		glDeleteFramebuffers(m_lightingStageFramebuffers.size(), m_lightingStageFramebuffers.data());
	}

	if (m_lightingStageDepthtargets.size() > 0) {
		glDeleteTextures(m_lightingStageDepthtargets.size(), m_lightingStageDepthtargets.data());
	}

	if (m_lightingStageColortargets.size() > 0) {
		glDeleteTextures(m_lightingStageColortargets.size(), m_lightingStageColortargets.data());
	}

	glDeleteFramebuffers(1, &m_lightingCompositeFramebufferA);
	glDeleteFramebuffers(1, &m_lightingCompositeFramebufferB);
	glDeleteTextures(1, &m_lightingCompositeColorTargetA);
	glDeleteTextures(1, &m_lightingCompositeColorTargetB);
	glDeleteTextures(1, &m_lightingCompositeDepthTargetA);
	glDeleteTextures(1, &m_lightingCompositeDepthTargetB);

	glDeleteFramebuffers(1, &m_baseColorFramebuffer);
	glDeleteTextures(1, &m_baseColorDepthTarget);
	glDeleteTextures(1, &m_baseColorColorTarget);
}

void dankengine::Renderer::clearBatchRenderables() {
	for (auto& buffer : m_renderableBatches) {
		buffer.second.clear();
	}
}

void dankengine::Renderer::clearAllRenderables() {
	clearBatchRenderables();
}

void dankengine::Renderer::clearAllLights() {
	m_pointLights.clear();
	m_directionalLights.clear();
	m_spotLights.clear();
}

void dankengine::Renderer::loadDefaultShaderProfiles() {
	ShaderProfile spriteDefault;
	spriteDefault.compositeStage = createRenderStage(RESOURCE_DIR_STR + "shaders/vert_sprite_basic.glsl", RESOURCE_DIR_STR + "shaders/frag_sprite_basic.glsl");
	addShaderProfile(names::defaultSpriteProfileName, spriteDefault);

	ShaderProfile meshTexturedDefault;
	meshTexturedDefault.compositeStage = createRenderStage(RESOURCE_DIR_STR + "shaders/vert_mesh_textured_basic.glsl", RESOURCE_DIR_STR + "shaders/frag_mesh_textured_basic.glsl");
	addShaderProfile(names::defaultMeshTexturedProfileName, meshTexturedDefault);

	ShaderProfile meshDefault;
	meshDefault.compositeStage = createRenderStage(RESOURCE_DIR_STR + "shaders/vert_mesh_basic.glsl", RESOURCE_DIR_STR + "shaders/frag_mesh_basic.glsl");
	addShaderProfile(names::defaultMeshProfileName, meshDefault);

	ShaderProfile meshPhongDefault;
	meshPhongDefault.compositeStage = createRenderStage(RESOURCE_DIR_STR + "shaders/vert_mesh_textured_basic.glsl", RESOURCE_DIR_STR + "shaders/frag_mesh_textured_basic.glsl");
	meshPhongDefault.pointLightStage = createRenderStage(RESOURCE_DIR_STR + "shaders/vert_mesh_phong.glsl", RESOURCE_DIR_STR + "shaders/frag_mesh_phong_point.glsl");
	meshPhongDefault.directionalLightStage = createRenderStage(RESOURCE_DIR_STR + "shaders/vert_mesh_phong.glsl", RESOURCE_DIR_STR + "shaders/frag_mesh_phong_directional.glsl");
	meshPhongDefault.spotLightStage = createRenderStage(RESOURCE_DIR_STR + "shaders/vert_mesh_phong.glsl", RESOURCE_DIR_STR + "shaders/frag_mesh_phong_spot.glsl");
	addShaderProfile(names::defaultMeshPhongProfileName, meshPhongDefault);

	ShaderProfile lightingComposite;
	lightingComposite.compositeStage = createRenderStage(RESOURCE_DIR_STR + "shaders/vert_passthrough_plane.glsl", RESOURCE_DIR_STR + "shaders/frag_light_composite.glsl");
	addShaderProfile("LightingComposite", lightingComposite);

	ShaderProfile lightingApplication;
	lightingApplication.compositeStage = createRenderStage(RESOURCE_DIR_STR + "shaders/vert_passthrough_plane.glsl", RESOURCE_DIR_STR + "shaders/frag_light_application.glsl");
	addShaderProfile("LightingApplication", lightingApplication);

	ShaderProfile blitColorDepth;
	blitColorDepth.compositeStage = createRenderStage(RESOURCE_DIR_STR + "shaders/vert_passthrough_plane.glsl", RESOURCE_DIR_STR + "shaders/frag_passthrough_plane.glsl");
	addShaderProfile("Passthrough", blitColorDepth);

	ShaderProfile geometryPassthrough;
	geometryPassthrough.compositeStage = createRenderStage(RESOURCE_DIR_STR + "shaders/vert_mesh_basic.glsl", RESOURCE_DIR_STR + "shaders/frag_geometry_passthrough.glsl");
	addShaderProfile("GeometryPassthrough", geometryPassthrough);

	_initFramebufferStage(m_baseColorFramebuffer, m_baseColorDepthTarget, m_baseColorColorTarget);
	_initFramebufferStage(m_lightingCompositeFramebufferA, m_lightingCompositeDepthTargetA, m_lightingCompositeColorTargetA);
	_initFramebufferStage(m_lightingCompositeFramebufferB, m_lightingCompositeDepthTargetB, m_lightingCompositeColorTargetB);
}

void dankengine::Renderer::addShaderProfile(const std::string& Name, ShaderProfile Profile) {
	m_shaderProfiles[Name] = Profile;
}

void dankengine::Renderer::removeShaderProfile(const std::string& Name) {
	m_shaderProfiles.erase(Name);
}

void dankengine::Renderer::render() {
	window.clear();

	for (auto fb : m_lightingStageFramebuffers) {
		glBindFramebuffer(GL_FRAMEBUFFER, fb);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, m_lightingCompositeFramebufferA);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebuffer(GL_FRAMEBUFFER, m_lightingCompositeFramebufferB);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebuffer(GL_FRAMEBUFFER, m_baseColorFramebuffer);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Base color pass
	for (auto& renderables : m_renderableBatches) {
		auto& list = renderables.second;

		if (list.size() > 0) {

			auto& profile = m_shaderProfiles[list[0].r->shaderProfile];

			_renderDrawBuffer(m_baseColorFramebuffer, list, profile.compositeStage);
		}
	}

	// Lighting passes
	for (auto& renderables : m_renderableBatches) {
		auto& list = renderables.second;

		if (list.size() > 0) {
			auto& profile = m_shaderProfiles[list[0].r->shaderProfile];
			
			int lightIdx = 0;

			for (auto& light : m_pointLights) {
				GLuint framebuffer = m_lightingStageFramebuffers[lightIdx];

				if (profile.pointLightStage) {
					_renderDrawBufferLit(framebuffer, list, light, profile.pointLightStage);
				}
				else {
					_renderDrawBuffer(framebuffer, list, m_shaderProfiles["GeometryPassthrough"].compositeStage);
				}

				lightIdx++;
			}

			for (auto& light : m_directionalLights) {
				GLuint framebuffer = m_lightingStageFramebuffers[lightIdx];

				if (profile.directionalLightStage) {
					_renderDrawBufferLit(framebuffer, list, light, profile.directionalLightStage);
				}
				else {
					_renderDrawBuffer(framebuffer, list, m_shaderProfiles["GeometryPassthrough"].compositeStage);
				}

				lightIdx++;
			}

			for (auto& light : m_spotLights) {
				GLuint framebuffer = m_lightingStageFramebuffers[lightIdx];

				if (profile.spotLightStage) {
					_renderDrawBufferLit(framebuffer, list, light, profile.spotLightStage);
				}
				else {
					_renderDrawBuffer(framebuffer, list, m_shaderProfiles["GeometryPassthrough"].compositeStage);
				}

				lightIdx++;
			}
		}
	}


	// Lighting pass composition
	auto& compositingProfile = m_shaderProfiles["LightingComposite"].compositeStage;
	auto& blittingProfile = m_shaderProfiles["Passthrough"].compositeStage;

	for (int i = 0; i < m_lightingStageFramebuffers.size(); i++) {
		compositingProfile->bind();
		glBindFramebuffer(GL_FRAMEBUFFER, m_lightingCompositeFramebufferA);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_lightingStageColortargets[i]);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, m_lightingStageDepthtargets[i]);

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, m_lightingCompositeColorTargetB);
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, m_lightingCompositeDepthTargetB);

		glBindVertexArray(0);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		blittingProfile->bind();

		glBindFramebuffer(GL_FRAMEBUFFER, m_lightingCompositeFramebufferB);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_lightingCompositeColorTargetA);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, m_lightingCompositeDepthTargetA);

		glDrawArrays(GL_TRIANGLES, 0, 6);

		glBindFramebuffer(GL_FRAMEBUFFER, m_lightingCompositeFramebufferA);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	// Lighting application
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	m_shaderProfiles["LightingApplication"].compositeStage->bind();

	m_shaderProfiles["LightingApplication"].compositeStage->setUniform3("ambient", glm::vec3(0.2f, 0.2f, 0.2f));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_baseColorColorTarget);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_baseColorDepthTarget);
	
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_lightingCompositeColorTargetB);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_lightingCompositeDepthTargetB);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void dankengine::Renderer::render(Scene& s) {
	clearAllRenderables();
	clearAllLights();

	for (auto n : s.root.children()) {
		_accumulateSceneChildren(n, glm::vec3(0, 0, 0), glm::vec3(1, 1, 1), glm::quat(1, 0, 0, 0));
	}

	size_t totalLights = m_pointLights.size() + m_directionalLights.size() + m_spotLights.size();

	if (totalLights > m_lightingStageFramebuffers.size()) {
		size_t sizeDiff = totalLights - m_lightingStageFramebuffers.size();

		std::vector<GLuint> framebuffers(sizeDiff);

		glGenFramebuffers(framebuffers.size(), framebuffers.data());

		for (auto fb : framebuffers) {
			m_lightingStageFramebuffers.push_back(fb);
		}


		std::vector<GLuint> depthtargets(sizeDiff);

		glGenTextures(depthtargets.size(), depthtargets.data());

		for (auto dt : depthtargets) {
			glBindTexture(GL_TEXTURE_2D, dt);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, window.width(), window.height(), 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

			m_lightingStageDepthtargets.push_back(dt);
		}


		std::vector<GLuint> colortargets(sizeDiff);

		glGenTextures(colortargets.size(), colortargets.data());

		for (auto ct : colortargets) {
			glBindTexture(GL_TEXTURE_2D, ct);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, window.width(), window.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

			m_lightingStageColortargets.push_back(ct);
		}

		for (int i = 0; i < m_lightingStageFramebuffers.size(); i++) {
			glBindFramebuffer(GL_FRAMEBUFFER, m_lightingStageFramebuffers[i]);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_lightingStageDepthtargets[i], 0);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_lightingStageColortargets[i], 0);
		}
	}

	render();
}

void dankengine::Renderer::flip() {
	window.display();
}

void dankengine::Renderer::frame() {
	render();
	flip();
}

void dankengine::Renderer::_accumulateSceneChildren(Handle<SceneNode>& node, glm::vec3 position, glm::vec3 scale, glm::quat rotation) {
	glm::vec3 newPosition = node->position + position;
	glm::vec3 newScale = node->scale * scale;
	glm::quat newRotation = node->rotation * rotation;

	Handle<Renderable>* p_r = std::get_if<Handle<Renderable>>(&(node->item));
	Handle<Camera>* p_c = std::get_if<Handle<Camera>>(&(node->item));
	Handle<Light>* p_l = std::get_if<Handle<Light>>(&(node->item));
	
	if (p_r != nullptr) {
		addBatchRenderable(*p_r, { newPosition, newScale, newRotation });
	}
	
	if (p_c != nullptr && (*p_c)->active) {
		m_cameraMatrix = (*p_c)->getMatrix();
		m_cameraTransform = glm::translate(glm::mat4(1), newPosition) * newRotation.operator glm::mat<4, 4, float, glm::packed_highp>() * glm::scale(glm::mat4(1), newScale);
		m_cameraTransform = glm::inverse(m_cameraTransform);
		m_cameraPosition = newPosition;
	}

	if (p_l != nullptr) {
		switch ((*p_l)->type) {
		case Light::Type::POINT:
			m_pointLights.push_back({ *p_l, { newPosition, newScale, newRotation} });
			break;
		case Light::Type::DIRECTIONAL:
			m_directionalLights.push_back({ *p_l, { newPosition, newScale, newRotation} });
			break;
		case Light::Type::SPOT:
			m_spotLights.push_back({ *p_l, { newPosition, newScale, newRotation} });
			break;
		}
	}

	for (auto n : node->children()) {
		_accumulateSceneChildren(n, newPosition, newScale, newRotation);
	}
}

void dankengine::Renderer::_initFramebufferStage(GLuint& framebuffer, GLuint& depthTarget, GLuint& colorTarget) {
	glGenTextures(1, &colorTarget);

	glBindTexture(GL_TEXTURE_2D, colorTarget);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, window.width(), window.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glGenTextures(1, &depthTarget);

	glBindTexture(GL_TEXTURE_2D, depthTarget);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, window.width(), window.height(), 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glGenFramebuffers(1, &framebuffer);

	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTarget, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorTarget, 0);
}

void dankengine::Renderer::_renderItem(_transformedRenderable& r, Handle<ShaderProgram> program) {
	program->bind();

	r.r->material->diffuseMap->bind(Texture::TEXTURE_2D, Texture::ActiveTexture(0));
	r.r->vertexArrayObject->bind();
	r.r->setUniforms(*program);

	program->setUniformMatrix4("view", false, m_cameraTransform);
	program->setUniformMatrix4("projection", false, m_cameraMatrix);

	auto model = glm::translate(glm::mat4(1), r.t.position) * r.t.rotation.operator glm::mat<4, 4, float, glm::packed_highp>() * glm::scale(glm::mat4(1), r.t.scale);
	auto normal = glm::transpose(glm::inverse((glm::mat3)model));

	program->setUniformMatrix4("model", false, model);

	r.r->update();
	r.r->callback(*r.r);

	size_t c = r.r->vertexCount();

	r.r->indexBuffer->bind(GPUBuffer::ELEMENT_ARRAY);
	glDrawElements(GL_TRIANGLES, c, GL_UNSIGNED_INT, 0);
}

void dankengine::Renderer::_renderItemLit(_transformedRenderable& r, _transformedLight& l, Handle<ShaderProgram> program) {
	program->bind();

	r.r->material->diffuseMap->bind(Texture::TEXTURE_2D, Texture::ActiveTexture(0));
	r.r->vertexArrayObject->bind();
	r.r->setUniforms(*program);

	program->setUniformMatrix4("view", false, m_cameraTransform);
	program->setUniformMatrix4("projection", false, m_cameraMatrix);

	auto model = glm::translate(glm::mat4(1), r.t.position) * r.t.rotation.operator glm::mat<4, 4, float, glm::packed_highp>() * glm::scale(glm::mat4(1), r.t.scale);
	auto normal = glm::transpose(glm::inverse((glm::mat3)model));

	program->setUniformMatrix4("model", false, model);
	program->setUniformMatrix3("normal", false, normal);

	program->setUniform3("viewCoord", m_cameraPosition);


	program->setUniform3("light.diffuse", l.l->diffuse);
	program->setUniform3("light.specular", l.l->specular);
	program->setUniform3("light.position", l.t.position);

	glm::vec3 newDir;
	if (l.l->type == Light::Type::SPOT) {
		newDir = (glm::vec3)(l.t.rotation.operator glm::mat<4, 4, float, glm::packed_highp>() * glm::vec4(l.l->direction, 1.0f));
	}
	else {
		newDir = l.l->direction;
	}

	program->setUniform3("light.direction", newDir);
	program->setUniform1("light.intensity", l.l->intensity);
	program->setUniform1("light.aperture", l.l->aperture);

	r.r->update();
	r.r->callback(*r.r);

	size_t c = r.r->vertexCount();

	r.r->indexBuffer->bind(GPUBuffer::ELEMENT_ARRAY);
	glDrawElements(GL_TRIANGLES, c, GL_UNSIGNED_INT, 0);
}

void dankengine::Renderer::_renderDrawBuffer(GLuint framebuffer, _drawBuffer& buffer, Handle<ShaderProgram> program) {
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

	GLenum activeBuffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, activeBuffers);

	glViewport(0, 0, window.width(), window.height());

	for (auto& r : buffer) {
		_renderItem(r, program);
	}
}

void dankengine::Renderer::_renderDrawBufferLit(GLuint framebuffer, _drawBuffer& buffer, _transformedLight& l, Handle<ShaderProgram> program) {
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

	GLenum activeBuffers[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, activeBuffers);

	glViewport(0, 0, window.width(), window.height());

	for (auto& r : buffer) {
		_renderItemLit(r, l, program);
	}
}

Handle<ShaderProgram> createRenderStage(const std::string& vertexShaderPath, const std::string& fragmentShaderPath) {
	Resource vert_source, frag_source;
	vert_source.loadFromFile(vertexShaderPath);
	frag_source.loadFromFile(fragmentShaderPath);

	Shader s_vert, s_frag;

	s_vert.create(Shader::VERTEX_SHADER);
	s_vert.addToSource(vert_source);

	s_frag.create(Shader::FRAGMENT_SHADER);
	s_frag.addToSource(frag_source);

	auto error_vert = s_vert.compile(true);
	auto error_frag = s_frag.compile(true);

	if ((error_vert != "success") || (error_frag != "success")) {
		std::cout << "vv Errors vv\n";
		_DEBUG_PRINT("Vertex Shader (%s):\n", vertexShaderPath.c_str());
		std::cout << error_vert << "\n";
		_DEBUG_PRINT("Fragment Shader (%s):\n", fragmentShaderPath.c_str());
		std::cout << error_frag << "\n";
		std::cout << "^^ Errors ^^\n";
	
		return nullptr;
	}

	Handle<ShaderProgram> ret = std::make_shared<ShaderProgram>();

	ret->attachShader(s_vert);
	ret->attachShader(s_frag);
	ret->bindFragDataLocation(0, "outColor");
	ret->link();

	return ret;
}
