#pragma once

#include "engine/dank.h"
#include "engine/handle.h"
#include "engine/graphics/renderable.h"
#include "engine/graphics/window.h"

#include "engine/graphics/scene/scene.h"
#include "engine/graphics/shaderprofile.h"

#include <vector>
#include <map>
#include <type_traits>
#include <cinttypes>

namespace dankengine {

	class DankExport Renderer {

	public:

		struct transformData {
			glm::vec3 position;
			glm::vec3 scale;
			glm::quat rotation;

			bool operator==(const transformData& rhs) {
				return position == rhs.position && scale == rhs.scale && rotation == rhs.rotation;
			}
		};


	public:

		Renderer();
		Renderer(size_t Width, size_t Height, const std::string& Title, int MSAA = 0);
		~Renderer();

		template<typename T>
		void addBatchRenderable(Handle<T>& Renderable, transformData t);

		template<typename T>
		void removeBatchRenderable(Handle<T>& Renderable);

		void clearBatchRenderables();
		void clearAllRenderables();
		void clearAllLights();

		void loadDefaultShaderProfiles();
		void addShaderProfile(const std::string& Name, ShaderProfile Profile);
		void removeShaderProfile(const std::string& Name);

		void render();	// Just render geometry
		void render(Scene& s);
		void flip();	// Just buffer flip
		void frame();	// Render & buffer flip

		Window window;

	private:

		glm::mat4 m_cameraMatrix;
		glm::mat4 m_cameraTransform;
		glm::vec3 m_cameraPosition;

		std::map<const std::string, ShaderProfile> m_shaderProfiles;

		void _accumulateSceneChildren(Handle<SceneNode>& node, glm::vec3 position, glm::vec3 scale, glm::quat rotation);

		typedef struct _transformedRenderable_t {
			Handle<Renderable> r;
			transformData t;

			bool operator==(const _transformedRenderable_t& rhs) {
				return r == rhs.r && t == rhs.t;
			}
		} _transformedRenderable;
		typedef std::vector<_transformedRenderable> _drawBuffer;

		std::map<const std::string, _drawBuffer> m_renderableBatches;

		typedef struct _transformedLight_t {
			Handle<Light> l;
			transformData t;

			bool operator==(const _transformedLight_t& rhs) {
				return l == rhs.l && t == rhs.t;
			}
		} _transformedLight;

		std::vector<_transformedLight> m_pointLights;
		std::vector<_transformedLight> m_directionalLights;
		std::vector<_transformedLight> m_spotLights;


		std::vector<GLuint> m_lightingStageFramebuffers;
		std::vector<GLuint> m_lightingStageDepthtargets;
		std::vector<GLuint> m_lightingStageColortargets;

		void _initFramebufferStage(GLuint& framebuffer, GLuint& depthTarget, GLuint& colorTarget);

		GLuint m_baseColorFramebuffer;
		GLuint m_baseColorDepthTarget;
		GLuint m_baseColorColorTarget;

		GLuint m_lightingCompositeFramebufferA;
		GLuint m_lightingCompositeDepthTargetA;
		GLuint m_lightingCompositeColorTargetA;

		GLuint m_lightingCompositeFramebufferB;
		GLuint m_lightingCompositeDepthTargetB;
		GLuint m_lightingCompositeColorTargetB;

		void _renderItem(_transformedRenderable& r, Handle<ShaderProgram> program);
		void _renderItemLit(_transformedRenderable& r, _transformedLight& l, Handle<ShaderProgram> program);
		void _renderDrawBuffer(GLuint framebuffer, _drawBuffer& buffer, Handle<ShaderProgram> program);
		void _renderDrawBufferLit(GLuint framebuffer, _drawBuffer& buffer, _transformedLight& l, Handle<ShaderProgram> program);
	};

	template<typename T>
	inline void Renderer::addBatchRenderable(Handle<T>& renderable, transformData t) {
		static_assert(std::is_base_of<Renderable, T>(), "Type is not Renderable");

		Handle<Renderable> handle = std::static_pointer_cast<Renderable>(renderable);

		_drawBuffer& v = m_renderableBatches[handle->shaderProfile];

		auto r = std::find(std::begin(v), std::end(v), _transformedRenderable{ handle, t });

		if (r == std::end(v)) {
			v.push_back({ handle, t });
		}
		
	}

	template<typename T>
	inline void Renderer::removeBatchRenderable(Handle<T>& renderable) {
		static_assert(std::is_base_of<Renderable, T>(), "Type is not Renderable");

		Handle<Renderable> handle = std::static_pointer_cast<Renderable>(renderable);

		auto& v = m_renderableBatches[handle->shaderProfile];

		auto r = std::find(std::begin(v), std::end(v), handle);

		if (r != std::end(v)) {
			v.erase(r);
		}

	}
}