#pragma once

#include "engine/dank.h"
#include "engine/handle.h"
#include "engine/graphics/gl/shaderprogram.h"

namespace dankengine {

	struct DankExport ShaderProfile {

		ShaderProfile() {
			compositeStage = std::make_shared<ShaderProgram>();
		}

		bool hasLighting() {
			return pointLightStage || directionalLightStage || spotLightStage;
		}

		Handle<ShaderProgram> compositeStage;
		Handle<ShaderProgram> pointLightStage;
		Handle<ShaderProgram> directionalLightStage;
		Handle<ShaderProgram> spotLightStage;

	};

}