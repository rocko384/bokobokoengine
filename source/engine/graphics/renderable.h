#pragma once

#include "engine/dank.h"
#include "engine/defaultnames.h"

#include "gl/gpubuffer.h"
#include "gl/vertexarrayobject.h"
#include "material.h"

#include <functional>

namespace dankengine {

	class DankExport Renderable {

	public:

		Renderable();
		~Renderable();

		Handle<VertexArrayObject> vertexArrayObject;
		Handle<GPUBuffer> vertexBuffer;
		Handle<GPUBuffer> indexBuffer;
		Handle<Material> material;
		
		void setDefaultVAOLayout2D();
		void setDefaultVAOLayout3D();

		virtual void setUniforms(ShaderProgram& program);
		size_t vertexCount();

		virtual void update();
		std::function<void(Renderable&)> callback;

		std::string shaderProfile;

	protected:

		size_t m_vertexCount;

	};

}