#pragma once

#include "engine/dank.h"

#include "engine/graphics/renderable.h"
#include "spriteanimation.h"

#include <glm/vec3.hpp>

namespace dankengine {

	class DankExport Sprite : public Renderable {

	public:

		Sprite();
		~Sprite();

		void setTile(int U, int V);
		void setTile(const glm::ivec2& Coords);
		
		void setTileSize(float W, float H);
		void setTileSize(const glm::vec2& Size);

		void setSheetSize(float W, float H);
		void setSheetSize(const glm::vec2& Size);

		void setTileSheet(const std::string& Path);
		void setTileSheet(const filesystem::path& Path);

		void addAnimation(uint32_t ID, SpriteAnimation Animation);
		void removeAnimation(uint32_t ID);
		void playAnimation(uint32_t ID);
		void playAnimation();
		void stepAnimation(uint32_t ID);
		void pauseAnimation();

		void setUniforms(ShaderProgram& program);
		void update();

	private:

		glm::ivec2 m_tile;
		glm::vec2 m_tileSize;
		glm::vec2 m_sheetSize;

		uint32_t m_currentAnimation;
		bool m_isPlaying;
		std::map<const uint32_t, SpriteAnimation> m_animations;

	};


}