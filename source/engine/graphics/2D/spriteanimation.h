#pragma once

#include "engine/dank.h"

#include <vector>
#include <chrono>
#include <glm/vec2.hpp>

namespace dankengine {

	struct DankExport SpriteAnimationFrame {

		glm::ivec2 tile;
		glm::vec2 tileSize;

	};

	class DankExport SpriteAnimation {

	public:

		SpriteAnimation();

		void pushFrame(SpriteAnimationFrame frame);
		void pushFrame(glm::ivec2 Tile, glm::vec2 tileSize);
		void stepFrame(bool inTime = true);
		const SpriteAnimationFrame& currentFrame();

		float fps;

	private:

		std::chrono::system_clock::time_point m_lastStep;
		uint32_t m_currentFrameIndex;
		std::vector<SpriteAnimationFrame> m_frames;

	};

}