#include "spriteanimation.h"

dankengine::SpriteAnimation::SpriteAnimation() {
	m_currentFrameIndex = 0;
	m_lastStep = std::chrono::system_clock::now();
}

void dankengine::SpriteAnimation::pushFrame(SpriteAnimationFrame frame) {
	m_frames.push_back(frame);
}

void dankengine::SpriteAnimation::pushFrame(glm::ivec2 Tile, glm::vec2 tileSize) {
	m_frames.push_back({ Tile, tileSize });
}

void dankengine::SpriteAnimation::stepFrame(bool inTime) {
	bool step = !inTime;
	
	if (inTime) {
		auto diff = std::chrono::system_clock::now() - m_lastStep;
		auto diff_ms = std::chrono::duration_cast<std::chrono::milliseconds>(diff);

		float frameTime_ms = 1000.0f / fps;

		if (diff_ms.count() > frameTime_ms) {
			step = true;
		}
	}

	if (step) {
		m_currentFrameIndex++;

		if (m_currentFrameIndex == m_frames.size()) {
			m_currentFrameIndex = 0;
		}
	}
}

const dankengine::SpriteAnimationFrame& dankengine::SpriteAnimation::currentFrame() {
	return m_frames[m_currentFrameIndex];
}
