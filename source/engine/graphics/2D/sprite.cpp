#include "sprite.h"
#include "engine/debug.h"

dankengine::Sprite::Sprite() {
	m_isPlaying = false;
	
	setDefaultVAOLayout2D();

	shaderProfile = names::defaultSpriteProfileName;

	auto v = {
		// Position		Normal				Color				Texture
		0.5f,  0.5f,	0.0f, 0.0f, 1.0f,	1.0f, 1.0f, 1.0f,	1.0f, 0.0f,		// Top-right
		0.5f, -0.5f,	0.0f, 0.0f, 1.0f,	1.0f, 1.0f, 1.0f,	1.0f, 1.0f,		// Bottom-right
		-0.5f, 0.5f,	0.0f, 0.0f, 1.0f,	1.0f, 1.0f, 1.0f,	0.0f, 0.0f,		// Top-left
		-0.5f, -0.5f,	0.0f, 0.0f, 1.0f,	1.0f, 1.0f, 1.0f,	0.0f, 1.0f		// Bottom-left
	};

	auto vi = {
		2, 3, 0,
		0, 3, 1
	};

	m_vertexCount = 6;

	indexBuffer->setData(vi, GPUBuffer::STATIC_DRAW, GPUBuffer::ELEMENT_ARRAY);
	vertexBuffer->setData(v);
}

dankengine::Sprite::~Sprite() {
}

void dankengine::Sprite::setTile(int U, int V) {
	setTile(glm::ivec2{ U, V });
}

void dankengine::Sprite::setTile(const glm::ivec2& Coords) {
	m_tile = Coords;
}

void dankengine::Sprite::setTileSize(float W, float H) {
	setTileSize(glm::vec2{ W, H });
}

void dankengine::Sprite::setTileSize(const glm::vec2& Size) {
	m_tileSize = Size;
}

void dankengine::Sprite::setSheetSize(float W, float H) {
	setSheetSize(glm::vec2{ W, H });
}

void dankengine::Sprite::setSheetSize(const glm::vec2& Size) {
	m_sheetSize = Size;
}

void dankengine::Sprite::setTileSheet(const std::string& Path) {
	material->diffuseMap->loadFromFile2D(Path);

	material->diffuseMap->setParameter(Texture::TEXTURE_WRAP_S, Texture::REPEAT, Texture::TEXTURE_2D);
	material->diffuseMap->setParameter(Texture::TEXTURE_WRAP_T, Texture::REPEAT, Texture::TEXTURE_2D);

	material->diffuseMap->setParameter(Texture::TEXTURE_BORDER_COLOR, { 1.0f, 0.0f, 0.0f, 1.0f });
	material->diffuseMap->setParameter(Texture::TEXTURE_MIN_FILTER, Texture::LINEAR);
	material->diffuseMap->setParameter(Texture::TEXTURE_MIN_FILTER, Texture::LINEAR);
}

void dankengine::Sprite::setTileSheet(const filesystem::path& Path) {
	setTileSheet(Path.string());
}

void dankengine::Sprite::addAnimation(uint32_t ID, SpriteAnimation Animation) {
	m_animations[ID] = Animation;
}

void dankengine::Sprite::removeAnimation(uint32_t ID) {
	m_animations.erase(ID);
}

void dankengine::Sprite::playAnimation(uint32_t ID) {
	m_currentAnimation = ID;
	m_isPlaying = true;
}

void dankengine::Sprite::playAnimation() {
	m_isPlaying = true;
}

void dankengine::Sprite::stepAnimation(uint32_t ID) {
	m_animations[ID].stepFrame(false);
}

void dankengine::Sprite::pauseAnimation() {
	m_isPlaying = false;
}

void dankengine::Sprite::setUniforms(ShaderProgram& program) {
	program.setUniform2("tile", m_tile);
	program.setUniform2("tileSize", m_tileSize);
	program.setUniform2("sheetSize", m_sheetSize);
}

void dankengine::Sprite::update() {
	if (m_isPlaying) {
		m_animations[m_currentAnimation].stepFrame();

		auto frame = m_animations[m_currentAnimation].currentFrame();

		setTile(frame.tile);
		setTileSize(frame.tileSize);
	}
}
