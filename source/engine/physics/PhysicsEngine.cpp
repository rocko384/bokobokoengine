#include "PhysicsEngine.h"

using namespace dankengine;

physics::PhysicsEngine::PhysicsEngine() {

}

// physics::PhysicsEngine::~PhysicsEngine() {
//
// }

void physics::PhysicsEngine::addPhysicsObject(Handle<SceneNode> scnode) {
    physicsObjects.push_back(std::make_shared<PhysicsObject>(scnode));
}

void physics::PhysicsEngine::addPhysicsObject(Handle<SceneNode> scnode, glm::vec3 v_initial, glm::vec3 a_initial) {
    physicsObjects.push_back(std::make_shared<PhysicsObject>(scnode, v_initial, a_initial));
}

void physics::PhysicsEngine::removePhysicsObject(Handle<SceneNode> scnode) {

}

void physics::PhysicsEngine::tick(std::chrono::duration<float> elapsed_time) {
    // make this not garbage later
    for (int i = 0; i < physicsObjects.size(); i++) {
        physicsObjects[i]->tick(elapsed_time);
    }
}
