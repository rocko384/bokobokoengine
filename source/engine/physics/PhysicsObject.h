#pragma once

#include "engine/dank.h"

#include "engine/handle.h"
#include "engine/graphics/scene/scenenode.h"

#include <time.h>

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

using namespace dankengine;

namespace physics {

	const glm::vec3 GRAVITY = glm::vec3(0.0, -9.80665, 0.0);

	class DankExport PhysicsObject {

	public:
		PhysicsObject(Handle<SceneNode> scnode);
		PhysicsObject(Handle<SceneNode> scnode, glm::vec3 v_initial, glm::vec3 a_initial);

		// from scnode
		glm::vec3 getPosition();
		glm::quat getRotation();

		// physics
		glm::vec3 getVelocity();
		glm::vec3 getAcceleration();
		int getMass();

		void tick(std::chrono::duration<float> elapsed_time);

	protected:

		Handle<SceneNode> scnode;
		int mass;
		glm::vec3 velocity;
		glm::vec3 acceleration;

	};
};
