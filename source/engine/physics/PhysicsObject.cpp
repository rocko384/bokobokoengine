#include "PhysicsObject.h"
#include <time.h>

using namespace dankengine;
using namespace glm;

physics::PhysicsObject::PhysicsObject(Handle<SceneNode> scnode) {
    this->scnode = scnode;
    mass = 1;
    velocity = vec3(0, 0, 0);
    acceleration = vec3(0, 0, 0);
}

physics::PhysicsObject::PhysicsObject(Handle<SceneNode> scnode, vec3 v_initial, vec3 a_initial) {
    this->scnode = scnode;
    mass = 1;
    velocity = v_initial;
    acceleration = a_initial;
}

// physics::PhysicsObject::~PhysicsObject() {
//     this->scnode = scnode;
// }

// from scnode
vec3 physics::PhysicsObject::getPosition() {
    return scnode->position;
}

quat physics::PhysicsObject::getRotation() {
    return scnode->rotation;
}

// physics
vec3 physics::PhysicsObject::getVelocity() {
    return this->velocity;
}

vec3 physics::PhysicsObject::getAcceleration() {
    return this->acceleration;
}

void physics::PhysicsObject::tick(std::chrono::duration<float> elapsed_time) {

//    position = position + velocity * elapsed_time;
//    velocity = velocity + acceleration * elapsed_time;
//    acceleration = GRAVITY;

    scnode->position += velocity * elapsed_time.count();
    this->velocity += acceleration * elapsed_time.count();



}
