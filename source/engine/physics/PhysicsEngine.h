#pragma once

#include "engine/dank.h"

#include "engine/handle.h"
#include "engine/graphics/scene/scenenode.h"

#include "engine/physics/PhysicsObject.h"

#include <time.h>

using namespace dankengine;

namespace physics {

	class DankExport PhysicsEngine {

	public:
		PhysicsEngine();

		void addPhysicsObject(Handle<SceneNode> scnode);
		void addPhysicsObject(Handle<SceneNode> scnode, glm::vec3 v_initial, glm::vec3 a_initial);
		void removePhysicsObject(Handle<SceneNode> scnode);
		void tick(std::chrono::duration<float> elapsed_time);

	private:
		std::vector<std::shared_ptr<PhysicsObject>> physicsObjects;

	};
};
