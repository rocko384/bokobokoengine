#include "inputswitchboard.h"

dankengine::InputSwitchboard::InputSwitchboard() {
}

dankengine::InputSwitchboard::~InputSwitchboard() {
}

void dankengine::InputSwitchboard::pollEventsFromWindow(Window& W) {

	auto windowEvent = W.pollEvent();

	while (windowEvent.has_value()) {

		switch (windowEvent->type) {
		case sf::Event::Closed: 
			W.close();
			break;

		case sf::Event::KeyPressed:
			m_keyMap[windowEvent->key.code] = true;
			m_altPressed = windowEvent->key.alt;
			m_controlPressed = windowEvent->key.control;
			m_shiftPressed = windowEvent->key.shift;
			m_systemPressed = windowEvent->key.system;
			break;

		case sf::Event::KeyReleased:
			m_keyMap[windowEvent->key.code] = false;
			m_altPressed = windowEvent->key.alt;
			m_controlPressed = windowEvent->key.control;
			m_shiftPressed = windowEvent->key.shift;
			m_systemPressed = windowEvent->key.system;
			break;
		}

		windowEvent = W.pollEvent();
	}
}

bool dankengine::InputSwitchboard::isAltPressed() {
	return m_altPressed;
}

bool dankengine::InputSwitchboard::isControlPressed() {
	return m_controlPressed;
}

bool dankengine::InputSwitchboard::isShiftPressed() {
	return m_shiftPressed;
}

bool dankengine::InputSwitchboard::isSystemPressed() {
	return m_systemPressed;
}

bool dankengine::InputSwitchboard::isKeyPressed(sf::Keyboard::Key key) {
	return m_keyMap[key];
}
