#pragma once

#include "engine/dank.h"

#include "engine/graphics/window.h"

#include <map>

namespace dankengine {

	class DankExport InputSwitchboard {

	public:

		InputSwitchboard();
		~InputSwitchboard();

		void pollEventsFromWindow(Window& W);

		bool isAltPressed();
		bool isControlPressed();
		bool isShiftPressed();
		bool isSystemPressed();
		bool isKeyPressed(sf::Keyboard::Key key);

	private:

		bool m_altPressed;
		bool m_controlPressed;
		bool m_shiftPressed;
		bool m_systemPressed;

		std::map<sf::Keyboard::Key, bool> m_keyMap;

	};

}