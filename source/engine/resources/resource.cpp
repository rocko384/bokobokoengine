#include "resource.h"

#include <cstring> // needed for memset

dankengine::Resource::Resource() {
	m_pData = nullptr;
	m_dataSize = 0;
}

dankengine::Resource::Resource(const std::string& filepath, FileType type) {
	m_pData = nullptr;
	m_dataSize = 0;

	loadFromFile(filepath, type);
}

dankengine::Resource::Resource(const filesystem::path& filepath, FileType type) {
	m_pData = nullptr;
	m_dataSize = 0;

	loadFromFile(filepath, type);
}

dankengine::Resource::~Resource() {
	delete[] m_pData;
}

int dankengine::Resource::loadFromFile(const std::string& filepath, FileType type) {
	return loadFromFile(filesystem::path(filepath), type);
}

int dankengine::Resource::loadFromFile(const filesystem::path& filepath, FileType type) {
	if (filesystem::exists(filepath) == false) {
		return FAILURE;
	}

	m_dataSize = filesystem::file_size(filepath);

	if (m_pData != nullptr) {
		delete[] m_pData;
	}

	m_pData = new int8_t[m_dataSize];
	std::memset(m_pData, 0, m_dataSize * sizeof(int8_t));

	std::ifstream f;

	if (type == BINARY) {
		f.open(filepath, std::ios::binary);
	}
	else {
		f.open(filepath);
	}

	if (f.fail() || !f.is_open()) {
		return FAILURE;
	}

	if (type == BINARY) {
		f.seekg(0, std::ios::beg);
		auto& file = f.read(reinterpret_cast<char*>(m_pData), m_dataSize);
	}
	else {
		int8_t c;

		int i = 0;

		while (!f.eof()) {
			f.get((char&)c);

			m_pData[i++] = c;
		}

		m_pData[i-1] = 0;

	}

	f.close();

	m_path = filepath;

	return SUCCESS;
}

size_t dankengine::Resource::getByteCount() {
	return m_dataSize;
}

size_t dankengine::Resource::getByteCount() const {
	return m_dataSize;
}

std::string dankengine::Resource::getDataAsString() {
	std::string ret = "";

	ret = std::string(reinterpret_cast<char*>(m_pData));

	return ret;
}

void* dankengine::Resource::getDataAsPtr() {
	return reinterpret_cast<void*>(m_pData);
}

const void* dankengine::Resource::getDataAsPtr() const {
	return reinterpret_cast<const void*>(m_pData);
}

filesystem::path dankengine::Resource::getPath() {
	return m_path;
}
