#pragma once

#include "engine/dank.h"

#include <cinttypes>
#include <string>
#include <fstream>
#include <filesystem>
#include <vector>
#include <algorithm>

namespace filesystem = std::filesystem;

#define RESOURCE_DIR_STR std::string("data/")
#define RESOURCE_DIR filesystem::path(RESOURCE_DIR_STR);

namespace dankengine {

	class DankExport Resource {

	public:

		enum Success {
			SUCCESS = 1,
			FAILURE = 0
		};

		enum FileType {
			BINARY,
			CHARACTER
		};

	public:

		Resource();
		Resource(const std::string& filepath, FileType type = CHARACTER);
		Resource(const filesystem::path& filepath, FileType type = CHARACTER);
		~Resource();

		int loadFromFile(const std::string& filepath, FileType type = CHARACTER);
		int loadFromFile(const filesystem::path& filepath, FileType type = CHARACTER);

		size_t getByteCount();
		size_t getByteCount() const;

		std::string getDataAsString();
		void* getDataAsPtr();
		const void* getDataAsPtr() const;

		filesystem::path getPath();

	private:

		int8_t* m_pData;
		size_t m_dataSize;
		filesystem::path m_path;

	};

}