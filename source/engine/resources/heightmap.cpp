
#include "heightmap.h"


dankengine::HeightMap::HeightMap(const std::string& path) {
	loadFromFile(path);
}

dankengine::HeightMap::HeightMap(const std::filesystem::path& p) {
	loadFromFile(p);
}

dankengine::HeightMap::HeightMap(const dankengine::Resource& r) {
	loadFromResource(r);
}


int dankengine::HeightMap::loadFromFile(const std::string& path) {
	if (image.loadFromFile(path)) {
		return 1;
	}
	
	// Try other formats if possible
	
	// Fail if there was no way to open the image
	return 0;
}

int dankengine::HeightMap::loadFromFile(const std::filesystem::path& path) {
	return loadFromFile(path.string());
}

int dankengine::HeightMap::loadFromResource(const dankengine::Resource& r) {
	if (image.loadFromMemory(r.getDataAsPtr(), r.getByteCount())) {
		return 1;
	};
	
	// try other formats
	
	// fail if nothing worked
	return 0;
}


unsigned int dankengine::HeightMap::getWidth() const {
	return image.getSize().x;
}

unsigned int dankengine::HeightMap::getHeight() const {
	return image.getSize().y;
}

// Explicit specialization to convert to float pixel
template<>
dankengine::Pixel<float, float> dankengine::HeightMap::getPixel(unsigned int x, unsigned int y) const {
	sf::Color clr = image.getPixel(x, y);
	Pixel<float, float> px;
	px.r = (float)clr.r / 256.f;
	px.g = (float)clr.g / 256.f;
	px.b = (float)clr.b / 256.f;
	px.a = (float)clr.a / 256.f;
	return px;
}
