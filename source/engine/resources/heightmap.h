#pragma once

#include "resource.h"
#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/Color.hpp>

#include <cstdint>
#include <type_traits>

namespace dankengine {
	
	// This template is sourcery, but just use it like Pixel<T>
	template<
		typename T, 
		typename = typename std::enable_if<
			std::is_arithmetic<T>::value, T
		>::type
	>
	class Pixel {
	public:
		T r = 0;
		T g = 0;
		T b = 0;
		T a = 0;
	};
	
	class DankExport HeightMap {
	public:
		HeightMap(const std::string& path);
		HeightMap(const std::filesystem::path& p);
		HeightMap(const Resource& r);
		
		int loadFromFile(const std::string& path);
		int loadFromFile(const std::filesystem::path& p);
		int loadFromResource(const Resource& r);
		
		unsigned int getWidth() const;
		unsigned int getHeight() const;
		
		// SFML guarantees 4 byte pixels, let's just go with that
		static constexpr size_t getPixelDepth() {return 4;}
		
		template<typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
		Pixel<T> getPixel(unsigned int x, unsigned int y) const {
			sf::Color px = image.getPixel(x, y);
			return {(T)px.r, (T)px.g, (T)px.b, (T)px.a};
		}
		
		const uint8_t* getRawPixels() const;
		
	private:
		sf::Image image;
	};
}
