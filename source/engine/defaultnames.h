#pragma once

#include <string>

namespace dankengine::names {

	const std::string defaultSpriteProfileName = "Sprite.default";
	const std::string defaultMeshProfileName = "Mesh.default";
	const std::string defaultMeshTexturedProfileName = "MeshTextured.default";
	const std::string defaultMeshPhongProfileName = "MeshPhong.default";

}