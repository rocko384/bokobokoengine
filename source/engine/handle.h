#pragma once

#include "engine/dank.h"

#include <memory>

namespace dankengine {

	template<typename T>
	using Handle = std::shared_ptr<T>;

	/*
	template<typename T>
	class Handle {

	public:

		Handle();
		Handle(const Handle& rhs);
		Handle(Handle&& rhs);
		Handle(const T& Object);
		Handle(const std::shared_ptr<T>& Object);

		Handle& operator=(const Handle<T>& rhs);
		Handle& operator=(Handle<T>&& rhs);

		template<typename B>
		Handle& operator=(const Handle<B>& rhs) {
			static_assert(std::is_base_of<T, B>(), "Type is not base of B");

			Handle<T> h = rhs.template base<T>();

			operator=(h);
		}

		template<typename B>
		Handle& operator=(Handle<B>&& rhs) {
			static_assert(std::is_base_of<T, B>(), "Type is not base of B");

			Handle<T> h = rhs.template base<T>();

			operator=(h);
		}

		bool operator==(const Handle<T>& rhs);

		template <typename B>
		Handle<B> base();

		bool isEmpty() {
			return m_ref->get() == nullptr;
		}

		T* operator->();

		void assign(const T& Object);

		T& ref();

//	private:

		std::shared_ptr<T> m_ref;

	};

	template<typename T>
	inline Handle<T>::Handle() {
		m_ref = std::make_shared<T>();
	}

	template<typename T>
	inline Handle<T>::Handle(const Handle& rhs) {
		operator=(rhs);
	}

	template<typename T>
	inline Handle<T>::Handle(Handle&& rhs) {
		operator=(rhs);
	}

	template<typename T>
	inline Handle<T>::Handle(const T& Object) {
		m_ref = std::make_shared<T>(Object);
	}

	template<typename T>
	inline Handle<T>::Handle(const std::shared_ptr<T>& Object) : m_ref(Object) {

	}

	template<typename T>
	inline Handle<T>& Handle<T>::operator=(const Handle<T>& rhs) {
		if (this != &rhs) {
			m_ref = rhs.m_ref;
		}

		return *this;
	}

	template<typename T>
	inline Handle<T>& Handle<T>::operator=(Handle<T>&& rhs) {
		if (this != &rhs) {
			m_ref = rhs.m_ref;
		}

		return *this;
	}

	template<typename T>
	inline bool Handle<T>::operator==(const Handle<T>& rhs) {
		return this->m_ref.get() == rhs.m_ref.get();
	}

	template<typename T>
	inline T* Handle<T>::operator->() {
		return m_ref.get();
	}

	template<typename T>
	inline void Handle<T>::assign(const T& Object) {
		m_ref = std::make_shared(Object);
	}

	template<typename T>
	inline T& Handle<T>::ref() {
		return *m_ref.get();
	}

	template<typename T>
	template<typename B>
	inline Handle<B> Handle<T>::base() {
		Handle<B> h;// (std::static_pointer_cast<B>(m_ref));
		h.m_ref = std::static_pointer_cast<B>(m_ref);

		return h;
	}*/

}