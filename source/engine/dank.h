#pragma once

#if defined(WIN32) && defined(dankengine_EXPORTS)
	#define DankExport __declspec(dllexport)
#elif defined(WIN32) && !defined(dankengine_EXPORTS)
	#define DankExport __declspec(dllimport)
#else
	#define DankExport
#endif