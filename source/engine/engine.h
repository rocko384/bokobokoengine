#pragma once

#include <engine/handle.h>

#include <engine/resources/resource.h>

#include <engine/graphics/gl/shader.h>
#include <engine/graphics/gl/shaderprogram.h>
#include <engine/graphics/gl/vertexarrayobject.h>
#include <engine/graphics/gl/gpubuffer.h>
#include <engine/graphics/gl/texture.h>

#include <engine/graphics/2D/sprite.h>

#include <engine/graphics/3D/mesh.h>

#include <engine/graphics/window.h>
#include <engine/graphics/renderer.h>
#include <engine/graphics/renderable.h>
#include <engine/graphics/material.h>

#include <engine/input/inputswitchboard.h>

#include <engine/debug.h>

